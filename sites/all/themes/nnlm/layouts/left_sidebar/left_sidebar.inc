<?php

// Plugin definition
$plugin = array(
  'title' => t('Left Sidebar'),
  'category' => t('NN/LM theme specific layouts'),
  'icon' => 'left_sidebar.png',
  'theme' => 'left_sidebar',
  'css' => 'left_sidebar.css',
  'regions' => array(
    'left' => t('Left sidebar'),
    'center' => t('Center content')
  ),
);
