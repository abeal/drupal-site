<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Sidebars'),
  'category' => t('NN/LM theme specific layouts'),
  'icon' => 'two_sidebar.png',
  'theme' => 'two_sidebar',
  'css' => 'two_sidebar.css',
  'regions' => array(
    'left' => t('Left sidebar'),
    'center'=> t('Center content'),
    'right' => t('Right sidebar')
  ),
);
