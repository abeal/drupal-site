
<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $date; ?> -- <?php print $user->name; ?></footer>
  <?php endif; ?>

  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
    <?php
    $body = "User: ";
    if(isset($user)){
      $body .= "$user->name";
    }
    else{
      $body .= "Anonymous";
    }
    $body .= "\nServer name: $_SERVER[SERVER_NAME]";
    $body .= "\nRequest URI: $_SERVER[REQUEST_URI]";
    $body .= "\n\n".date("Y-m-d H:i:s")." ".($_SERVER["REMOTE_ADDR"]);
  ?>
  <!-- Output modified by nnlm theme -->
  <div style="margin: 1em; padding: 1em; border: 1px solid green; background: #efe; color: #030;">
  <strong>Your request signature:</strong><?php print "<pre>$body</pre>"; ?>
  </div>
</article>
