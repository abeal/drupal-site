<?php

/**
 * Implements hook_views_query_alter
 * Modifies the admin product query to be sensitive to editorial section.
 *
 * @param  array $view  The view to be altered
 * @param  stdClass $query An object describing the query.
 *
 * @return NULL
 */
function nnlm_commerce_views_query_alter(&$view, &$query) {
  switch ($view->name) {
    case 'commerce_products':
      //nnlm_core_filter_view_editorial_sections($view);
      break;

    default:
      break;
  }
}

