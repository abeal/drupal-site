<?php
namespace Drupal\nnlm_commerce;
use Drupal\nnlm_core\Utilities as Core_Utilities;

/**
 * Contains logic for handling commerce actions
 */
class Controller {

  /**
   * Loads the active order
   *
   * @return array The active commerce order
   * @throws \Exception If errors occur in loading active order, such
   * as no active order existing.
   */
  public static function load_active_order() {
    $active_order = &drupal_static('nnlm_commerce_active_order');
    if (isset($active_order)) {
      return $active_order;
    }
    if (commerce_cart_order_session_exists()) {
      $active_order = commerce_cart_order_load();
    }
    else {
      global $user;
      $active_order = commerce_cart_order_load($user->uid);
    }
    if (!$active_order) {
      throw new \Exception("Active order could not be loaded");
    }
    return $active_order;
  }

  /**
   * Saves changes to the active order
   *
   * @param  array $order The commerce order to be saved
   *
   * @return NULL
   */
  public static function save_active_order($order) {
    commerce_order_save($order);
  }

  /**
   * Provides the editorial section of the products in the cart.
   *
   * @param  array $order The commerce order
   *
   * @return array        A drupal taxonomy term array of the section
   * @throws  \Exception If errors in section loading occur
   */
  public static function get_order_section($order) {
    $section = &drupal_static(__FUNCTION__);
    if (isset($section)) {
      return $section;
    }

    //nnlm_core_dump($order, "Order");
    $first_line_item = commerce_line_item_load($order->commerce_line_items['und'][0]['line_item_id']);
    if (!$first_line_item) {
      throw new \Exception("Could not load the first line item in this order");
    }
    $first_cart_product = commerce_product_load($first_line_item->commerce_product['und'][0]['product_id']);
    if (!$first_cart_product) {
      throw new \Exception("Could not load the first product in this order");
    }
    $section = taxonomy_term_load($first_cart_product->field_section['und'][0]['tid']);

    //nnlm_core_dump($section, "Section0");
    if (isset($section->tid)) {

      //nnlm_core_dump($section, "Setting section by first product");
      return $section;
    }

    //if here, section was not part of the product itself.  See if it's already been
    //established as part of the order
    if (isset($order->data['editorial_section'])) {
      $section_name = $order->data['editorial_section'];
      $section = taxonomy_get_term_by_name($section_name, 'sections');

      //nnlm_core_dump($order, "Setting section by order data");
      if (!empty($section)) {

        // get first entry of result
        return reset($section);
      }
    }

    //If here, section is not yet part of the order, either.  Set it as
    //the first page argument.  This is likely to be the most commonly executed
    //section for new orders
    if (empty($section)) {
      $section = taxonomy_get_term_by_name(arg(0), 'sections');

      //nnlm_core_dump($order, "Setting section by page argument");
      if (!empty($section)) {

        // get first entry of result
        return reset($section);
      }
    }

    //Out of options, set to be national.
    throw new \Exception("Editorial section could not be determined");

    //Really buggered, fail miserably. TODO: flush out this use case?
  }
}

