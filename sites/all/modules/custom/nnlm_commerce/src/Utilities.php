<?php
namespace Drupal\nnlm_commerce;
use Drupal\nnlm_core\Utilities as Core_Utilities;

/**
 * Utilities
 * Various non-hook functions invoked by the nnlm_core module.
 */
class Utilities {

  /**
   * Returns the person or persons to be emailed when an order
   * is submitted.  This function relies on the NN/LM Staff Roster list functionality,
   * and expects there to be certain lists of staff members to handle the individual cases.
   *
   * @param  arary $order the Commerce order
   *
   * @return mixed        an array of emails to send the order request to, or
   *                         an empty array if no valid recipients could be found.  That
   *                         latter case is probably an error, and should be dealt with
   *                         accordingly.
   */
  public static function fetch_order_recipients($order) {
    $result = array();
    //TODO: modify logic to retrieve users assigned  the appropriate section
    //in the Drupal UI.
    return $result;
    $order_section = self::get_order_section($order);
    $order_type = self::get_order_type($order);
    /* get a list of users from the same editorial section */






    //nnlm_core_dump($order_section, "Order section");
    //nnlm_core_dump($order_type, "Order type");
    $query = db_select('workbench_access_user', 'w')->fields('w', array('uid'))->condition('w.access_id', $order_section->tid, '=');
    //drupal_set_message((string) $query, "Order recipients string");
    $editorial_section_owners = $query->execute()->fetchAllAssoc('uid');
    if (empty($editorial_section_owners)) {
      drupal_set_message("No owners could be determined for this store", "error");
      return $result;
    }
    $contact_list_names = array(
      'lending_library_item' => 'Lending library contacts',
      'nlm_promotional_item' => 'NLM promotional item contacts',
    );
    $query = \Database::getConnection('default', 'rosters')->select('lists', 'l');
    $query->fields('l', array('ListMemberTableID'));
    $query->join('list_descriptions', 'ld', 'ld.ListID = l.ListID');
    $query->condition('ld.ListTitle', $contact_list_names[$order_type], '=');
    $query->condition('l.ListMemberTable', 'people', '=');
    $query->condition('l.ListMemberTableID', array_keys($editorial_section_owners), 'IN');
    $result = $query->execute()->fetchallAssoc('ListMemberTableID');
    if (empty($result)) {
      drupal_set_message("Request for list of recipients failed.", "error");
      return $result;
    }
    $editorial_section_owners = user_load_multiple(array_keys($result));
    if (empty($editorial_section_owners)) {
      drupal_set_message("Could not retrieve store owners", "error");
      return $result;
    }
    foreach ($editorial_section_owners as $k => $o) {
      $editorial_section_owners[$k] = $o->mail;
    }
    return $editorial_section_owners;
  }

  /**
   * Provides the type of products in the cart.
   *
   * @param  array $order The commerce order
   *
   * @return mixed        The string machine name of the type, or FALSE if it could not be
   *                          determined (errors set)
   */
  public static function get_order_type($order) {
    $type = &drupal_static(__FUNCTION__);
    if (isset($type)) {
      return $type;
    }
    $first_line_item = commerce_line_item_load($order->commerce_line_items['und'][0]['line_item_id']);
    if (!$first_line_item) {
      drupal_set_message("Could not load the first line item in this order", "Error");
      return FALSE;
    }
    $first_cart_product = commerce_product_load($first_line_item->commerce_product['und'][0]['product_id']);
    if (!$first_cart_product) {
      drupal_set_message("Could not load the first product in this order", "error");
      return FALSE;
    }
    $type = $first_cart_product->type;
    return $type;
  }

  /**
   * Sets the shopping origin for a given order (entity reference to
   * a node)
   *
   * @param int $nid   The nid of the node where the experience originated
   * @param stdClass $order The active shopping cart order
   */
  public static function set_shopping_origin($nid = NULL, $order = NULL) {
    try {
      if (is_null($order)) {
        nnlm_core_dump("Order is null", __FUNCTION__, 'warning');
        $order = Controller::load_active_order();
      }
      $w_order = entity_metadata_wrapper('commerce_order', $order);
      if (empty($nid)) {
        return self::unset_shopping_origin($order);
      }
      elseif (!ctype_digit($nid)) {
        throw new \Exception("Wrong data type passed to " . __FUNCTION__);
      }
      else {
        $w_order->field_commerce_origin_node->set($nid);
      }
      $w_order->save();
      cache_clear_all('nnlm_commerce', 'cache', TRUE);
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), __FUNCTION__, 'error');
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR, 'error');
    }
  }

  /**
   * Sets the shopping origin for a given order (entity reference to
   * a node)
   *
   * @param int $nid   The nid of the node where the experience originated
   * @param stdClass $order The active shopping cart order
   */
  public static function get_shopping_origin($order = NULL) {
    try {
      $origin = &drupal_static(__NAMESPACE__ . '\\Utilities::' . __FUNCTION__);
      if (!empty($origin)) {
        return $origin;
      }
      if (is_null($order)) {
        //nnlm_core_dump("Order is null", __FUNCTION__, 'warning');
        $order = Controller::load_active_order();
      }
      $w_order = entity_metadata_wrapper('commerce_order', $order);
      $origin = $w_order->field_commerce_origin_node->value();
      return $origin;
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), __FUNCTION__, 'error');
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR, 'error');
    }
  }

  /**
   * Clears the field_commerce_origin_node entity reference field.
   *
   * @param  stdClass $order The active shopping cart order
   *
   * @return NULL
   */
  public static function unset_shopping_origin($order = NULL) {
    try {
      if (is_null($order)) {
        $order = Controller::load_active_order();
      }
      $order->field_commerce_origin_node = array();
      commerce_order_save($order);
      cache_clear_all('nnlm_commerce', 'cache', TRUE);
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), __FUNCTION__, 'error');
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR, 'error');
    }
  }
  public static function get_redirect_alias($order = NULL) {
    try {
      $redirect_alias = &drupal_static(__NAMESPACE__ . '\\Utilities::' . __FUNCTION__);
      if (isset($redirect_alias)) {
        return $redirect_alias;
      }
      if (is_null($order)) {
        $order = Controller::load_active_order();
      }
      $origin = Utilities::get_shopping_origin($order);
      $redirect_alias = drupal_get_path_alias('node/' . $origin->nid);
      return "/node/$origin->nid";
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), __FUNCTION__, 'error');
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR, 'error');
    }
  }

  /**
   * Provides the editorial section of the products in the cart.
   *
   * @param  array $order The commerce order
   *
   * @return mixed        A drupal taxonomy term array of the section, or FALSE
   *                        if it could not be determined (errors set)
   */
  public static function get_order_section($order) {
    try {
      static $section;
      if (isset($section)) {
        return $section;
      }
      $origin = self::get_shopping_origin($order);
      if (empty($origin) || empty($origin->workbench_access)) {
        return taxonomy_get_term_by_name('national', 'sections');
      }
      $section = taxonomy_term_load(array_pop($origin->workbench_access), 'sections');
      return $section;
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), __FUNCTION__, 'error');
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR, 'error');
    }
  }
  public static function set_theme($order) {
    static $theme_context = NULL;
    if (!is_null($theme_context)) {
      // it's already been called elsewhere.
      return $theme_context;
    }
    $section = self::get_order_section($order);
    if (!$section) {
      return;
    }
    $theme_context = context_load($section->name . '_theme');
    if ($theme_context !== FALSE) {
      context_set('context', $theme_context->name, $theme_context);
    }
    else {
      nnlm_core_dump("Theme context not found", 'Warning', 'warning');
    }
  }
}

