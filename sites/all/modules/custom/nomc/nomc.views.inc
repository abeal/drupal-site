<?php
// $Id$


/**
 * Implements hook_views_data
 * Allows views integration for the NOMC database
 *
 * @return {a.array} an array of table information, keyed by the name of the table
 */
function nomc_views_data() {
  if(variable_get('nnlm_enable_nomc_database_views') === FALSE){
    return;
  }
  $data = array();
  //Projects table definition
  $data['Projects_Drupal']['table']['group'] = t('NOMC');
  $data['Projects_Drupal']['table']['title'] = t('NOMC Projects');
  $data['Projects_Drupal']['table']['help'] = t("Contains NOMC project data");
  $data['Projects_Drupal']['table']['base'] = array(
    'field' => 'Project_ID',
    'title' => t('NOMC Project'),
    'help' => t('Project information from NOMC'),
    'database' => 'nomc',
  );
  //funding table definition
  $data['Funding']['table']['group'] = t('NOMC');
  $data['Funding']['table']['title'] = t('NOMC Funding');
  $data['Funding']['table']['help'] = t('Contains NOMC funding info');

  //join condition for funding table
  $data['Funding']['table']['join']['Projects_Drupal'] = array(
    'left_field' => 'Project_ID',
    'field' => 'Project_ID',
  );

  /** FIELD DEFINITIONS FOR Projects **/

  /** Project: project id **/
  $data['Projects_Drupal']['Project_ID'] = array(
    'title' => t('Projects_Drupal: Project ID'),
    'help' => t('The project id of this project'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  /** Project: title **/
  $data['Projects_Drupal']['Project_Title'] = array(
    'title' => t('Project Title'),
    'help' => t('The project title of this project'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => FALSE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /** Project: RML **/
  $data['Projects_Drupal']['RML'] = array(
    'title' => t('RML'),
    'help' => t('The Regional Medical Library that owns this project.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => FALSE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /** Project: description **/
  $data['Projects_Drupal']['Project_Description'] = array(
    'title' => t('Project Description'),
    'help' => t('The description of this project.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => FALSE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /** Project: Principal Investigator Institution **/
  $data['Projects_Drupal']['PI_Institution'] = array(
    'title' => t('PI Institution'),
    'help' => t('The Principal Investigator Institution for this project.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => FALSE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /** Project: start date **/
  $data['Projects_Drupal']['StartDate'] = array(
    'title' => t('Start Date'),
    'help' => t('The start date for this project'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_date',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  /** Project: end date **/
  $data['Projects_Drupal']['EndDate'] = array(
    'title' => t('End Date'),
    'help' => t('The end date for this project'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_date',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  /** the primary division is the NLM center that directs
   the funding for this project.  Do not display directly. */
  $data['Projects_Drupal']['primaryDivision'] = array(
    'title' => t('Primary Division'),
    'help' => t('The end date for this project'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => FALSE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /** FIELD DEFINITIONS FOR Funding **/

  /** Funding: project id **/
  $data['Funding']['Project_ID'] = array(
    'title' => t('Funding: Project ID'),
    'help' => t('The project id of this project'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /** Funding: fiscal amount **/
  $data['Funding']['fiscalAmount'] = array(
    'title' => t('Funding award amount'),
    'help' => t('The amount of funding awarded to this project'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /** Funding: fiscal year **/
  $data['Funding']['fiscalYear'] = array(
    'title' => t('Fiscal Year'),
    'help' => t('The fiscal year the award was granted'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      // the field to display in the summary.
      'name field' => 'title',
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}

