<?php
// $Id$

/**
 * Custom implementation of views_handler_filter_date, as the original
 * can't handle YYYY-MM-DD formats appropriately.
 */
class nomc_views_handler_filter_date_sql extends views_handler_filter_date {

  /**
   * displays the options form when creating the view
   * @see https://api.drupal.org/api/drupal/includes!form.inc/group/form_api/7
   *
   * @param  [array] $form       The array containing the form data
   * @param  array $form_state The array describing the form state
   *
   * @return array             An array containing form additions
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function pre_query() {

    /*
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
    if (!isset($values->{$alias})) {
      //I don't know the conditions under which this might occur,
      //so I'm leaving it live for now.
      throw new Exception("Alias not set");
    }
    // YYYY-MM-DD format, or NULL
    $date = date_create($values->{$alias});
    if ($date === FALSE) {
      return FALSE;
    }
    dpm("Current filter value: " . $values->{$alias});
    $values->{$alias} = $date->format('Y-m-d');
    dpm("Transformed filter value: " . $values->{$alias});
    */
    // get the value of the submitted filter
  }
}

