<?php
/**
 * @file
 *   Default theme implementation for Scald File Render.
 */
?>
<div class="scald-file-context-full">
  <div class='file-info'><strong>File information</strong>
    <ul>
      <li><strong>File size: </strong><?php print $vars['file_size']; ?></li>
      <li><strong>File mime type: </strong><?php print $vars['file_mime_type']; ?></li>
      <li><strong>Last updated: </strong><?php print $vars['last_updated']; ?></li>
      <?php if(!empty($vars['used_in'])): ?>
        <li><strong>This file is linked in following locations:</strong><ul>
        <?php foreach($vars['used_in'] as $url): ?>
          <li><a href='<?php print $url; ?>'><?php print $url; ?></a></li>
        <?php endforeach; ?>
        </ul></li>
      <?php endif; ?>
    </ul>
  </div>
  <h1><img src="<?php print file_create_url($vars['thumbnail_source']); ?>" alt="file type icon" /><?php print $vars['file_title']; ?></h1>

  <form method="get" action="<?php print file_create_url($vars['file_source']); ?>">
    <button type="submit">Download now</button>
  </form>
</div>
