<?php
/**
 * @file
 * Custom views handler definition.
 *
 * Place this code in
 * /sites/all/[custom_module_name]/includes/views_handler_my_custom_field.inc
 */

/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class views_handler_register_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }

  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Render callback handler.
   *
   * Return the markup that will appear in the rendered field.
   */
  function render($values) {
    $registration_url = '#'; //default, replace with appropriate.
    $result = '<p class="material-button"><a class="registration-url button" target="_blank" href="';
    switch($values->class_schedules_status){
      case 'W': //wait list
      case 'O': //open for registration
        if(isset($values->class_schedules_reg_required) && intval($values->class_schedules_reg_required) === 1){
          if(isset($values->class_schedules_external_reg_link) && !is_null($values->class_schedules_external_reg_link)){
            //TODO: add check for email address, and transform link appropriately
            $registration_url = $values->external_reg_link;
            break;
          }
          //TODO: replace this with local registration url
          $registration_url = $values->class_regions_register_url.'?schedule_id='.$values->schedule_id;
        }
        else{
          $registration_url = $values->class_regions_register_url.'?schedule_id='.$values->schedule_id;
        }
        break;
      case 'L': //registration is closed
        return t("<p class='error'>Registration is closed for this class</p>");
      case 'C': //don't bother; these aren't displayed.
        return t("<p class='error'>Closed</p>");
      case 'D': //cancelled
        return t("<p class='error'>This class has been cancelled</a>");
    }
    $result .= ($values->class_schedules_status=='W') ? "$registration_url\">Join Waiting List</a></p>" : "$registration_url\">Register</a></p>";
    if(!empty($values->class_schedules_other_reg_info)){
      $result .= "<p><strong>Other info: </strong>$values->class_schedules_other_reg_info</p>";
    }
    return $result;
  }
}
