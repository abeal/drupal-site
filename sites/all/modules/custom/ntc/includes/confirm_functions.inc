<?php
/**
 * @file
 * Custom functions for handling ntrp confirmation pages
 *
 * Place this code in
 * /sites/all/ntc/includes/confirm_functions.inc
 */
 
/**
 * decodes the encrypted values using the Salt
 */
function decode($val){
	return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, "aytpwapywb", base64_decode($val), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
}

/**
 * builds the confirmation form for the user specified
 */
function form_ntc_build_confirm_form($form_state){
	if($_GET['ref']){
		$reg_id = decode($_GET['ref']);	
	}else{
		drupal_set_message(t('The page requested does not exist'.__LINE__),'error');
		drupal_goto('/training-schedule');
	}
	if(!$reg_info = get_reg_info($reg_id)){
		drupal_set_message(t('The page requested does not exist'.__LINE__),'error');
		drupal_goto('/training-schedule');
	}
	if($reg_info['reg_canceled'] == 'Y'){
		$html = '<h1>Registration Canceled</h1>';
		$html.= '<p>This registration has been canceled, if your plans change, please register for the class again.</p>';
		$page[] = array(
			'#type' => 'item',
			'#markup' => t($html)
		);
	}else if($reg_info['confirmed'] == 'Y'){
		$html = '<h1>Registration Confirmed</h1>';
		$html.= '<p>This registration has been confirmed, if your plans change, please contact us.</p>';
		$page[] = array(
			'#type' => 'item',
			'#markup' => t($html)
		);
	}else{
		$class_info = get_class_info($reg_info['schedule_id']);
		$class_description = get_class_details($class_info['class_id']);
		$rgn = get_rgn_settings($reg_info['region']);
		$respond_by = strtotime($class_info['class_date']);		
		$respond_by-=(60*60*24*14);
		$temp=date("l",$respond_by);
		while($temp!=="Friday"){//force date to friday
			$respond_by+=(60*60*24);
			$temp=date("l",$respond_by);
		}
		$respond_by=date("l, F d, Y",$respond_by);
		$html = '<h1>Class Participation Confirmation</h1>';
		$html.= '<p>Thank you for registering for the NN/LM class listed below. Please review the class information and click "Confirm" below to verify that you will be attending the class. If you are no longer able to attend the class please click "Cancel".</p><p>Please confirm or cancel before: <strong>'.$respond_by.'</strong></p><hr/>';
		$html.= '<h2>Class Information</h2>';
		$html.= '<div style="padding-left: 3em"><p>'.$class_description['class_name'];
		$html.=($class_description['duration']?'<br/>'.$class_description['duration']:'');
		$html.='<br/><a href="/ntc/classes/class_details.html?class_id='.$class_info['class_id'].'">Details</a>';
		$html.='</p></div>';
		$html.= '<h2>Location</h2>';
		$html.= '<div style="padding-left: 3em"><p>';
		$html.= ($class_info['state']=='XX'?'Online':'<a href="/ntc/classes/site.html?site_id='.$class_info['site_id'].'">'.$class_info['city'].', '.$class_info['state']."</a>");
		$html.= '</p></div>';
		$html.= '<h2>Date</h2>';
		$html.= '<div style="padding-left: 3em"><p>';
		$html.= ($class_info['end_date']?
			date('l, F d, Y',strtotime($class_info['class_date'])).' - '.date('l, F d, Y',strtotime($class_info['end_date'])):
			date('l, F d, Y',strtotime($class_info['class_date'])).($class_info['class_time']?' '.date('g:i',strtotime($class_info['class_time'])).' '.$class_info['time_zone']:''));
		$html.= ($class_info['addtl_date_info']?'<br/>'.$class_info['addtl_date_info']:'');
		$html.= '</p></div>';
		$html.= '<h2>Sponsoring RML/Center</h2>';
		$html.= '<div style="padding-left: 3em"><p>';
		$html.= '<a href="'.$rgn['home_url'].'">'.$rgn['description'].'</a>';
		$html.= '</p></div><hr/>';
		$page[] = array(
			'#type' => 'item',
			'#markup' => t($html)
		);
		if($class_description['special_terms']){
			$page['special_terms'] = array(
				'#type' => 'checkbox', //you can find a list of available types in the form api
				'#title' => "I agree with the statement below:",
				'#description' => $class_description['special_terms'],
				'#default_value' => 0,
				//'#required' => $required//Can't be required, otherwise can't submit Cancel. Will have to do this in the validate handler.
			);
		}
		$page['confirm_btn'] = array(
			'#type' => 'submit',
			'#title' => 'Confirm',
			'#value' => 'Confirm',
		);
		$page['cancel_btn'] = array(
			'#type' => 'submit',
			'#title' => 'Cancel',
			'#value' => 'Cancel',
		);
	}
	return $page;
}

/**
 * validates the confirmation form
 */
function form_ntc_validate_confirm_form($form_state){
	//print_r($form_state);
	if(($form_state['triggering_element']['#title']=='Confirm')&&$form_state['values']['special_terms']===0){//(NULL==0)==true, but (NULL===0)==false
		form_set_error('special_terms', 'You must accept the terms.');
	}
}

/**
 * submits teh confirmation form
 */
function form_ntc_submit_confirm_form($form_state){
	$reg_id = decode($_GET['ref']);
	$reg_info = get_reg_info($reg_id);
	$schedule_id = $reg_info['schedule_id'];
	$now = date("Y-m-d H:i:s", time());

	if($form_state['triggering_element']['#title']=='Confirm'){
		db_set_active('general');
		$query = db_update('class_regs')
			->fields(array(
				'confirmed'=>'Y',
				'confirm_cancel_date'=>$now
			))
			->condition('reg_id', $reg_id, '=')
			->execute();
		if($query){
			db_set_active();// return to default db
			send_email($reg_id,3);
			drupal_set_message(t('Your registration has been confirmed.'));
			return true;
		}else{
			db_set_active();// return to default db
			form_set_error(NULL, 'An unknown error occured. Please try again.');
			return false;
		}
	}else if($form_state['triggering_element']['#title']=='Cancel'){
		db_set_active('general');
		$query = db_update('class_regs')
			->fields(array(
				'reg_canceled'=>'Y',
				'confirm_cancel_date'=>$now
			))
			->condition('reg_id', $reg_id, '=')
			->execute();
		if($query){
			db_set_active();// return to default db
			update_schedule_id($schedule_id);
			send_email($reg_id,3);
			drupal_set_message(t('Your registration has been canceled.'));
			return true;
		}else{
			db_set_active();// return to default db
			form_set_error(NULL, 'An unknown error occured. Please try again.');
			return false;
		}
	}else{
		form_set_error(NULL, 'An unknown error occured. Please try again.');
		return false;
	}
}

function cancel_reg($reg_id){
	
}

function confirm_reg($reg_id){
	
}