<?php
namespace NNLM\Migration\Utility;
use NNLM\Migration\Utilities as U;

/**
 *
 */
class MenuTree {
  const STAGING_AREA_ROOT = '/var/www/html';
  // set to NULL to remove restriction
  const HOST_RESTRICTION = 'november.hsl.washington.edu';
  // The name of the drupal menu this tree reflects
  private $menu_name = NULL;
  // The root of the node tree we will build
  private $menu_tree = NULL;
  // Editorial section
  private $section = NULL;

  /**
   * Upon construction, will build a tree representation of the
   * menu whose name was passed in the constructor
   *
   * @param string $menu_name The name of a drupal menu.  By convention, all
   *                          NN/LM sectional menus in drupal are named
   *                          'menu-[section]-main-menu'
   * @throws \Exception If    No root node (Node is named "Navigation", and
   *                           has a plid of 0) exists in the menu, or the
   *                           menu with the passed name does not exist.
   *
   */
  public function __construct($menu_name) {
    if (menu_load($menu_name) === FALSE) {
      throw new \Exception("Menu $menu_name does not exist");
    }
    $menu_links = menu_load_links($menu_name);
    $menu = array();
    foreach($menu_links as &$link){
      $alias = drupal_get_path_alias($link['link_path']);
      if(preg_match("/^http/", $alias)){
        $menu[$alias] = $link;
      }
      else{
        $menu []= $link;
      }
    }
    unset($menu_links);
    ksort($menu);
    //find the root node in this menu.
    foreach ($menu as $path=>$link) {
      if (self::is_root_link($link)) {
        $this->menu_tree = $this->build_node($link);
        unset($menu[$path]);
      }
    }
    //double check a root was found.
    if (is_null($this->menu_tree)) {
      throw new \Exception("Root link not found or incorrect:" . print_r($root_link, TRUE));
    }

    $this->section = preg_replace("/menu-([a-z]+)-main-menu/", "$1", $menu_name);
    //process interesting menu items into the tree
    foreach ($menu as $path=>$link) {
      //Do not modify menu items that the user has already moved.
      $node = $this->build_node($link);
      //Do not move menu items that cannot realize an alias.
      if (empty($node->alias)) {
        //U::dump($link['link_title'], 'Not moving unaliased node');
        continue;
      }
      //Do not move menu items that, for whatever reason, could not
      //be parsed into an array for comparison.
      if (empty($node->parts)) {
        continue;
      }
      //Recursive callback, called with tree root and node to hang.
      self::hang_node($this->menu_tree, $node);
    }
  }

  /**
   * Displays a representation of the tree structure of this menu
   *
   * @return string
   */
  public function __toString() {
    return $this->rec_print();
  }

  /**
   * Applies the tree structure built in the constructor to the menu,
   * excluding items that have been directly modified or do not
   * point to internal links. Called initially with $this->menu_tree
   *
   * @return int Number of items moved
   */
  public function apply_structure(&$node = NULL, &$options = array()) {
    //set option defaults
    $options['check_only'] = @$options['check_only'] ?  : FALSE;
    $options['nid'] = @$options['nid'] ?  : NULL;
    $moved = 0;
    if (is_null($node)) {
      //U::dump($this->menu_name, "Applying constructed tree structure to menu");
      $node = &$this->menu_tree;
    }

    //check that all subfolders are represented
    //check backwards in the path until we find an unrepresented node,
    //or a success.  Because the array is sorted, we need not check further
    //than that.
    for ($i = count($node->parts) - 1; $i > 0; $i--) {
      $built_path = array_slice($node->parts, 0, $i);
      $existing_node = &$this->find_alias(implode('/', $built_path));
      if ($existing_node === NULL) {
        U::dump("Subfolder " . implode('/', $built_path) . ' has no representation', "WARNING");
      }
      else {
        // subpath is represented appropriately.
        break;
      }
    }
    if (!empty($node->children)) {
      //check that tree child nodes link plid value matches parent's link mlid
      foreach ($node->children as & $child) {
        //link does not match node tree structure; move it.
        if (!$options['check_only']) {
          $moved++;
          self::move_link($child, $node);
        }

        $moved += self::apply_structure($child, $options);
      }
    }
    return $moved;
  }

  /**
   * Finds all .prefs files in the staging area, and constructs a keyed
   * array where a path lookup will retrieve a correct .prefs entry (array)
   * for that location.  Cached for speed (cache is refreshed daily)
   *
   * Note: this function can only work where there is an active copy of
   * the staging area (the folder synchronized with production for legacy
   * nnlm.gov).  Currently hardcoded to only work on november.
   *
   * For more information, @see http://goo.gl/2BR1dn
   *
   * @return array The keyed array as described above.
   */
  private static function build_menu_prefs() {
    //test that we are on november.
    if (!is_null(self::HOST_RESTRICTION) &&
      $_SERVER['HOSTNAME'] !== self::HOST_RESTRICTION
    ) {
      throw new \Exception("The MenuTree class can only work on the
        host " . self::HOST_RESTRICTION);
    }
    $cache_name = 'nnlm_migrate_build_menu_prefs_cache';
    // recalculate .prefs cache daily
    $cache_max_age = time() + 86400;
    //array of values built up from site root.
    //

    $prefs_file_contents = &drupal_static($cache_name);
    if (!isset($prefs_file_contents)) {
      U::dump("Static cache miss");
      if ($cache = cache_get($cache_name)) {
        U::dump("DB cache hit");
        $prefs_file_contents = $cache->data;
      }
      else {
        U::dump("DB cache miss");
        U::dump("Rebuilding .prefs file information");
        try {
          $prefs_files = array();
          $objects = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(self::STAGING_AREA_ROOT),
            \RecursiveIteratorIterator::LEAVES_ONLY,
            \RecursiveIteratorIterator::SELF_FIRST |
            \RecursiveIteratorIterator::CATCH_GET_CHILD
          );
          //get paths of all .prefs files in the staging area
          foreach ($objects as $prefs_path => $object) {
            if ($object->getFilename() == '.prefs') {
              if (strpos($prefs_path, '/includes/includes') !== FALSE) {
                //user error
                continue;
              }
              $affected_directory = str_replace('/includes/.prefs', '', $prefs_path);
              $prefs_files[$affected_directory] = array($affected_directory => $prefs_path);
            }
          }
          //sort by url to allow ordered construction
          ksort($prefs_files);
          //fill array of each entry with .prefs entries for itself and parent
          //values in parent directories.
          $last_affected_directory = NULL;
          foreach ($prefs_files as $affected_directory => $prefs) {
            foreach ($prefs_files as $parent_directory => $prefs) {
              if ($parent_directory === $affected_directory) {
                break;
              }
              if (strpos($affected_directory, $parent_directory) !== FALSE) {
                //this is a subfolder of the other.
                $prefs_files[$affected_directory] += $prefs_files[$parent_directory];
              }
            }
            //sort values so they are loaded in the correct order for
            //value overrides to occur.
            ksort($prefs_files[$affected_directory]);
          }
          $prefs_file_contents = array();
          foreach ($prefs_files as $affected_directory => $arr) {
            $result = array();
            //U::dump($affected_directory, "Processing");
            foreach ($arr as $k => $prefs_filepath) {
              if (isset($prefs_file_contents[$k])) {
                //U::dump($k, "Merging cache of ");
                //see if we've already parsed it earlier
                $result = array_merge($result, $prefs_file_contents[$k]);
              }
              else {
                //U::dump($prefs_filepath, "parsing new config at");
                $result = self::parse_prefs_file($prefs_filepath, $result);
              }
            }
            //replace filepath entries with parsed entries
            $prefs_file_contents[$affected_directory] = $result;
          }
          cache_set($cache_name, $prefs_file_contents, 'cache', $cache_max_age);
        }
        catch(\Exception$e) {
          //do nothing.
          U::dump($e->getMessage(), __FUNCTION__ . ": Caught exception");
        }
      }
    }
    return $prefs_file_contents;
  }

  /**
   * Creates a stdClass object with the following properties:
   *   url: the link_path value of the original link
   *   nid: The node the link belongs to
   *   alias: The path_alias of the node, if it exists, or NULL otherwise.
   *   parts: an array containing the separate "folders" present in the url
   *          (separated by / in the url)
   *   children: an array that will ultimately contain other nodes that
   *             live below this node in the heirarchy
   *   link:  The original link object
   *
   * @param  array $link  A loaded drupal link object
   *
   * @return stdClass     The object as described above
   */
  private function build_node($link) {
    $node = new \stdClass();
    $node->url = $link['link_path'];
    $node->title = $link['link_title'];
    $node->nid = str_replace('node/', '', $link['link_path']);
    $node->alias = U::relative_url(drupal_lookup_path('alias', $node->url));
    $node->parts = explode("/", $node->alias);
    $node->customized = (intval($link['customized']) === 1) ? TRUE : FALSE;
    $node->children = array();
    $node->link = $link;
    //U::dump($node->alias, "Built node from $node->url.  NID: $node->nid, Alias");
    return $node;
  }

  /**
   * Iterates through the tree structure, and identifies links
   * that are not categorized correctly.  Output assumes command line.
   *
   * @return NULL
   */
  public function check_structure() {
    U::dump("Checking menu structure");
    $moved = $this->apply_structure($this->menu_tree, TRUE);
    U::dump($moved, "Number of items misfiled");
  }

  /**
   * Removes duplicate items that exist as siblings within the menu. "duplicate"
   * in this case refers to the path alias of the item.
   *
   * @param  stdClass $node A node object of the type primarily used by this
   *                        class
   *
   * @return NULL
   */
  private static function dedupe(&$node) {
    $dupe_list = array();
    foreach ($node->children as $i => $child) {
      if (in_array($child->alias, $dupe_list)) {
        U::dump($child->alias, "Removing duplicate menu item " . $child->title);
        menu_link_delete($child->link['mlid']);
        unset($node->children[$i]);
      }
      else {
        $dupe_list[] = $child->link['link_path'];
      }
    }
    foreach ($node->children as $child) {
      self::dedupe($child);
    }
  }

  /**
   * Returns all instances of a given alias in the menu, or FALSE if
   *
   * @param  string $alias A valid menu alias //TODO:Fill out def. better
   *
   * @return array        A reference to the matching tree node, or NULL
   *                        if one is not found.
   */
  public function find_alias($alias, &$node = NULL) {
    $recent_results = &drupal_static(__FUNCTION__);
    if (empty($recent_results)) {
      $recent_results = array();
    }
    if (isset($recent_results[$alias])) {
      return $recent_results[$alias];
    }
    $alias_parts = explode('/', $alias);
    if (empty($alias_parts)) {
      return FALSE;
    }
    if (is_null($node)) {
      $node = &$this->menu_tree;
    }
    if (count($alias_parts) === count($node->parts)) {
      if ($alias === $node->alias) {
        $recent_results[$alias] = TRUE;
        return TRUE;
      }
    }
    elseif (count($alias_parts) > count($node->parts)) {
      for ($i = 0; $i < count($node->parts); $i++) {
        if ($node->parts[$i] !== $alias_parts[$i]) {
          // cannot be a child of this node.
          return FALSE;
        }
      }
      if (!empty($node->children)) {
        foreach ($node->children as $child) {
          //possibly a child beyond here.
          $result = $this->find_alias($alias, $child);
          if ($result) {
            return TRUE;
          }
          //otherwise, keep looking.
        }
      }
    }
    return FALSE;
  }

  /**
   * Constructs a stdClass tree of the menu
   * to structure heirarchically correct.  If a menu url is entirely
   * contained within its parent (using / markers - that is to say,
   * foo/bar/baz would be a child of foo/bar, but not of foo/bar/ba),
   * then it is re-saved as a child of the parent.
   *
   * @param \stdClass $parent The parent node.  Called initially with root.
   * @param  \stdClass $node  The node to hang
   *
   * @return bool TRUE if the node was hung as a child of $parent, FALSE
   *                   otherwise.
   */
  private static function hang_node(&$parent, &$node) {
    //U::dump("Parent: $parent->alias, Node: $node->alias", "Hangnode");
    //depth first traversal of tree
    foreach ($parent->children as $child) {
      if (self::hang_node($child, $node)) {
        return TRUE;
      }
    }
    //parent is root; have to hang.
    if (self::is_root_link($parent->link)) {
      //root node, hang it.
      $parent->children[] = $node;
      return TRUE;
    }
    if (count($node->parts) < count($parent->parts)) {
      //a shorter url cannot be the child of a longer one.
      return FALSE;
    }
    for ($i = 0; $i < count($parent->parts); $i++) {
      if ($node->parts[$i] !== $parent->parts[$i]) {
        // parent url must be entirely contained within child. do not
        // hang
        return FALSE;
      }
    }
    //match of parent so far.  Check if length is the same.
    if (count($node->parts) === count($parent->parts)) {
      //url is the same, for all intents and purposes.  Do not hang; this
      //is a sibling, not a child.  It will likely be removed by the
      //dedupe function in a subsequent step (still deciding policy on that)
      return FALSE;
    }
    //node is a child of $parent.
    $parent->children[] = $node;
    return TRUE;
  }

  /**
   * checks the link for the criteria we have set that defines a link
   * as being the 'root' of the menu heirarchy
   *
   * @param  array  $link A drupal link array descriptor
   *
   * @return bool       TRUE if the link matches the criteria for being 'root',
   *                         FALSE otherwise
   */
  private static function is_root_link($link) {
    return ($link['link_title'] == 'Navigation' && $link['plid'] == 0);
  }

  /**
   * Performs the necessary drupal operations to reparent a given menu link.
   *
   * @param  stdClass $node   A node of the type primarily used by this class
   * @param  stdClass $parent The parent node of $node
   *
   * @return NULL
   * @throws \Exception If something goes wrong with the save operation.
   */
  private static function move_link(&$node, &$parent, $force = FALSE) {
    if ($node->link['plid'] == $parent->link['mlid']) {
      return TRUE;
    }
    if ($node->customized && !$force) {
      U::dump($node->title . '(' . $node->alias . ')', "Cannot move customized node");
      return FALSE;
    }
    //U::dump('(' . $parent->alias . ')', "Moving link (" . $node->alias . ')' . " under parent ");
    $node->link['plid'] = $parent->link['mlid'];
    $node->link['router_path'] = 'node/%';
    if (!menu_link_save($node->link)) {
      throw new \Exception("menu link " . $node->alias . " could not be saved.");
    }
    return TRUE;
  }

  /**
   * parses a nnlm.gov .prefs file into an array
   *
   * @param  string $filepath The path to a valid .prefs file
   * @param  array  $result   The array to place values into.  Duplicate keys
   *                          will be overwritten if the passed array is
   *                          non-empty
   *
   * @return array  The updated result.
   */
  private static function parse_prefs_file($filepath, $result = array()) {
    if (basename($filepath) !== '.prefs') {
      throw new \Exception("Invalid attempted usage of " . __FUNCTION__);
    }
    if (!file_exists($filepath) || !is_readable($filepath)) {
      throw new \Exception("$filepath does not exist or is not readable");
    }
    $fp = fopen($filepath, 'r');
    $linenum = 0;
    while ($line = fgets($fp, 1024)) {
      $line = trim($line);
      $linenum++;
      if (preg_match("/^#/", $line)) {
        // comment
        continue;
      }
      if (empty($line)) {
        continue;
      }
      list($key, $val) = preg_split("/=/", $line, 2);
      $result[$key] = $val;
    }
    return $result;
  }

  /**
   * Prints the menu tree heirarchy, formatted for command line
   *
   * @param  stdClass  $node  Called initially with the root node
   * @param  integer $depth The depth of the recursion. Used internally
   *                        for formatting
   *
   * @return NULL
   */
  private function rec_print(&$node = NULL, $depth = 0) {
    if (is_null($node)) {
      $node = &$this->menu_tree;
    }
    try {
      $title = $node->title;
      $buffer = str_repeat('-', $depth);
      $result = "\n" . $buffer . '-' . '(' . $node->alias . ') ' . $title;
      if (!empty($node->children)) {
        foreach ($node->children as $child) {
          $result .= self::rec_print($child, ($depth + 1));
        }
      }
      return $result;
    }
    catch(\Exception$e) {
      U::dump($e->getMessage(), "Exception");
      U::dump($node, "Node");
    }
    return '';
  }

  /**
   * Recursively sorts the node tree based on alias
   *
   * @param  stdClass $node Called initially with the root of the node tree
   *
   * @return NULL
   */
  private static function rec_sort(&$node) {
    usort($node->children,
    function ($a, $b) {
        if ($a->link['link_path'][0] == '<') {
          // move pseudo-links to front
          return - 1;
      }
        if ($b->link['link_path'][0] == '<') {
          // pseudo links to front.
          return 1;
      }
        return strcasecmp($a->alias, $b->alias);
    }
    );
    foreach ($node->children as $child) {
      self::rec_sort($child);
    }
  }
}

