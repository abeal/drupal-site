<?php
namespace NNLM\Migration\MigrationSource;
use NNLM\Migration\Utilities as U;

/**
 * This custom source class is necessary because I need to do 2 things during
 * a file migration:
 * 1) Determine the list of active files by examining already imported nodes,
 * and
 * 2) Be able to provide those files from a filesystem location outside of drupal.
 * Note that this class uses MigrateFileList to do the actual heavy lifting.
 */
class FileSource extends \MigrateSource {
  // List of nodes whose content should be queried for this migration
  protected $nodelist = array();
  protected $firstrun = FALSE;
  public function __construct(Migration & $migration, array$options = array()) {
    parent::__construct($this, $options);
    // adding this here for earlier access than is provided by the parent class.
    $dependencies = $migration->getDependencies();
    //drupal_set_message("dependencies: " . print_r($dependencies, true));
    foreach ($dependencies as $d_machine_name) {
      $map_table = 'migrate_map_' . $d_machine_name;
      drupal_set_message("Database table: $map_table");
      $result = db_query('SELECT destid1 FROM {' . $map_table . '}');
      $node_ids = array();
      foreach ($result as $record) {
        $node_ids[] = $record->destid1;
      }
      $this->nodelist =& $node_ids;
    }
    $filelist = array_unique($filelist);
    sort($filelist);

    $this->filelist = array();
    foreach ($filelist as $link) {
      $o = new StdClass();
      $o->id = md5($link);
      $o->link = $link;
      $this->filelist[$o->id] = $o;
    }
  }

  /**
   * Return a string representing the source, for display in the UI.
   */
  public function __toString() {
    return t('Assembles a list of media files from all nodes in a given completed migration, and returns that list, with the arrays "images" and "files" as sorted sub-arrays of those same links divided by type.');
  }

  public function getIdList() {
    if (empty($this->filelist)) {
      return array();
    }
    return array_map('md5', $this->filelist);
  }

  /**
   * Returns a list of fields available to be mapped from the source,
   * keyed by field name.
   */
  public function fields() {
    drupal_set_message('fields');
    return array(
      'id' => t('ID'),
      'link' => t('nnlm.gov url location'),
    );
  }

  /**
   *
   * @return the number of available source records.
   */
  public function computeCount() {
    return count($this->filelist);
  }

  /**
   * reconstructs the file list by querying source nodes.
   *
   * @return [type] [description]
   */
  private function rebuildFileList() {
    //drupal_set_message('rebuildFileList');
    $filelist = array();;
    $dependencies = $this->activeMigration->getDependencies();
    //drupal_set_message("dependencies: " . print_r($dependencies, true));
    foreach ($dependencies as $d_machine_name) {
      $map_table = 'migrate_map_' . $d_machine_name;
      drupal_set_message("Database table: $map_table");
      $result = db_query('SELECT destid1 FROM {' . $map_table . '}');
      $node_ids = array();
      foreach ($result as $record) {
        $node_ids[] = $record->destid1;
      }
      //drupal_set_message("Node ids: " . print_r($node_ids, true));
      $nodes = node_load_multiple($node_ids);
      //drupal_set_message("Node count: " . count($nodes));
      //scan each node's body for links.  Add them to filelist.
      foreach ($nodes as $n) {
        $e = entity_metadata_wrapper('node', $n);
        $origin_url = array_shift(array_values($e->field_origin_url->value()));
        //drupal_set_message("processing url " . $origin_url);
        $body = array_shift(array_values($e->body->value()));
        $links = self::extract_links($body);
        //drupal_set_message("Links: " . print_r($links, TRUE));
        $filelist = array_merge($filelist, $links);
      }
    }
    $filelist = array_unique($filelist);
    sort($filelist);

    $this->filelist = array();
    foreach ($filelist as $link) {
      $o = new StdClass();
      $o->id = md5($link);
      $o->link = $link;
      $this->filelist[$o->id] = $o;
    }
    //drupal_set_message("filelist: " . print_r($this->filelist, TRUE));
  }

  /**
   * Do whatever needs to be done to start a fresh traversal of the source data.
   *
   * This is always called at the start of an import, so tasks such as opening
   * file handles, running queries, and so on should be performed here.
   */
  public function performRewind() {
    $this->rebuildFileList();
  }
}
