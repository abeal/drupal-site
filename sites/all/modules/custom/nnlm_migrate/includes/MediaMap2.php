<?php

/**
 * @file
 */
namespace NNLM\Migration;
use NNLM\Migration\Utilities as U;

/**
 * For sophisticated tracking of media items during the migration process.
 */
class MediaMap2
{
  protected static $allowed_types = array('file', 'image', 'audio', 'video',);

  const DEFAULT_ATOM_CONTEXT = 'inline';
  const DEFAULT_FILE_ATOM_CONTEXT = 'aliased_representation';

  /**
   *
   */
  public function __construct() {
  }

  /**
   * Loads any atoms in Drupal that have the given md5 value.
   *
   * @param string $md5_value
   *   an md5 value for a file
   *
   * @return mixed            The first atom encountered whose file matches that md5 value, or NULL if none found.
   */
  private static function getAtomByMD5($md5_value) {
    $query = db_select('filehash', 'f')->fields('f', array('fid', 'md5'))->condition('md5', $md5_value, '=')->range(0, 1);
    $result = $query->execute();
    $record = $result->fetchObject();
    if (!$record || !$record->fid) {
      U::dump(__FUNCTION__ . ": record not found", 'Warning', 'warning');
      return NULL;
    }
    $sid = scald_search(array('base_id' => $record->fid), FALSE, TRUE);
    if (empty($sid)) {
      U::dump(__FUNCTION__ . "File $uri exists in Drupal, but has no scald atom.", 'Error', 'error');
      return NULL;
    }
    else {
      U::dump("Scald atom found with sid $sid");
    }
    $atom = scald_atom_load($sid);
    if (!$atom) {
      throw new \Exception("Scald atom with id $sid was found, but couldn't be loaded");
    }
    //U::dump($atom, "Atom");
    return $atom;
  }

  /**
   * Convenience functino for findMapentry.
   */
  public static function getMapEntry($uri, $options = array()) {
    return self::findMapEntry($uri, $options);
  }

  /**
   * The actual output here is cobbled together from the scald code, and the
   * output from the scald javascript dnd module.  It just has to hold
   * together for the migration.
   *
   * @param  stdClass $atom The atom to be rendered
   *
   * @return [type]       [description]
   */
  public static function render_atom($atom, $context = NULL, $legend = '') {
    $atom_html = '';
    if(is_null($context)){
      if($atom->type === 'file'){
        $context = self::DEFAULT_FILE_ATOM_CONTEXT;
      }
      else{
        $context = self::DEFAULT_ATOM_CONTEXT;
      }
    }
    if(is_string($atom) || is_integer($atom)){
      $atom = scald_atom_load($atom);
    }
    if (!empty($atom)) {
      $atom_html = scald_render($atom, $context);
    }
    else {
      throw new \Exception(__FUNCTION__ . ": atom was empty!");
    }
    if (empty($atom_html)) {
      return '<div class="error">No output found for atom ' . $atom->title . '</div>';
    }
    //U::dump($atom, "Atom");
    $output = '
    <div contenteditable="false" class="dnd-atom-wrapper type-' . $atom->type . ' context-' . $context . '">
      <div class="dnd-drop-wrapper">' . $atom_html . '</div>
      <div class="dnd-legend-wrapper" contenteditable="true"><div class="meta">' . $legend . '</div></div>
    </div>';
    //U::dump($output, "Output");
    return $output;
  }

  /**
   * Searches the migration map for an entry matching the given parameters.
   */
  public static function findMapEntry($key, $value, $options = array()) {
    $o = new \stdClass();
    U::dump($value, "Finding map entry by property Key: $key, Value");
    if ($key === 'loc') {
      $value = strtolower($value);
    }
    try {
      $connection = \Database::getConnection('default', 'migration_map');
      $query = $connection->select('migration_map', 'm')->fields('m', array('loc', 'file_md5'))->isNotNull("file_md5")->condition('file_md5', '0', '<>')->condition($key, $value)->range(0, 1);
       //only care about the first.
      list($count_result) = $query->countQuery()->execute()->fetchCol();
      //U::dump($count_result, "Count result");
      if ($count_result == 0) {
        U::dump("No records found with key $key matching value $value", "Warning", "warning");
        return NULL;
      }
      $result = $query->execute();
      foreach ($result as $row) {
        //U::dump($row, "findMapentry result");
        foreach ($row as $k => $v) {
          $o->{$k} = $v;
        }
      }
      if (!isset($o->file_md5)) {
        throw new \MigrateException("MD5 sum not found.  Not thought possible");
      }

      //unreliable values - eliminate
      $o->scald_id = NULL;
      $o->drupal_node_id = NULL;

      //note, the following does a Drupal db lookup to check for duplicates first.
      $atom = self::importFileEntry($o->loc);
      if (empty($atom)) {
        U::dump("Could not create an atom from file found at $loc", "Warning", "warning");
        return NULL;
      }
      $o->scald_id = $atom->sid;
      return $o;
    }
    catch(\Exception $e) {
      U::dump($e->getMessage(), "Exception in " . __FUNCTION__, 'error');
      return NULL;
    }
  }

  /**
   * Updates the map file entry if missing.
   *
   * @param string $uri
   *   The uri of the media item
   *
   * @return boolean
   */
  public static function verifyFileEntry($uri) {
    $filepath = U::real_filename($uri);
    return (empty($filepath)) ? FALSE : TRUE;
  }

  /**
   * Imports the file located at nnlm.gov uri passed.
   *
   * @param string $uri
   *   the nnlm.gov url of the file on the legacy filesystem
   *
   * @return $object  The scald atom that was created.
   *
   * @throws \MigrateException if errors happen during the creation process.
   */
  private static function importFileEntry($uri) {
    U::dump("Attempting to import new file $uri");
    if (empty($uri)) {
      throw new \MigrateException("URI was empty in MediaMap::importFileEntry");
    }

    // U::log($uri, "File entry for file not found. Importing", 'notice');
    $lower_url = strtolower($uri);
    $filesystem_uri = str_replace("http://nnlm.gov/", "/var/www/html/", $lower_url);
    $case_corrected_filesystem_uri = U::real_filename($filesystem_uri);
    if (!$case_corrected_filesystem_uri) {
      throw new \MigrateException("File $filesystem_uri does not exist on filesystem.  Cannot import");
    }

    // Check md5 value to make sure it doesn't already exist in Drupal.
    $md5_value = md5_file($case_corrected_filesystem_uri);
    if (empty($md5_value)) {
      throw new \MigrateException("Could not import file $filesystem_uri");
    }
    $atom = self::getAtomByMD5($md5_value);
    if (!is_null($atom)) {
      U::dump($atom->sid, "Atom already exists");
      return $atom;
    }

    //file does not exist.  Import
    $finfo = new \finfo(FILEINFO_MIME);
    $type = array_shift(explode('/', $finfo->file($case_corrected_filesystem_uri)));
    if (!in_array($type, self::$allowed_types)) {
      $type = 'file';
    }
    $valid_editorial_sections = nnlm_core_get_editorial_sections(array('tid' => TRUE));

    // field_section will be something like 'pnr', 'psr', 'scr'
    $field_section = (explode('/', str_replace('http://nnlm.gov/', '', $lower_url)));
    if (is_array($field_section)) {
      $field_section = reset($field_section);
    }
    if (!array_key_exists($field_section, $valid_editorial_sections)) {
      U::log($uri, "Section $field_section does not exist (using all_regions default)", 'warning');
      $field_section = 'all_regions';
    }
    $file = (object)array(
      'uid' => 1,
      'title'=>basename($case_corrected_filesystem_uri),
      'uri' => $case_corrected_filesystem_uri,
      'filemime' => file_get_mimetype($case_corrected_filesystem_uri),
      'status' => 1);
    // $handle = fopen($case_corrected_filesystem_uri, 'r');
    // if (!$handle) {
    //   throw new \MigrateException("Could not open handle to file $case_corrected_filesystem_uri.");
    // }
    // $file = file_save_data($handle, NULL, \FILE_EXISTS_ERROR);
    // fclose($handle);
    $file = file_copy($file, 'public://migrated');
    if (empty($file)) {
      throw new \MigrateException("Could not save file date from file $case_corrected_filesystem_uri.");
    }
    U::dump($file->fid, U::termcolored("New managed file created from uri $case_corrected_filesystem_uri.  Fid", 'GREEN'));
    $atom = U::create_scald_atom($file);
    if (!$atom) {
      throw new \MigrateException("Could not create scald atom from file $case_corrected_filesystem_uri.");
    }
    //U::dump($atom, "Created new atom with id $atom->sid", 'notice');
    return $atom;
  }
}
