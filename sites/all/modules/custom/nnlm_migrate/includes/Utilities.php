<?php
// $Id$

namespace NNLM\Migration;
use NNLM\Migration\Utility\MenuTree as MenuTree;

/**
 *  General utilities
 *
 * Some general utilities to aid the other classes in this module.
 */
class Utilities {
  private static $has_firephp = FALSE;
  // one hour max for migrations to execute
  const CRON_EXECUTION_TIME = 3600;
  private static $log = array();
  private static $initial_memory_usage = 0;
  private static $_colors = array(
    'LIGHT_RED' => "[1;31m",
    'LIGHT_GREEN' => "[1;32m",
    'YELLOW' => "[1;33m",
    'LIGHT_BLUE' => "[1;34m",
    'MAGENTA' => "[1;35m",
    'LIGHT_CYAN' => "[1;36m",
    'WHITE' => "[1;37m",
    'NORMAL' => "[0m",
    'BLACK' => "[0;30m",
    'RED' => "[0;31m",
    'GREEN' => "[0;32m",
    'BROWN' => "[0;33m",
    'BLUE' => "[0;34m",
    'CYAN' => "[0;36m",
    'BOLD' => "[1m",
    'UNDERSCORE' => "[4m",
    'REVERSE' => "[7m",
  );
  public static function termcolored($text) {
    $color = self::$_colors['NORMAL'];
    if (func_num_args() > 1) {
      for ($i = 1; $i < func_num_args(); $i++) {
        $color = func_get_arg($i);
        $text = chr(27) . self::$_colors[$color] . $text . chr(27) . "[0m";
      }
    }
    return $text;
  }
  /**
   * Logs a message to stdout, and stores it for later display as well
   *
   * @param  string $loc     The index of the media item being logged about
   * @param  string $message The message to display
   * @param  string $type    the type of message.  Leaving this free form
   *                         on purpose.  Generally, 'notice', 'warning',
   *                         'error', 'info', but there may be others
   *
   * @return NULL
   */
  public static function log($loc, $message, $type = 'error') {
    $msg = new \stdClass;
    $msg->url = $loc;
    $msg->message = $message;
    $msg->type = $type;
    $msg->timestamp = time();
    self::$log[$type][] = $msg;
    self::dump($message, ucfirst($type), $type);
  }
    /**
   * returns log entries generated during processing
   *
   * @param  string $type The type of messages to be retrieved.  One of
   *                      "notice", "warning", "error", "info"
   *
   * @return array       An array of log messages.  This array is keyed
   *                        by type if $type is empty, otherwise it is indexed
   *                        numerically.  Log entries are stdClass items
   *                        with values of 'loc', 'message', and 'type'
   */
  public static function getLogEntries($type = NULL) {
    if (empty(self::$log)) {
      return array();
    }
    if (is_null($type)) {
      $result = array();
      foreach(self::$log as $type=>$arr){
        $result []= $arr;
      }
      return $result;
    }
    return (self::$log[$type]) ?  : array();
  }
  /**
   * prints all log entries to the console
   * @param  string $type An optional entry type to filter by.  Usually 'error'
   * @return NULL
   */
  public function printLogEntries($type=NULL){
    $entries = self::getLogEntries($type);
    $printEntry = function($entry){
      $type = $entry->type;
      unset($entry->type);
      unset($entry->timestamp);
      $entry->url = '['.$entry->url.']('.$entry->url.')';
      Utilities::dump($entry, ucfirst($type), $type);
    };
    foreach($entries as $entry){
      //TODO: Trying to get property of non-object?
      if(is_array($entry)){
        foreach($entry as $e){
          $printEntry($e);
        }
        return;
      }
      if(is_null($type) && $entry->type !== $type){
        continue;
      }
      try{
          $printEntry($entry);
      } catch(\Exception $e){
        U::dump($e->getMessage().", type: ".gettype($entry), "Exception", 'error');
      }
    }
  }
  /**
   * prints a message to the console, and stores it for later sending
   * in email.  This function may cause memory issues if clearLog
   * is not called between migrations.
   *
   * @param  string $msg   the message to be sent
   * @param  string $label An optional label to describe the data
   * @param  string $level one of 'notice' or 'error'
   *
   * @return NULL
   */
  public static function dump($msg, $label = 'Notice', $level = 'ok') {
    global $user;
    $levels = array(
      'ok' => WATCHDOG_INFO,
      'info' => WATCHDOG_INFO,
      'notice' => WATCHDOG_NOTICE,
      'warning' => WATCHDOG_WARNING,
      'error' => WATCHDOG_ERROR,
    );
    if ($msg instanceof \Exception){
      $label = 'Exception';
      $msg = "\n\t".implode("\n\t", array(
        "File: ".$msg->getFile(),
        "Line".$msg->getLine(),
        "Code:".$msg->getCode(),
        "Message:".$msg->getMessage()
      ));
    }
    else if (is_array($msg)) {
      if(empty($msg)){
        self::dump('(empty array)', $label, $level);
        return;
      }
      self::dump('(array)', $label, $level);
      foreach ($msg as $k => $m) {
        if (is_nan(intval($k))) {
          self::dump($m, "\t- ", $level);
        }
        else {
          self::dump($m, "\t$k - ", $level);
        }
      }
      return;
    }
    else if (is_object($msg)){
      self::dump('(object)', $label, $level);
      foreach ($msg as $k => $m) {
        if (is_nan(intval($k))) {
          self::dump($m, "\t - ", $level);
        }
        else {
          self::dump($m, "\t$k - ", $level);
        }
      }
      return;
    }
    $msg = $label . ': ' . $msg;
    switch ($level) {
      case NULL:
      case 'ok':
        //$log_entry['severity'] = 'notice';
      case 'notice':
        //drush_log($msg, $level);
        print $msg . "\n";
        //migrate_watchdog($log_entry);
        break;

      case 'info':
        //drush_log(self::termcolored($msg, 'BLUE'), $level);
        print self::termcolored(self::termcolored($msg, 'BLUE'), 'BOLD') . "\n";
        //migrate_watchdog($log_entry);
      case 'warning':
        //drush_log(self::termcolored($msg, 'YELLOW'), $level);
        print self::termcolored(self::termcolored($msg), 'LIGHT_RED') . "\n";
        //migrate_watchdog($log_entry);
        break;

      case 'error':
        //drush_log(self::termcolored($msg, 'RED', 'BOLD'), $level);
        print self::termcolored(self::termcolored($msg, 'RED'), 'BOLD') . "\n";
        //migrate_watchdog($log_entry);
        break;
    }
  }

  /**
   * Retrieves and clears the log of stored messages for this migration
   *
   * @return {array reference} A reference to the array of stored messages.
   */
  public static function &clearLog() {
    $result = self::$messages;
    self::$messages = array();
    return $result;
  }
  public static function replaceRostersSnippet(&$body_text) {

    preg_match_all("#(<\?php[^\?]+\?>)#", $body_text, $matches);
    //print "Matches: ".print_r($matches, TRUE);
    foreach ($matches[0] as $i => $php_snippet) {
      $original_snippet = $php_snippet;
      // strip delimiters
      $php_snippet = substr($php_snippet, 5, strlen($php_snippet) - 7);
      if (strpos($php_snippet, 'class.rosters.php') === FALSE) {
        continue;
      }
      $output_token_string = '';
      $php_snippet = trim(preg_replace('/\s\s+/', ' ', $php_snippet));
      //print "PHP snippet: " . $php_snippet."\n";
      //convert array to JSON structure
      $php_snippet = preg_replace("/^.*array\(([^\)]+)\).*$/", '{' . "$1" . '}', $php_snippet);
      $php_snippet = str_replace("=>", ':', $php_snippet);
      $php_snippet = str_replace("'", '"', $php_snippet);
      //print "PHP snippet (after): " . $php_snippet."\n";
      $options = (array)json_decode($php_snippet);
      if (!is_array($options)) {
        throw new \Exception("Could not parse out options");
      }
      //print "Options found: ". print_r($options, TRUE)."\n";
      if (isset($options['PeopleID'])) {
        $output_token_string = '[rosters:people/' . $options['PeopleID'] . ']';
      }
      elseif (isset($options['ListID'])) {
        $output_token_string = '[rosters:lists/' . $options['ListID'] . ':group=region]';
      }
      $body_text = str_replace($original_snippet, $output_token_string, $body_text);
      //skip variables for now.  Overly complicated for very little gain.  Let users hand-correct if necessary
    }
  }
  /*
  public static function create_hash_map($rebuild = FALSE) {
    // need direct access to credentials for pdo
    global $databases;
    $hash_map = &drupal_static(__FUNCTION__ . 'hash_map');
    if (!isset($hash_map) || empty($hash_map) || $rebuild) {
      $cache = cache_get(__FUNCTION__ . 'hash_map');
      if (isset($cache) && !empty($cache->data) && !$rebuild) {
        $hash_map = $cache->data;
      }
      else {
        self::dump("Generating media map (intensive operation)...");
        $hash_map = array();
        $hash_query = \Database::getConnection('default', 'migration_map');
        if (!$hash_query) {
          throw new Exception("Could not establish db connection");
        }
        $hash_query = $hash_query->select('migration_map', 'm');
        $hash_query->fields('m', array(
            'file_md5',
            'id',
            'loc',
            'scald_id',
          )
        )->distinct();
        $hash_query->condition('content_type', 'Media Item', '=');
        $hash_query->isNotNull('m.file_md5');
        $hash_query->condition('m.file_md5', '0', '<>');
        //$hash_query->condition('migrate', 'y', '=');
        $hash_query->orderBy('m.loc');
        $query_result = $hash_query->execute();
        //index both by loc and by md5 sum.  Use references for when
        //multiple locations point to an identical file.
        foreach ($query_result as $record) {
          $url_lower = strtolower($record->loc);
          // if($url_lower === 'http://nnlm.gov/images/external.png'){
          //   self::dump("Found url 'http://nnlm.gov/images/external.png'");
          // }
          $existing_entry = $hash_map[$url_lower];
          if (!empty($existing_entry)) {
            if ($existing_entry->file_md5 !== $record->file_md5) {
              throw new \MigrateException("Different md5 signatures for the same location!  Locations:\n URL Lower: $url_lower\nExisting entry: " .
                $existing_entry->loc . "\nEntry md5" . $existing_entry->file_md5 .
                "\nRecord loc" . $record->loc .
                "\nRecord md5: " . $record->file_md5
              );
            }
            if (empty($existing_entry->scald_id)) {
              $hash_map[$url_lower]->scald_id = $record->scald_id;
            }
            continue;
          }

          $existing_entry = $hash_map[$record->file_md5];
          if (!empty($existing_entry) && $existing_entry->loc !== $record->loc) {
            if ($existing_entry->file_md5 !== $record->file_md5) {
              var_dump($existing_entry);
              throw new \MigrateException("Miscategorization found ($record->file_md5). Exiting");
            }
            $hash_map[$url_lower] = &$hash_map[$record->file_md5];
            if (empty($existing_entry->scald_id)) {
              $hash_map[$url_lower]->scald_id = $record->scald_id;
            }
            continue;
          }
          $info = new \stdClass();
          $info->file_md5 = $record->file_md5;
          $info->loc = $url_lower;
          $info->scald_id = (!empty($record->scald_id)) ? $record->scald_id : NULL;
          $info->fid = NULL;
          $hash_map[$info->loc] = $info;
          $hash_map[$info->file_md5] = &$hash_map[$info->loc];
        }
        unset($hash_query);
        $connection = new \PDO('mysql:host=' . $databases['default']['default']['host'] . ';dbname=' . $databases['default']['default']['database'], $databases['default']['default']['username'], $databases['default']['default']['password']);
        $migration_db = $databases['migration_map']['default']['database'];
        $drupal_db = $databases['default']['default']['database'];
        $query_string = "SELECT m.loc, fh.fid, fh.md5
          FROM $migration_db.migration_map m
          INNER JOIN $drupal_db.filehash fh
          ON (fh.md5 = m.file_md5)";
        $stmt = $connection->query($query_string, \PDO::FETCH_ASSOC);
        if ($stmt) {
          foreach ($stmt as $row) {
            $existing_entry = &$hash_map[strtolower($row['loc'])];
            if (!empty($existing_entry)) {
              // we don't care about files that exist in DRupal but not in the mm.
              continue;
            }
            $existing_entry->fid = $row['fid'];
            if (empty($existing_entry->scald_id)) {
              $atom = scald_search(array('base_id' => $row['fid']), FALSE, TRUE);
              if ($atom) {
                $hash_map[$url_lower]->scald_id = $atom->sid;
              }
            }
          }
        }
        else {
          throw new \MigrateException("Could not connect to db: " . print_r($connection->errorInfo(), TRUE));
        }
      }
      //populate items with scald ids where present.  Use PDO directly, as
      //Drupal does not support cross-database joins.


      cache_set(__FUNCTION__ . 'hash_map', $hash_map, 'cache');
    }
    return $hash_map;
  }
  */
  public static function real_filename($fileName, $caseSensitive = FALSE) {
    if (file_exists("$fileName")) {
      return (is_readable($fileName)) ? $fileName : FALSE;
    }
    if ($caseSensitive) {
      return FALSE;
    }

    // Handle case insensitive requests
    $directoryName = dirname($fileName);
    $fileArray = glob($directoryName . '/*', GLOB_NOSORT);
    $fileNameLowerCase = strtolower($fileName);
    foreach ($fileArray as $file) {
      if (strtolower($file) == $fileNameLowerCase) {
        if(!is_readable($file)){
          throw new \MigrateException("File $file exists, but is not readable!");
        }
        return $file;
      }
    }
    return FALSE;
  }
  public static function confirm($output) {
    $output .= ' [ynq]';
    do {
      $value = drush_prompt(dt($output));
      switch ($value) {
        case 'y':
        case 'Y':
          return TRUE;

        case 'n':
        case 'N':
          return FALSE;

        case 'q':
        case 'Q':
          self::printLogEntries();
          try{
            drush_user_abort();
            throw new \MigrateException("Aborting due to user request");
          } catch(\Exception $e){
            self::dump($e);
            die();
          }
          break;

        case FALSE:
        default:
          self::dump("invalid input.");
          $value = FALSE;
      }
    } while ($value === FALSE);
  }

  /**
   * creates a new Scald Atom for a given file.  Also creates any
   * taxonomy terms that do not yet exist in the 'tags' directory.
   */
  public static function create_scald_atom($file, $file_data = array()) {
    static $scald_providers = array(
      'image' => 'scald_image',
      'audio' => 'scald_audio',
      'file' => 'scald_file',
      'flash' => 'scald_flash',
      'video' => 'scald_youtube',
    );
    static $sections = NULL;
    if (is_null($sections)) {
      $sections = nnlm_core_get_editorial_sections(array('tid' => TRUE));
    }
    //self::dump($file, "File");
    $file_data = (object)($file_data + array(
        'field_origin_url' => '',
        'field_tags' => array(),
        'field_migration_notes' => '',
        'field_section' => 'all_regions',
      ));
    if (is_string($file_data->field_section)) {
      $file_data->field_section = $sections[$file_data->field_section];
    }
    //self::dump($file_data, "File data");
    $type = $file->type;
    if (!in_array($type, array_keys($scald_providers))) {
      $type = 'file';
    }
    static $tags_vid = NULL;
    if (is_null($tags_vid)) {
      $tags_vid = taxonomy_vocabulary_machine_name_load('tags')->vid;
    }
    try {
      $tags = self::get_tags($row->tags);
      foreach ($tags as $name => $tid) {
        if (empty($tid)) {
          $new_term = new \stdClass();
          $new_term->vid = $tags_vid;
          $new_term->name = $name;
          $new_term->vocabulary_machine_name = 'tags';
          taxonomy_term_save($new_term);
          self::dump($new_term->tid, "Saved new term $name");
          $tags[$name] = $new_term->tid;
        }
      }
      //self::dump(array_values($tags), "Assiging tags");
      $atom = new \ScaldAtom($type, NULL, array(
          'title' => (isset($file->title)) ? $file->title : basename($file->value),
          'publisher' => $file->uid,
          'provider' => $scald_providers[$type],
          'base_id' => $file->fid,
        ));
      switch ($type) {
        case 'file':
          $atom->scald_file[LANGUAGE_NONE][0] = (array)$file;
          $atom->scald_file[LANGUAGE_NONE][0]['display'] = 1;
          break;

        default:
          $atom->scald_thumbnail[LANGUAGE_NONE][0]['fid'] = $file->fid;
          break;
      }
      $atom = $atom->save();
      //self::dump($atom, "New atom");
      $w = entity_metadata_wrapper('scald_atom', $atom, array("bundle" => $type));
      $w->field_origin_url->set(array('url' => $file_data->field_origin_url));
      $w->field_tags->set($file_data->field_tags);
      $w->field_migration_notes->set($file_data->field_migration_notes);
      $w->field_section->set($file_data->field_section);
      $w->save();
      return $atom;
    }
    catch(\Exception$e) {
      self::dump($e->getMessage(), "Exception (type $type, Origin url $file_data->field_origin_url");
      return NULL;
    }
  }

  /**
   * Builds a map of Media Item content in the staging area as recorded in the
   * drupal_migration database.  This map is used during the import
   * process to determine duplicates and already-imported content (beyond
   * the use of highwater marks, such as in BasicNodeMigration).
   *
   * The map considers items that have an identical md5 signature to be equal.
   * As such, it only records one such object instance when encountered, and
   * any subsequent mapped items will point to that same instance.  This
   * reduces the amount of actual file content that needs to be migrated.
   * Entries that do not exist in Drupal in any form will only have the md5 hash
   * present for the file, and a 0 if the file did not exist on the filesystem
   * during the last filesystem crawl.
   *
   * @param  boolean $imported_entries_only Exclude map entries that are
   *                                        not already imported into D
   *
   * @return array  The built Media Item map.
   */
  public static function create_atom_map() {
    $url_map = &drupal_static(__FUNCTION__ . 'url_map');
    if (!$url_map) {
      $hash_map = self::create_hash_map();
      $atoms = entity_load('scald_atom', FALSE, array());
      foreach ($atoms as $i => $atom) {
        try {
          $w = entity_metadata_wrapper("scald_atom", $atom, array('bundle' => $atom->type));
          $md5_hash = $w->scald_thumbnail->value();
          if (empty($md5_hash)) {
            $md5_hash = $w->scald_file->value();
          }
          $md5_hash = $md5_hash['filehash']['md5'];
          //self::dump($md5_hash, "Hash value");
          if (empty($md5_hash)) {
            self::dump($w->label(), "Missing hash");
            throw new \Exception("Could not find hash for atom " . $w->label());
          }
          $origin_url = $w->field_origin_url->url->value();

          foreach ($hash_map as $url => $hash) {
            if ($hash === 0) {
              //not interested in urls with no file backing.
              continue;
            }
            if ($hash == $md5_hash) {
              $url_map[strtolower($url)] = $atom->sid;
            }
          }
        }
        catch(\Exception$e) {
          continue;
        }
      }
      unset($atoms);
      unset($hash_map);
    }
    //self::dump(array_slice($url_map, 0, 10), "Url map slice");
    if (empty($url_map)) {
      return array();
    }
    return $url_map;
  }

  /**
   * Converts decimal entity string to character
   *
   * @param  string $t a string containing one or more decimal entity chars
   *
   * @return string The decoded string.
   */
  public static function decode_entity($t) {
    $convmap = array(0x0, 0x2FFFF, 0, 0xFFFF);
    return mb_decode_numericentity($t, $convmap, 'UTF-8');
  }

  /**
   * retrieves the password for a username established in the 'secret' file. This
   * file is a space-separated username/password list, established separately to
   * keep it out of the git repository.
   *
   * @param  String $username The username to look up
   *
   * @return mixed           String password if the username was found, false
   *                         otherwise.
   */
  public static function get_credentials($username) {
    $secret_credentials_file = drupal_get_path('module', 'nnlm_core') . '/.secret.php';
    if (!file_exists($secret_credentials_file)) {
      throw new \Exception("cannot continue; credentials file not present");
    }
    if (!is_readable($secret_credentials_file)) {
      throw new \Exception("cannot continue; credentials file not readable");
    }
    $fp = fopen($secret_credentials_file, 'r');
    while ($line = fgets($fp)) {
      $arr = preg_split("/ /", $line);
      if (count($arr) < 2) {
        continue;
      }
      $u = trim($arr[0]);
      $p = trim($arr[1]);
      if ($u === $username) {
        return $p;
      }
    }
    return FALSE;
  }

  /**
   * Returns what this nnlm.gov url would be if it were a relative link.
   * Careful. Not much error checking.
   *
   * @param  string $url The nnlm.gov url
   *
   * @return string      The relative url, trimmed of any trailing slash
   */
  public static function relative_url($url) {
    if (preg_match('|^http|', $url) && !preg_match('|http://nnlm.gov|', $url)) {
      //not local.  Skip
      return FALSE;
    }
    return str_replace('http://nnlm.gov', '', trim($url, '/'));
  }
  public static function normalizePath($path) {
    // Remove any kind of funky unicode whitespace
    $normalized = preg_replace('#\p{C}+|^\./#u', '', $path);

    // Path remove self referring paths ("/./").
    $normalized = preg_replace('#/\.(?=/)|^\./|\./$#', '', $normalized);

    // Regex for resolving relative paths
    $regex = '#\/*[^/\.]+/\.\.#Uu';

    while (preg_match($regex, $normalized)) {
      $normalized = preg_replace($regex, '', $normalized);
    }

    if (preg_match('#/\.{2}|\.{2}/#', $normalized)) {
      throw new LogicException('Path is outside of the defined root, path: [' . $path . '], resolved: [' . $normalized . ']');
    }

    return rtrim($normalized, '/');
  }

  /**
   * Provides the absolute form of an nnlm.gov url
   *
   * @param  string $url The url to be normalized
   * @param  string $loc The url of the domain name
   *
   * @return string      the normalized url
   */
  public static function absolute_url($url, $loc = 'http://nnlm.gov') {
    $url_parts = parse_url($url, PHP_URL_HOST);
    // standardize with / on the end
    $loc = rtrim($loc, '/') . '/';
    // Is it a relative link (URI)?
    if ($url_parts === FALSE || !isset($url_parts['host']) || ($url_parts['host'] == '')) {
      // It is, so prepend our base URL
      //self::dump($loc, "Loc");
      //self::dump($url, "Normalizing.  Original URL");
      //self::dump($loc, "Base url");
      if ($url[0] === '/') {
        //change to be relative to site root.
        $loc = 'http://nnlm.gov';
      }
      $url = self::normalizePath($loc . $url);

      //self::dump($url, "Absolute URL");
    }
    return $url;
  }
  private static function writeToFile($filename, $str) {
    if (strlen($str) === 0) {
      throw new \Exception("attempted to write a zero lenth string!  Filename: $filename.");
    }
    $fp = fopen('./' . $filename, 'w');
    if (!$fp) {
      throw new \Exception("could not open file $filename for writing in directory " . getcwd());
    }
    if (fwrite($fp, $str) === FALSE) {
      echo "Cannot write to file ($filename)";
      exit;
    }
    fclose($fp);
  }

  /**
   * Takes a block of page body content, and runs it through HTMLPurifier, Tidy,
   * and some other methods to ensure sanity
   *
   * @param  string $content The page body content
   * @param  string $url     The url the content came from.  Used to make sure
   *                         all local URLs in the page are absolute (used later
   *                         for media replacement)
   *
   * @return string          The purified content
   */
  public static function purify($content, $url) {
    $purifier = &drupal_static(__FUNCTION__);
    $html_purifier_config = array(
      'Attr.AllowedRel' => array('nofollow', 'help', 'alternate', 'author', 'bookmark'),
      // we must set it empty, so that html purifier does not autofill with inappropriate
      'Attr.DefaultImageAlt' => '',
      'Attr.EnableID' => TRUE,
      'AutoFormat.RemoveSpansWithoutAttributes' => TRUE,
      'CSS.AllowImportant' => TRUE,
      // allows edge case css
      'CSS.AllowTricky' => TRUE,
      // Contentious
      'CSS.Proprietary' => TRUE,
      // allows edge case css
      'CSS.Trusted' => TRUE,
      // TODO: eventually, this might be useful.  Not ready for it yet.
      //'Core.CollectErrors'=>TRUE,
      'Core.ConvertDocumentToFragment' => TRUE,
      // instead of removing
      //'Core.EscapeInvalidChildren'=>TRUE,
      // instead of removing
      //'Core.EscapeInvalidTags'=>TRUE,
      // Contentious
      'Core.RemoveInvalidImg' => FALSE,
      // Kills PHP
      'Core.RemoveProcessingInstructions' => TRUE,
      // this is default, but remember this one!
      //'Filter.ExtractStyleBlocks.Scope'=>NULL,
      // TODO: move this style to the style field.
      //'Filter.ExtractStyleBlocks'=>TRUE,
      // "HTML 4.01 Transitional", "HTML 4.01 Strict", "XHTML 1.0 Transitional", "XHTML 1.0 Strict", "XHTML 1.1"
      'HTML.Doctype' => 'XHTML 1.0 Transitional',
      // "none", "light", "medium", "heavy",
      'HTML.TidyLevel' => 'light',
      // comments are stripped without this.
      'HTML.Trusted' => TRUE,
      // adds IE fixes for flash
      'Output.FlashCompat' => TRUE,
      //'Output.SortAttr'=>TRUE,
      'Output.TidyFormat' => TRUE,
      // superseded by URI.Host?
      'URI.Base' => 'nnlm.gov',
      'URI.DefaultScheme' => 'http',
      // Contentious.  Allow hotlinking or not?
      'URI.DisableExternalResources' => FALSE,
      'URI.Host' => 'nnlm.gov',
      // this is mandatory.  Links like '../foo.jpg' need to be squashed.
      'URI.MakeAbsolute' => TRUE,
    );
    if (!isset($purifier)) {
      // Composer dependency
      $purifier = new \HTMLPurifier();
    }
    $config = \HTMLPurifier_Config::createDefault();
    foreach ($html_purifier_config as $k => $v) {
      $config->set($k, $v);
    }
    $config->set('URI.Base', $url);
    return $purifier->purify($content, $config);
  }

  /**
   * fetchContent retrieves a file from the staging area, and pulls the
   * content from between the 'main' div tags.  It utilizes the helper
   * function processNode to actually extract the content.
   *
   * @param String $loc The url of the file to extract. Must begin with
   * http://nnlm.gov (as anything else would not be in our staging area...right?)
   *
   * @return StdClass an object with the following properties:
   *   original_html: the original html in the body
   *   body:  the body after being cleaned for import
   *   files: absolute urls to any files linked in the page body (on nnlm.gov).  Query strings
   *   and anchors are removed.
   *   links: absolute urls to any nnlm.gov pages linked in the page body.  Includes
   *   files.
   */
  public static function fetchContent($loc) {
    $allowed_file_types = array('jpg', 'png', 'gif', 'doc', 'ppt', 'xls', 'tar', 'tgz', 'zip', 'docx', 'pptx', 'xlsx', 'psd', 'rtf', 'pdf');

    $result = new \stdClass;
    $result->DOM = NULL;
    // see @return value above.
    $result->links = array();
    $result->references = array();
    // see @return value above.
    $result->files = array();
    // see @return value above
    $result->original_html = NULL;
    $result->body = "
      <h1 style='color:red'>Error during import</h1>
      <p>The migration script encountered one or more errors during the import of this
      content.  Please note that the script is only set up to handle pages
      that are build on top of NN/LM templates, which possess markers that
      demarcate the beginning and ending of body content.  This did not
      appear to be the case here - this page must be migrated by hand.</p>
    //overwrite default once it becomes available.
      ";
    // Flag to caller if there was a problem. overwrite once we know everything's cool.
    $result->error = TRUE;
    // Indicates to caller the nature of the problem.
    $result->errmsg = 'Default error message';

    $fs_loc = self::url_to_filepath($loc);

    try {
      if (!file_exists($fs_loc)) {
        throw new \Exception("File $fs_loc does not exist on the filesystem");
      }
      if (is_dir($fs_loc)) {
        throw new \Exception("File $fs_loc is a directory, and could not be imported");
      }
      if (!is_readable($fs_loc)) {
        throw new \Exception("File $fs_loc is not readable");
      }
      $result->original_html = file_get_contents($fs_loc);
      if (empty($result->original_html)) {
        throw new \Exception("Original html could not be obtained for file $fs_loc");
      }
      //extract php rosters snippets

      self::replaceRostersSnippet($result->original_html);

      libxml_use_internal_errors(TRUE);
      if (!$result->DOM = \DOMDocument::loadHTML($result->original_html)) {
        throw new \Exception("Could not load html into parser");
      }
      /*self::dump($DOM->saveHTML(), "Original html");
      self::dump($DOM->doctype->name, "Doctype name");
      self::dump($DOM->doctype->publicId, "Doctype public id");
      self::dump($DOM->doctype->systemId, "Doctype system id");*/









      //
      $extracted_node = $result->DOM->getElementById('main');
      if (!$extracted_node) {
        $extracted_node = $result->DOM->getElementsByTagName('body')->item(0);
      }
      if (empty($extracted_node)) {
        throw new \Exception("Content of $loc could not be parsed");
      }
      $extracted_node = $extracted_node->parentNode->removeChild($extracted_node);
      while ($result->DOM->hasChildNodes()) {
        // empty the DOM
        $result->DOM->removeChild($result->DOM->firstChild);
      }
      //add extracted back
      $result->DOM->appendChild($extracted_node);

      /**
       * Processing_fn processes all links in body content, setting the
       * attribute 'filekey' and 'extfilekey' for links to internal
       * and external content, for easier extraction later.
       * @var function
       */
      $processing_fn =
      function (&$dom_element, $attr_name)
      use ($result, $allowed_file_types) {
        $allowed_external_domains = array('youtube.com');
        $actual_url = $dom_element->getAttribute($attr_name);
        if (empty($actual_url)) {
          return;
        }
        $result->links[$actual_url] = $actual_url;
        if (Utilities::relative_url($actual_url) === FALSE) {
          //not local, we *might* still modify...
          foreach ($allowed_external_domains as $domain) {
            if (strpos($actual_url, $domain) !== FALSE) {
              //removed this line until I actually start stripping these in the migration.
              //$dom_element->setAttribute('extfilekey', $actual_url);
              break;
            }
          }
          return;
        }
        $suffix = trim(substr($actual_url, -4), '.');
        if (in_array($suffix, $allowed_file_types)) {
          $result->files[$actual_url] = $actual_url;
          $dom_element->setAttribute('filekey', $actual_url);
        }
      };
      foreach ($result->DOM->getElementsByTagName('a') as $dom_element) {
        $processing_fn($dom_element, 'href');
      }
      foreach ($result->DOM->getElementsByTagName('img') as $dom_element) {
        $processing_fn($dom_element, 'src');
      }
      $result->error = FALSE;
      $result->errmsg = '';
    }
    catch(\Exception$e) {
      self::log($loc, $e->getMessage(), 'error');
      $result->error = TRUE;
      $result->errmsg = $e->getMessage();
    }
    return $result;
  }

  /**
   * reuturns the filesystem location of a url on november.hsl.washington.edu.
   *
   * @param String $url
   *    The url location of the item. Must be local to nnlm.gov.
   *    Note: tidy function implicit in migrations ensures the input
   *    to this function is an absolute url.
   *  @todo Buff this method out to protect against illegal accesses.
   */
  public static function url_to_filepath($url) {
    $basic_page_suffixes = array('html', 'php', 'htm', 'php3');
    //echo "Server hostname: ".$_SERVER["HOSTNAME"]."\n";

    $november_filesystem_url = rawurldecode(str_replace("http://nnlm.gov", "/var/www/html", $url));
    //if the url is to a folder, assume apache defaults for an index page.
    //Iterate through the possibilities, until a match is found.
    if (substr($november_filesystem_url, -1) == '/') {
      foreach ($basic_page_suffixes as $suffix) {
        if (file_exists($november_filesystem_url . 'index.' . $suffix)) {
          $november_filesystem_url .= 'index.' . $suffix;
        }
      }
    }
    return $november_filesystem_url;
  }

  /**
   * Prints execution time
   *
   * @param  string $label A label to present in front of the elapsed time. Invoke with NULL to reset the timer
   *
   * @return NULL
   */
  public static function e_time($group = 'default', $msg = NULL, $label = "Timer", $level = 'notice') {
    static $execution_start = NULL;
    if ($execution_start === NULL || $group === NULL) {
      $execution_start = array('default' => microtime(TRUE));
      return;
    }
    if ($msg === NULL) {
      $execution_start[$group] = microtime(TRUE);
      return;
    }
    $execution_end = microtime(TRUE);
    $diff = (float)$execution_end - (float)$execution_start[$group];
    $execution_start[$group] = $execution_end;
    if ($diff > 5) {
      $level = 'warning';
    }
    if ($diff > 10) {
      $level = 'error';
    }
    $diff = round($diff, 4);
    $msg .= " ($diff seconds)";
    self::dump($msg, $label, $level);
  }

  /**
   * Effectively prints the memory usage gain between invocations.
   */
  public static function memoryUsage() {
    if (self::$initial_memory_usage == 0) {
      self::$initial_memory_usage = memory_get_usage();
    }
    $current_memory_usage = memory_get_usage();
    $peak_memory_usage = memory_get_peak_usage();
    $diff = $current_memory_usage - self::$initial_memory_usage;
    $diff_peak = $peak_memory_usage - self::$initial_memory_usage;
    if ($diff == 0) {
      return;
    }
    if ($current_memory_usage > pow(1024, 3)) {
      $current_memory_usage = round($current_memory_usage / pow(1024, 3)) . 'GB';
    }
    elseif ($current_memory_usage > pow(1024, 2)) {
      $current_memory_usage = round($current_memory_usage / pow(1024, 2)) . 'MB';
    }
    elseif ($current_memory_usage > pow(1024, 1)) {
      $current_memory_usage = round($current_memory_usage / pow(1024, 1)) . 'KB';
    }
    else {
      $current_memory_usage = $current_memory_usage . ' bytes';
    }

    if ($diff > pow(1024, 3)) {
      $usage = round($diff / pow(1024, 3)) . 'GB';
    }
    elseif ($diff > pow(1024, 2)) {
      $usage = round($diff / pow(1024, 2)) . 'MB';
    }
    elseif ($diff > pow(1024, 1)) {
      $usage = round($diff / pow(1024, 1)) . 'KB';
    }
    else {
      $usage = $diff . ' bytes';
    }

    if ($diff_peak > pow(1024, 3)) {
      $peak_usage = round($diff_peak / pow(1024, 3)) . 'GB';
    }
    elseif ($diff_peak > pow(1024, 2)) {
      $peak_usage = round($diff_peak / pow(1024, 2)) . 'MB';
    }
    elseif ($diff_peak > pow(1024, 1)) {
      $peak_usage = round($diff_peak / pow(1024, 1)) . 'KB';
    }
    else {
      $peak_usage = $diff_peak . " bytes";
    }
    $limit = ini_get('memory_limit');
    self::dump("$current_memory_usage (diff: $usage (peak diff: $peak_usage, limit: $limit)", "Memory usage");
  }

  /**
   * Provides the region code from the general database for a given regional
   * shortname
   *
   * @param  string $region_shortname The short name of the region (e.g.
   *                                  'pnr', 'psr', 'national')
   *
   * @return string                   The region code (as a string)
   */
  public static function getRegionCode($region_shortname) {
    $region_code = \Database::getConnection('default', 'general')->query('SELECT region_code FROM {regions} WHERE abbreviation = :abbr', array(':abbr' => strtoupper($region_shortname)))->fetchField();
    if (!is_null($region_code)) {
      return (string) $region_code;
    }
    // WebSTOC region code by default.
    return NULL;
  }

  /**
   * Provides a list of taxonomy terms and associated vids from the 'tags'
   * vocabulary
   *
   * @param  array  $tag_names An optional list of tags to filter the
   *                           result by.
   *
   * @return array  A hash of tags with the tag name as key, and tid as value,
   *                  or an empty array if the filters were too restrictive
   *                  or if something else goes wrong.
   */
  public function get_tags($tag_names = array()) {
    if (empty($tag_names)) {
      return array();
    }
    $tags = &drupal_static(__FUNCTION__);
    if (!$tags || empty($tags)) {
      if ($range_vocab = taxonomy_vocabulary_machine_name_load('tags')) {
        $tree = taxonomy_get_tree($range_vocab->vid);
        foreach ($tree as $tid => $o) {
          $tags[$o->name] = $o->tid;
        }
        unset($tree);
      }
    }
    if (is_string($tag_names)) {
      $tag_names = array_map(
      function ($o) {
          return trim($o);
      }
        ,
        explode(",", $tag_names)
      );
    }
    $result = array();
    foreach ($tag_names as $name) {
      $result[$name] = $tags[$name];
    }
    return $result;
  }

  /**
   * For easy retrival of roleid for a given role during import.
   *
   * @param  String $role_name The name of the role to be retrieved.
   *
   * @return Int            The Drupal $rid of the role.
   */
  public static function get_role_id($role_name) {
    list($rid) = db_query("SELECT r.rid FROM {role} r WHERE r.name = :rname", array(':rname' => $role_name))->fetchCol();
    return $rid;
  }

  /**
   * for testing only.  Remove upon module completion
   *
   * @return void
   */
  public static function run_test() {
    //self::dump("Running migration testing function");
  }

  /**
   * cron-invoked function to run a migration by migration name.  Emails
   * abeal@uw.edu with migration results.
   * @deprecated Not used.
   *
   * @param  string $migration_name The name of the migration to run
   *
   * @return NULL
   */
  /*
  public static function cronMigration($migration_name) {
    //load composer additions.
    // Get migration object


    //email me with cron results
    try {
      $migration = \Migration::getInstance($migration_name);
      if (!$migration) {
        drush_set_error(dt('Unrecognized migration invoked: !cn', array('!cn' => $migration_name)));
        return FALSE;
      }
      switch ($migration->getStatus()) {
        case \MigrationBase::STATUS_IDLE:
          $result = $migration->processImport();
          break;

        case \MigrationBase::STATUS_IMPORTING:
        case \MigrationBase::STATUS_ROLLING_BACK:
        case \MigrationBase::STATUS_STOPPING:
          throw new \Exception(dt("ERROR: The migration $migration_name is currently busy.  Skipping"));
          break;

        case \MigrationBase::STATUS_DISABLED:
        default:
          throw new \Exception(dt("ERROR: The migration $migration_name is either missing or disabled"));
          break;
      }
      $messages = &self::clearLog();
      $count = count($messages);

      $email_body = "Migration $migration_name executed. Message count: $count.\nBEGIN\n";
      $email_body .= implode("\n", $messages);
      $email_body .= "\nEND\n";
      // Give it a body
      // And optionally an alternative body
      // Composer dependency
      $message = \Swift_Message::newInstance()->setCharset('UTF-8')->setPriority(3)->setSubject('Drupal cron: migration executed')->setFrom(array('webstoc@gmail.com' => 'WebSTOC'))->setTo(array('abeal@uw.edu'))->setBody($email_body);
      $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")->setUsername('webstoc@gmail.com')->setPassword('AyujH4o8HnWS');
      $mailer = \Swift_Mailer::newInstance($transport);
      $result = $mailer->send($message);
      if (!$mailer->send($message, $failures)) {
        throw new \Exception("Could not send mail: " . print_r($failures, TRUE));
      }
    }
    catch(\Exception$e) {
      die($e->getMessage());
    }
  }
  */
}

