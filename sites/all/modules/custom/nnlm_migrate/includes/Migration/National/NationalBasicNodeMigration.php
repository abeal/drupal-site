<?php
namespace NNLM\Migration\Migration\National;
use NNLM\Migration\Migration\BasicNodeMigration as BasicNodeMigration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
class NationalBasicNodeMigration extends BasicNodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    $this->description = t("National Basic Node Migration.");
    $this->setEditorialSection('national');

    $this->buildBaseQuery();
    //$query->condition('m.loc', 'http://nnlm.gov/' . $this->section->name . '/%', 'LIKE');

    $this->query->condition(db_or()
      ->condition('m.loc', 'http://nnlm.gov/rsdd/%', 'LIKE')
      ->condition('m.loc', 'http://nnlm.gov/outreach/%', 'LIKE')
      ->condition('m.loc', 'http://nnlm.gov/hip/%', 'LIKE')
      //->condition('m.loc', 'http://nnlm.gov/funding/%', 'LIKE')
    );
    parent::__construct($arguments);
    //$this->printQuery();
    //replace relevant field mappings
    //NONE for national - uses defaults
  }
}
/*
<?php
namespace NNLM\Migration\Migration\Training;
use NNLM\Migration\Migration\BasicNodeMigration as BasicNodeMigration;
use NNLM\Migration\Utilities as U;

class TrainingBasicNodeMigration extends BasicNodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    $this->description = t("Training Basic Node Migration.");
    $this->setEditorialSection('training');
    $this->buildBaseQuery();
    $this->query->condition(db_or()
      ->condition('m.loc', 'http://nnlm.gov/training/%', 'LIKE')
      //OR???
    );
    parent::__construct($arguments);
    //$this->query->range(0,1);

    //replace relevant field mappings
    //NONE for Training - uses defaults
  }
}

 */
