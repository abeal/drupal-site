<?php
namespace NNLM\Migration\Migration\SCR;
use NNLM\Migration\Migration\BasicNodeMigration as BasicNodeMigration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
class SCRBasicNodeMigration extends BasicNodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->setEditorialSection('scr');
    //Replace query with custom condition for this migration
    $query = $this->getSource()->query();
    /**
     * Command line options:
     *   -u, --url: restrict to single url.  Can be any url within this
     *   migration.  Restricts migration to processing ONLY that url.  Must be
     *   within the migration - cannot do a random url.
     */

    $query->condition('m.loc', 'http://nnlm.gov/' . $this->section->name . '/%', 'LIKE');
    //$db_or = db_or();

    //$db_or->condition('m.loc', 'http://nnlm.gov/scr/about/', '=');
    //$db_or->condition('m.loc', 'http://nnlm.gov/scr/about/welcome.html', '=');
    //$query->condition($db_or);
    //recreate the migration source based on the updated query
    $this->source = new \MigrateSourceSQL($query, $this->getSource()->fields());
  }
}
