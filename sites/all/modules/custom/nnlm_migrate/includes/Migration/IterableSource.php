<?php
namespace NNLM\Migration\Migration;
use NNLM\Migration\Utilities as U;
/**
 *   Defines a migration source that uses an internal iterator.  We must write
 *   our own MigrateSource class, as we are pulling from custom flat files.
 */
abstract class IterableSource extends \MigrateSource {
  protected $iterator = NULL;

  /**
   *  provides field names for interface
   *
   *  This is a required function for any implementing subclass.
   *  This function provides a default if the subclass has not yet implemented
   */
  public function fields() {
    if ($this->computeCount() == 0) {
      return array();
    }
    $sample = $this->getNextRow();
    $this->performRewind();
    $result = array();
    foreach ($sample as $k => $v) {
      $result[$k] = ucfirst($k);
    }
    return $result;
  }

  /**
   *
   * @return
   *  The number of entries in the source
   */
  public function computeCount() {
    return $this->iterator->count();
  }

  /**
   * rewinds the source to the beginning
   **/
  public function performRewind() {
    $this->iterator->rewind();
  }

  /**
   *
   * @return
   *    The next entry, or FALSE if no more entries
   */
  public function getNextRow() {
    if (!$this->iterator->valid()) {
      U::msg(t("Completed"));
      return FALSE;
    }
    $result = $this->iterator->current();
    $this->iterator->next();
    return $result;
  }
}
