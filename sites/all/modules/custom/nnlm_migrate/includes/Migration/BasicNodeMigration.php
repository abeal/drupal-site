<?php
namespace NNLM\Migration\Migration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
abstract class BasicNodeMigration extends NodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    //ensure certain necessary items are set before calling parent constructor.
    if(is_null($this->section)){
      $this->setEditorialSection("national");
    }
    if(is_null($this->query)){
      $this->buildBaseQuery();
    }
    if(is_null($this->description)){
      $this->description = t("Basic Node Migration.");
    }

    $this->query->condition('content_type', 'Basic Page', '=');
    $this->destination = new \MigrateDestinationNode('basic_node',
      \MigrateDestinationNode::options(
        'en',
        'utf-8'
      )
    );
    $this->addUnmigratedDestinations(array(
        'field_node_styles:format',
      ), t('Do Not Migrate'));
    parent::__construct($arguments);
  }
}
