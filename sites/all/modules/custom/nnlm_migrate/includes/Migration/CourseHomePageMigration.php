<?php
namespace NNLM\Migration\Migration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
abstract class CourseHomePageMigration extends NodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    //ensure certain necessary items are set before calling parent constructor.

    if(is_null($this->section)){
      $this->setEditorialSection("national");
    }
    if(is_null($this->query)){
      $this->buildBaseQuery();
    }
    if(is_null($this->description)){
      $this->description = t("Course Home Page migration.");
    }
    //Note - the below may need to be changed to 'Course Home Page'
    //if the migration map changes.
    $this->query->condition('content_type', 'Training Class', '=');
    $this->destination = new \MigrateDestinationNode('course_home_page',
      \MigrateDestinationNode::options(
        'en',
        'utf-8'
      )
    );
    $this->addUnmigratedDestinations(array(
        'field_course_developer',
        'field_course_developer:nnlm_rosters_people_id',
        'field_external_course_materials',
        'field_external_course_materials:title',
        'field_external_course_materials:attributes',
        'field_external_course_materials:language',
        'field_internal_course_materials',
        'field_internal_course_materials:options',
        'field_mla_clearinghouse_entry',
        'field_mla_clearinghouse_entry:title',
        'field_mla_clearinghouse_entry:attributes',
        'field_mla_clearinghouse_entry:language'
      ), t('Do Not Migrate'));
    parent::__construct($arguments);
  }
}
