<?php

// $Id$

namespace NNLM\Migration\Migration;
use NNLM\Migration\Utilities as U;
use NNLM\Migration\MediaMap2 as MediaMap;
use NNLM\Migration\Utility\MenuTree as MenuTree;

/**
 * All actual basic node migration classes extend this one.
 */
abstract class NodeMigration extends \Migration
{

  // ?
  protected $workbench_access_id = NULL;

  // ?
  protected $path_lookup = NULL;

  // ?
  protected $tid_lookup = NULL;
  protected $menu_name = NULL;

  //string
  // populate with menu entries that we create afterwards.
  protected $links = array();
  protected $home_link = NULL;

  //array
  protected $region = NULL;

  //editorial section  all content should be owned by.
  protected $section = NULL;

  //taxonomy term object.
  //a list of file ids that need to be rerun in the files migration.  Populated
  //during link replacement as broken links to files are discovered.
  protected $broken_files = array();
  protected $source_fields = array();
  protected $processed_urls = array();

  // migration messages
  protected $messages = array();
  protected $media_problems = array();

  protected $rebuild_ignored_items = FALSE;
  /**
   * In addition to normal migration setup, the below instantiates several
   * special-use objects.  In particular, the media map is a construct
   * designed to prevent importing of media that already exists in the
   * existing site (calculated by md5 sum).  This migration also imports
   * media on the fly when it is missing.
   *
   * This migration should only ever be run via drush.
   * @param array $arguments Used by the parent class only.
   */
  public function __construct($arguments) {

    //U::dump($arguments, "Arguments");
    parent::__construct($arguments);
    $this->team = array(new \MigrateTeamMember('Aron Beal', 'abeal@uw.edu', t('Implementor')));

    //check for proper setup by subclass.
    foreach (array('description', 'menu_name', 'query', 'section') as $prop) {
      if (is_null($this->{$prop})) {
        throw new \Exception("Migration $prop was not set in subclass");
      }
    }

    //mapping ties the 'id' attribute in the source to the nid in the target.
    $this->map = new \MigrateSQLMap($this->machineName,

    //schema definition for the primary key of the source.
    array('id' => array('type' => 'int', 'not null' => TRUE, 'description' => 'Unique migration map id.', 'alias' => 'm',),),

    //schema definition for the primary key of the destination
    \MigrateDestinationNode::getKeySchema(), 'default',

    // allows use of highwater marks
    array('track_last_imported' => TRUE));
    $this->source_fields = array('author' => t('The Drupal user ID of the account that created the node. Same as the rosters PeopleID'), 'body' => t('The content of the page, to be inserted in the Drupal body field'), 'comment' => t('Whether or not this node allows comments.  By default, all entries but wordpress blog entries will have this set to 0.'), 'drupal_node_id' => t('Drupal Node ID (set after import, not canonical!)'), 'field_section' => t("The editorial section this page belongs to.  Used for workbench."), 'field_tags' => t("The 'free form' taxonomy tags to apply to this content"), 'hide' => t("Whether or not to hide this record from the migration process.  These records (when this field is true) are never processed, regardless of the migration setting"), 'id' => t('ID'), 'language' => t('Language.  Always set to be English (at this point)'), 'loc' => t('URL to original content on nnlm.gov (unique)'), 'migrate' => t('Whether or not this content has been marked for migration'), 'notes' => t("Notes field from migration map.  Used for keeping migration notes."), 'sticky' => t("Whether or not this node is sticky (stays at the top of lists)."), 'title' => t('Title'),);

    // TIP: By default, each time a migration is run, any previously unimported source items
    // are imported (along with any previously-imported items marked for update). If the
    // source data contains a timestamp that is set to the creation time of each new item,
    // as well as set to the update time for any existing items that are updated, then
    // you can have those updated items automatically reimported by setting the field as
    // your highwater field.
    $this->highwaterField = array(

    // Column to be used as highwater mark
    'name' => 'highwater_mark',

    // Table alias containing that column
    'alias' => 'hm',

    // [drupal database].migrate_map_[migration name] and
    // drupal_migration.highwater_mark.highwater_mark should both be unix
    // timestamp fields
    //'type' => 'int',
    );
    $this->source = new \MigrateSourceSQL($this->query, $this->source_fields);
    foreach (array('title', 'language', 'comment', 'body') as $field) {
      $this->addFieldMapping($field, $field)->description($this->source_fields[$field]);
    }

    //$this->addFieldMapping('field_section:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_section:ignore_case')->defaultValue(FALSE);
    $this->addFieldMapping('field_section:create_term')->defaultValue(FALSE);

    //add taxonomy tags in a manner that allows comma separation
    $this->addFieldMapping('field_tags', 'field_tags')->separator(',')->description($this->source_fields['field_tags']);
    $this->addFieldMapping('field_tags:ignore_case')->defaultValue(TRUE);
    $this->addFieldMapping('field_tags:create_term')->defaultValue(TRUE);

    //reset this line in derived class.
    // section taxonomy assignment
    // $this->addFieldMapping(destination, source=NULL, warnonoverride=TRUE)
    // default value for imported items is unpublished.
    $this->addFieldMapping("status")->defaultValue(0);

    // "promoted to front page" isn't really relevant in our case.
    $this->addFieldMapping("promote")->defaultValue(0);

    //$this->addFieldMapping('field_section:ignore_case')->defaultValue(TRUE);
    //$this->addFieldMapping('field_section:create_term')->defaultValue(FALSE);
    $this->addFieldMapping('field_migration_notes', 'notes')->description($this->source_fields['notes']);

    // $this->addFieldMapping('field_migration_notes:format')->defaultValue('plain_text');
    // $this->addFieldMapping("field_migration_notes:language")->defaultValue("en");
    // $this->addFieldMapping("body:language")->defaultValue("en")->description(t('Setting the language field as part of the promotion
    //       is beyond the scope of what a migration can provide; any non-english
    //       pages must be so marked by hand after the migration is complete'));
    $this->addFieldMapping('field_origin_url', 'loc')->defaultValue("")->description($this->source_fields['loc']);
    $this->addFieldMapping("body:format")->defaultValue("filtered_html")->description(t('The filtered_html format is the only one allowed
to non-administrative users.  It attaches the CKEditor WYSIWYG editor
as part of its makeup'));

    $this->addFieldMapping('uid', 'author')->description($this->source_fields['author']);

    // we'll construct the path by hand.
    $this->addFieldMapping('pathauto')->defaultValue(0)->description(t("Incoming path values are set using the historical url
location of the original item on nnlm.gov"));

    //TODO: Figure out how to add image data, and convert that data in the imported body.
    $this->addUnmigratedSources(array('drupal_node_id', 'field_section', 'hide', 'highwater_mark', 'loc', 'migrate', 'sticky',), t('Do Not Migrate'));
    $this->addUnmigratedDestinations(array('body:summary', 'changed', 'created', 'field_migration_errors', 'field_node_scripts',

    //'field_node_scripts:format',
    //'field_node_scripts:language',
    'field_node_styles',

    //'field_node_styles:format',
    //'field_node_styles:language',
    'field_origin_url:attributes', 'field_origin_url:language', 'field_origin_url:title',

    //'field_section:source_type',
    'field_tags:source_type', 'is_new', 'log', 'path', 'revision', 'revision_uid', 'sticky', 'tnid', 'translate',), t('Do Not Migrate'));
    parent::__construct($arguments);
  }

  /**
   * Called by subclasses - modifies the particulars of a migration
   * to be appropriate to a given region.
   *
   * @param  string $section_name The region to limit to
   *
   * @return NULL
   */
  protected function setEditorialSection($section_name) {
    $this->section = taxonomy_get_term_by_name($section_name, 'sections');
    if (empty($this->section)) {
      throw new \Exception("Could not find region $section_name in sections taxonomy");
    }
    $this->section = end($this->section);
    $this->description = t("Basic page migration for the " . $this->section->name . " editorial section.");
    $this->menu_name = $this->buildMenuName();
    if (!menu_load($this->menu_name)) {

      // only regional editorial sections acutally have separate menus.
      throw new \Exception("Menu $this->menu_name does not exist");
    }
    $this->removeFieldMapping('field_section');
    $this->addFieldMapping('field_section')->description(t("Editorial section"))->defaultValue($this->section->name);
  }

  /**
   * Builds the base query that this map uses.  This was removed into a function
   * to allow subclasses to start the query build process, so particulars can be
   * added to an already formed query as the ascent to parent constructors is performed.
   * Note that this query uses migration_map.migration_map data.  This is a
   * user-curated database.table of information about nnlm.gov assets.
   */
  protected function buildBaseQuery() {
    $query = \Database::getConnection('default', 'migration_map')->select('migration_map', 'm');
    $query->fields('m', array('author', 'drupal_node_id', 'hide', 'id', 'loc', 'migrate', 'notes', 'title',));
    $query->fields('hm', array('highwater_mark',));

    //group concat free-form taxonomy tags into one field.
    $query->addExpression("GROUP_CONCAT(t.tag SEPARATOR ',')", 'field_tags');
    $query->join('highwater_mark', 'hm', 'm.id = hm.id');
    $query->leftJoin('tag_assignments', 'ta', 'm.id = ta.migration_map_id');
    $query->leftJoin('tags', 't', 't.id = ta.tags_id');

    //migrate MUST be set to Y
    $query->condition('file_md5', '"0"', '<>');
    $query->isNotNull('file_md5');
    $query->condition('migrate', 'y', '=');

    //only the BP content type for this migration
    $query->condition('loc', '%.css', 'NOT LIKE');
    $query->condition('loc', '%.scss', 'NOT LIKE');
    $query->condition('loc', '%.js', 'NOT LIKE');

    //$query->condition('notes', '%equested by%', 'LIKE');
    // testing
    //$query->condition('loc', '%bandage%', 'LIKE');
    //eliminate the home page - too complicated to do by machine.
    $query->groupBy('m.loc');
    $query->orderBy('loc');
    $this->query = & $query;
  }
  /**
   * Prints query for debugging, including arguments
   * @return [type] [description]
   */
  protected function printQuery(){

    //query debugging
    $query = (string) $this->query;
    $query = str_replace("{", '', $query);
    $query = str_replace("}", '', $query);
    foreach($this->query->arguments() as $k=>$v){
      $query = str_replace($k, "'$v'", $query);
    }
    print $query;
  }
  /**
   *
   * @return {string} The name of the menu that this migration will use
   */
  public function buildMenuName() {
    $this->menu_name = & drupal_static("Migrate/NodeMigration/buildMenuName");
    if (!$this->menu_name) {
      if (is_null($this->section)) {
        throw new \Exception("NodeMigration/buildMenuName was called before the editorial section was established");
      }

      //TODO: set this up to load from the db directly.
      $custom_menus = array('evaluation', 'gmr', 'mar', 'mcr', 'ner', 'pnr', 'psr', 'scr', 'sea');
      $this->menu_name = (in_array($this->section->name, $custom_menus)) ? 'menu-' . $this->section->name . '-main-menu' : 'menu-national-main-menu';
    }
    return $this->menu_name;
  }



  /**
   * Loads all links from whatever menu this migration is assigned
   * to use, and indexes them by path, ignoring ones that do
   * not have an actual non-machine alias, and links that do not belong to
   * Drupal
   *
   * @return array The links from the menu defined under $this->menu_name
   */
  private function getMenuLinks() {
    return menu_load_links($this->menu_name);
  }
  private function replaceWithError(&$element) {
    U::dump("Creating in-page error message for missing media");
    $problem_element = htmlspecialchars($element->ownerDocument->saveXML($element));
    $f = $element->ownerDocument->createDocumentFragment();
    $f->appendXML("<span style='color: red; font-weight: bold;'>Migration error: A scald atom equivalent for a file was not found (original element: <span style='font-family:Courier'>$problem_element</span>). Please replace this block with an appropriate media item.");
    $element->parentNode->replaceChild($f, $element);
  }

  /**
   * Replaces an internal link with a Scald equivalent.
   *
   * @param  string $element The DOMElement to be modified
   *
   * @return NULL
   * @throws Exception If any part of the rendering has an issue.
   */
  private function replace_internal_link(&$element, $caption = '', $loc = 'http://nnlm.gov/') {
    static $cache_id = 'nnlm-migration-ignored-media-items';
    static $firstrun = TRUE;
    $ignored_items = & drupal_static($cache_id);
    if ($firstrun) {
      $firstrun = FALSE;
      // if (U::confirm("Rebuild ignored items list?")) {
      //   $this->rebuild_ignored_items = TRUE;
      // }

      //U::dump($ignored_items, "Ignored items");


    }
    // if (!$ignored_items || !is_array($ignored_items)) {

    //   //U::dump("Loading ignored items from cache");
    //   $cache = cache_get($cache_id, 'cache');
    //   if (isset($cache) && !empty($cache->data) && !$rebuild) {
    //     $ignored_items = $cache->data;
    //     $this->rebuild_ignored_items = FALSE;
    //   }
    //   else {
    //     $ignored_items = array();
    //     cache_set($cache_id, $ignored_items, 'cache', 360);
    //   }
    // }

    //url is the media item url found at web location $loc
    $media_url = U::absolute_url($element->getAttribute('filekey'), $loc);
    $lower_url = strtolower($media_url);
    // if (in_array($lower_url, $ignored_items)) {
    //   U::dump("Skipping ignored url $media_url");
    //   $this->replaceWithError($element);
    //   return;
    // }
    // else {
    //   U::dump($media_url, "Replacing url");
    // }
    try {
      $map_entry = MediaMap::findMapEntry('loc', $lower_url);
      // U::dump($map_entry, "Map entry");
      // $atom = scald_atom_load($sid);
      // U::dump($atom, "Atom");
      // $replacement_html = MediaMap::render_atom($map_entry->scald_id, MediaMap::DEFAULT_ATOM_CONTEXT);
      // U::dump($replacement_html, )
      // return FALSE;
      if (!empty($map_entry)) {
        $f = $element->ownerDocument->createDocumentFragment();
        $replacement_html = MediaMap::render_atom($map_entry->scald_id);
        //U::dump(U::termcolored($element->ownerDocument->saveXML($element), 'GREEN'), "Replacing element");
        $f->appendXML($replacement_html);
        $element->parentNode->replaceChild($f, $element);
        //U::dump(U::termcolored($replacement_html, 'GREEN'), "Replaced with");
        return TRUE;
      }
      else {
        $element->parentNode->removeChild($element);
        U::dump(U::termcolored($element->ownerDocument->saveXML($element), 'RED'), "Element removed and not replaced");
        $this->media_problems []= "The following element was removed and not replaced from [$loc]($loc) (missing media): <pre>".htmlspecialchars($element->ownerDocument->saveXML($element)).'</pre>';
        return FALSE;
      }
    }
    catch(\MigrateException $e) {
      U::log($loc, $e->getMessage(), 'error');
      $this->replaceWithError($element);
      return FALSE;
    }
  }

  /**
   * executed just prior to the row being available to migrate
   *
   *  This implementation fills in certain fields at runtime based
   * on the other data assigned to a given row.  It uses the
   * editorial section mapping established in the constructor
   * to determine the closest-match editoral section for a bit
   * of content.
   * @see http://drupal.org/node/1132582
   *
   * @param $current_row {stdClass}
   *
   */
  public function prepareRow($row) {
    $row->title = U::decode_entity($row->title);
    U::dump("$row->title ($row->loc)", "Processing");

    //$row->title = utf8_decode($row->title);//($row->title, 'HTML-ENTITIES');
    // increment as we go, and assign at the end.
    $row->field_migration_errors = 0;
    if (!empty($row->drupal_node_id)) {

      //Node exists in system already.  This will be picked up later, BUT
      //we must preload the node, because migrate doesn't allow for things like
      //making a migration decision based on the published status of the
      //target node (if it already exists), and we can't stop the migration
      //of a given node once we're in the prepare method body.
      $node = node_load($row->drupal_node_id);
      if ($node) {
        $w_node = entity_metadata_wrapper('node', $node, array('bundle' => 'training_class'));
        if (intval($w_node->status->value()) === 1) {

          //node is published.  Never overwrite!
          //U::dump($w_node->label(), "Skipped node (published)");
          $this->messages[] = "Skipped node " . $w_node->label() . ": refusing to overwrite Drupal version (published)";
          return FALSE;
        }
      }
      else {

        //node id in map is erroneous.  Get rid of it.
        $row->drupal_node_id = NULL;
      }
    }

    //retrieve migration information to use to replace body content
    //U::dump($url, "Atom map");
    try {
      $content = U::fetchContent($row->loc);
    }
    catch(\Exception $e) {
      U::log($row->loc, $e->getMessage(), 'error');
      return FALSE;
    }

    //U::dump($content, "content");
    //U::dump(U::purify($content->DOM->saveHTML(), $row->loc), "Content body", 'info');
    if ($content->error === TRUE) {

      //error flagged during fetchContent method
      U::log($row->loc, "Skipped import of item " . $row->loc . ":" . $content->errmsg, 'error');
      return FALSE;
    }

    //U::dump($row->body, "Body (start)", 'info');
    if (count($content->files) === 0) {
      $row->body = U::purify($content->DOM->saveHTML(), $row->loc);

      //U::dump($row->body, "Body");
      return TRUE;
    }

    //if we're here, locally hosted files were found during fetchContent method.
    //elements in the source will have the custom attribute 'filekey' assigned
    //to them by the fetchContent method.  find all such elements and replace
    //them with scald atom equivalents.
    $xpath = new \DOMXpath($content->DOM);

    //all elements that have the filekey attribute
    $elements = $xpath->query("//*[(@filekey)]");
    if (!is_null($elements)) {
      $exceptions = array();
      for ($i = 0; $i < $elements->length; $i++) {

        //for all matched elements, replace with scald atoms
        $element = & $elements->item($i);
        try {

          //U::dump(U::termcolored($element->ownerDocument->saveXML($element), 'BLUE'), "Processing");
          // if cannot extract child text, replace with empty instead.
          $text = '';
          if ($element->hasChildNodes()) {
            $text = preg_replace('/\s+/', ' ', $element->nodeValue);

            //U::dump($text, "Nodevalue");


          }
          if ($row->loc[strlen($row->loc) - 1] === '/') {
            self::replace_internal_link($element, $text, $row->loc);
          }
          else {
            self::replace_internal_link($element, $text, str_replace(basename($row->loc), '', $row->loc));
          }
        }
        catch(\Exception $e) {
          throw $e;

          //bug out.  Create very noticeable error for content body.

          $error_message = $e->getMessage();
          $row->notes = $error_message;
          $exceptions[] = "ERROR: $error_message.";

          //U::dump($error_message, "Media error", 'error');


        }
      }
      if (!empty($exceptions)) {

        //U::dump($error, "Media error for item $row->loc: ", 'error');
        $this->messages[] = "Media error for item " . $row->loc . ":\n\t" . implode("\n\t", $exceptions);

        /*$content->original_html = '<!-- This page source is displayed because migration errors were detected.  Feel free to delete this block after the errors are resolved. -->'."\n\n".$content->original_html;
        $f->appendXML('<h2>Original page source code</h2><div style="border: 1px solid gray; margin: 2em; font-family: Courier; padding: 2em;">'.htmlspecialchars($content->original_html).'</div>');
        */
      }
    }

    //overwrite previously saved body with media-updated version
    $row->body = U::purify($content->DOM->saveHTML(), $row->loc);

    //U::dump($row->body, "Body (end)", 'info');
    return TRUE;
  }

  /**
   * Executed after the node object is constructed, but before it is
   * saved to the db.
   *
   * @param  StdClass $node The node to be saved
   * @param  A.Array $row  The row of data from the origin
   */
  public function prepare($node, $row) {
    try {

      //$w_node = entity_metadata_wrapper('node', $node, array('bundle' => 'basic_node'));
      //U::dump(array_keys($w_node->getPropertyInfo()), "Properties");
      //U::dump($node, "Node");
      //Add migration information to notes field
      $row->notes = trim($row->notes) . "\nMigrated on: " . date("r");
      //U::dump($notes_with_timestamp, "Notes with timestamp");
      // if (isset($node->field_migration_notes)) {
      //   U::dump($node->field_migration_notes, "notes");
      //   //U::dump($notes_with_timestamp, "Adding field notes");
      //   $node->field_migration_notes['und'][0]['value'] = $notes_with_timestamp;
      // }
      if (isset($node->field_migration_errors) && !empty($row->field_migration_errors)) {

        //U::dump($row->field_migration_errors, "Migration errors detected");
        $node->field_migration_errors['und'][0]['value'] = $row->field_migration_errors;
      }

      //


    }
    catch(\Exception $e) {
      $msg = "Exception for item " . $row->loc . ": " . $e->getMessage();

      //U::dump($msg, "Exception", 'error');
      $this->messsages[] = $msg;
    }

    //we'll try to reposition all the links to appropriate locations once
    //the migration ends.


  }

  /**
   * Adds the node to workbench access, and saves a new alias from the
   * old location
   *
   * @param  \stdClass $node The node object to be saved
   * @param  array $row  The row data being imported
   *
   * @return NULL
   */
  public function complete($node, $row) {
    global $base_url;

    //update node title if necessary
    //
    //U::dump("Imported node " . $node->title . ' ('.$row->loc.')');
    workbench_access_node_insert($node);
    $alias = array(
      'source' => 'node/' . $node->nid,
      'alias' => str_replace('http://nnlm.gov/', '', trim($row->loc, '/')),
      'language' => 'en'
    );

    //add the internal pathauto alias for this node
    path_save($alias);

    $menu_name = $this->buildMenuName();
    //Check for existing menu item
    $existing_menu_link = menu_link_get_preferred($alias['source'], $menu_name);
    if (!$existing_menu_link) {
      $new_menu_entry = array('link_path' => 'node/' . $node->nid, 'link_title' => U::decode_entity($node->title), 'menu_name' => $menu_name, 'router_path' => 'node/%',

      //inserts a new link
      'mlid' => NULL,);
      //TODO:try to find existing parent in the same menu, based on alias

      //U::dump($new_menu_entry['link_title'], "Creating new menu link");
      menu_link_save($new_menu_entry);
    }
    else {
      U::dump($existing_menu_link, "Updating existing menu link");
      $existing_menu_link['link_title'] = U::decode_entity($w_node->label());
      menu_link_save($existing_menu_link);
    }
    U::dump("Imported node " . $node->title . ' (' . $row->loc . ')');
    $https_url = preg_replace("/^http[^s]?:/", 'https:', $base_url);
    $this->processed_urls[$row->loc] = $https_url . '/node/' . $node->nid;

    //TODO: load all nodes, find absolute links to this content, and replace
    //with drupal internal links


  }

  /**
   * Runs after import is complete.  Add all the files referenced,
   * and update the migration map to record which entries are successfully
   * imported.
   *
   * @return NULL
   */
  public function postImport() {
    parent::postImport();
    U::dump($this->machineName, "\nStarting post-import process");

    //first connection gets us a list of all basic_node entries for this migration, and
    //their associated origin urls.
    $dm_records = db_query('
SELECT
mm.sourceid1,
mm.destid1,
mm.last_imported
FROM {' . $this->getMap()->getMapTable() . '} mm');

    //second connection is used to update the migration map with info about migrated records.
    $nids = array();
    $num_updated = 0;
    $nid_map = array();
    $connection = \Database::getConnection('default', 'migration_map');
    foreach ($dm_records as $dm_record) {

      //update the migration map (external to drupal) to
      //reflect the locations of the migrated content.
      //Note: we set "last_updated" (a timestamp field) to be the same
      //as the migrated time, because the default would be a newer timestamp
      //that would obviate the point of the highwater field.
      $num_updated+= $connection->update('migration_map')->fields(array('drupal_node_id' => $dm_record->destid1, 'last_migrated' => $dm_record->last_imported,))->condition("id", $dm_record->sourceid1, '=')->execute();
    }

    //disabling menu arrangement temporarily
    //$mt = new MenuTree($this->menu_name);
    //$mt->apply_structure();
    U::dump($this->machineName, "\n#Migration results");
    if (!empty($this->messages)) {
      $messages = '';
      foreach($this->messages as $message){
        $messages .= "\n- $message";
      }
      print "##Messages\n$messages\n";
    }
    if (!empty($this->media_problems)) {
      $media_problems = "\n";
      foreach($this->media_problems as $media_problem){
        $media_problems .= "\n- $media_problem";
      }
      print "##Media problems\n$media_problems\n";
    }
    //U::dump("\n##Processing errors");
    //U::printLogEntries();

    /*if (!empty($this->broken_files)) {
    U::dump("\ndrush mi Files --idlist=" . implode(",", $this->broken_files), "Please rerun file migration as follows");
    }*/

    $output = "\n";
    foreach ($this->processed_urls as $original => $new) {
      $output.= "- [$original (Original)]($original)\n- [$new (New)]($new)\n";
    }
    print "##Migration results\n$output\n";
  }

  /**
   * Runs before a rollback takes place.  Roll back any updates to the
   * migration map regarding locations of external content.
   *
   * @return NULL
   */
  public function preRollback() {
    parent::preRollback();
    $dm_records = db_query('SELECT sourceid1,destid1 FROM {' . $this->getMap()->getMapTable() . '} mm');
    $connection = \Database::getConnection('default', 'migration_map');
    $nids = array();
    $num_updated = 0;
    foreach ($dm_records as $dm_record) {
      $num_updated+= $connection->update('migration_map')->fields(array('drupal_node_id' => NULL, 'last_migrated' => NULL,))->condition("id", $dm_record->sourceid1, '=')->execute();
      $nids[] = $dm_record->destid1;
    }

    //remove the nodes from workbench access moderation.
    $nodes = node_load_multiple($nids);
    $current_menu_links = $this->getMenuLinks();
    foreach ($nodes as $node) {
      workbench_access_node_delete($node);
      $w_node = entity_metadata_wrapper('node', $node, array('bundle' => 'basic_node'));
      foreach ($current_menu_links as $link) {
        if ($link['link_title'] == $w_node->label()) {

          //delete any existing link of the same title.
          menu_link_delete($link['mlid']);
        }
      }
    }
  }
}

