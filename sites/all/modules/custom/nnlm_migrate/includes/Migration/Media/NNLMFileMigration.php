<?php
namespace NNLM\Migration\Migration\Media;
use NNLM\Migration\Utilities as U;

/**
 * Abstract class that handles any file migration.  Only subclasses are allowed,
 * as I actually need to create a migration for each file type so as to access
 * the custom fields each type has.
 */
class NNLMFileMigration extends \Migration {
  const FILE_NOT_FOUND = 1;
  const FILE_NOT_READABLE = 2;
  const MAP_ENTRY_NOT_FOUND = 4;
  const HAS_DUPLICATES = 8;
  const ENTRY_MODIFIED = 16;
  //stores valid editorial sections for reference during migration.
  private $valid_editorial_sections = array();
  private $file_suffixes = array();
  // stores message codes about skipped entries for later display
  private $modified_entries = array();
  private $skipped_entries = array();
  // migration messages
  private $messages = array();
  private $last_imported = NULL;
  private $migration_db = NULL;

  public function __construct($arguments) {
    global $databases;
    parent::__construct($arguments);
    if (!isset($databases['migration_map']['default'])) {
      throw new \Exception("Warning: The nnlm_migrate module is missing the definition for the drupal migration database in settings.php");
    }
    $this->valid_editorial_sections = nnlm_core_get_editorial_sections(array('tid' => TRUE));
    $this->team = array(new \MigrateTeamMember('Aron Beal', 'abeal@uw.edu', t('Implementor')));
    $this->description = t("Basic file migration.");
    //This has content types as dependencies, as rather than taking media items
    //from the migration map, this migration is going to build up a list by examining
    //media links that are part of imported content, and bringing those links in instead.
    //
    $source_fields = array(
      'author' => t('The Drupal user ID of the account that created the node. Same as the rosters PeopleID'),
      'drupal_node_id' => t('Drupal Node ID (set after import, not canonical!)'),
      'file_path' => t("The path on the filesystem to the original file (on HOTEL)."),
      'file_md5' => t("The md5 checksum of the original file.  Used to prevent duplication."),
      'hide' => t("Whether or not to hide this record from the migration process.  These records (when this field is true) are never processed, regardless of the migration setting"),
      'id' => t('ID'),
      'loc' => t('URL to original content on nnlm.gov (unique)'),
      'migrate' => t('Whether or not this content has been marked for migration'),
      'sticky' => t("Whether or not this node is sticky (stays at the top of lists)."),
    );

    /**
     * Note that the join conditions for this query exclude
     * media items that have a md5 value of "0" (file not found),
     * and a md5 value of null (not a media item, or the update script
     * has failed to provide that for us).  It does NOT group by file
     * md5 value, which means different instances of the same image
     * will be encountered.  These are to be filtered in the
     * prepareRow method.
     * @var [type]
     */
    if (!isset($this->migration_db)) {
      $this->migration_db = \Database::getConnection('default', 'migration_map');
    }
    $query = $this->migration_db->select('migration_map', 'm');
    $query->fields('m', array(
        'author',
        'drupal_node_id',
        'hide',
        'id',
        'lastmod',
        'loc',
        'migrate',
        'notes',
        'file_path',
        'file_md5',
      )
    );
    $query->addField('hm', 'highwater_mark');
    $query->addExpression("GROUP_CONCAT(t.tag SEPARATOR ',')", 'tags');
    $query->join('highwater_mark', 'hm', 'm.id = hm.id');
    $query->leftJoin('tag_assignments', 'ta', 'm.id = ta.migration_map_id');
    $query->leftJoin('tags', 't', 't.id = ta.tags_id');
    //Only files
    $query->condition('content_type', 'Media Item', '=');
    //$query->condition('loc', '%/images/10%', 'LIKE');
    //migrate MUST be set to Y
    $query->condition('migrate', 'y', '=');
    //$query->condition('m.loc', '%/images/3coversNew.jpg', 'LIKE');
    //$query->condition('loc', '%/scr/%', 'LIKE');
    //records hidden in the migration map are disallowed.
    $query->isNotNull('m.file_md5');
    // ignore items already migrated.
    //$query->isNull('m.scald_id');
    //$query->range(0, 100);
    //any special tags that modify the import process go here.
    //Note: this is required, or it'll group by tags!
    $query->groupBy('m.loc');
    $query->orderBy('m.loc');

    // TIP: By default, each time a migration is run, any previously unimported source items
    // are imported (along with any previously-imported items marked for update). If the
    // source data contains a timestamp that is set to the creation time of each new item,
    // as well as set to the update time for any existing items that are updated, then
    // you can have those updated items automatically reimported by setting the field as
    // your highwater field.
    $this->highwaterField = array(
      // Column to be used as highwater mark
      'name' => 'highwater_mark',
      // Table alias containing that column
      'alias' => 'hm',
      // [drupal database].migrate_map_[migration name] and
      // drupal_migration.highwater_mark.highwater_mark should both be unix
      // timestamp fields
    );
    $this->source = new \MigrateSourceSQL($query, $source_fields);
    $this->destination = new \MigrateDestinationFile();
    $this->map = new \MigrateSQLMap($this->machineName,
      //schema definition for the primary key of the source.
      array(
        'id' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Unique migration map id.',
          'alias' => 'm',
        ),
      ),
      //schema definition for the primary key of the destination
      \MigrateDestinationFile::getKeySchema(),
      'default',
      array('track_last_imported' => TRUE)
    );

    $this->addFieldMapping('uid', 'author')->defaultValue(1);
    $this->addFieldMapping('value', 'file_path');
    $this->addFieldMapping('timestamp', 'lastmod');
    $this->addFieldMapping('file_replace')->defaultValue(FILE_EXISTS_RENAME);

    $this->addFieldMapping('destination_file', 'file_md5');
    $this->addFieldMapping('destination_dir')->defaultValue('public://migrated');
    // rewrite at runtime in prepareRow
    // roll back files with migration
    // we'll construct the path by hand.
    $this->addUnmigratedSources(array(
        'drupal_node_id',
        'hide',
        'highwater_mark',
        'loc',
        'migrate',
        'notes',
        'sticky',
        'file_path',
        'file_md5',
      ), t('DNM'));
    $this->addUnmigratedDestinations(array(
        'path',
        'source_dir',
        'urlencode',
        'preserve_files',
      ), t('DNM'));
  }

  /**
   * Invoked before any node creation
   *
   * @param  array $row The data from a row to be made into a file entity
   *
   * @return bool FALSE if the row is to be skipped, TRUE otherwise
   */
  public function prepareRow($row) {
    U::dump($row->loc, "Processing media item", 'ok');
    parent::prepareRow($row);
    //allowed scald atom types.  If not in this array, set to "file"
    static $allowed_types = array(
      'file',
      'image',
      'audio',
      'video',
    );

    //set file path relative to november
    $row->file_path = str_replace('/nnlm.gov/', '/var/www/html/', $row->file_path);

    //ensure file exists
    if (!file_exists($row->file_path)) {
      $this->skipped_entries[] = $row->loc . ': nonexistent';
      U::dump($row->loc, "Skipped: nonexistent", 'error');
      return FALSE;
    }
    if (!is_readable($row->file_path)) {
      $this->skipped_entries[] = $row->loc . ': unreadable';
      U::dump($row->loc, "Skipped: unreadable", 'error');
      return FALSE;
    }

    $finfo = new \finfo(FILEINFO_MIME);
    $row->type = array_shift(explode('/', $finfo->file($row->file_path)));
    //U::dump($type, "Scald type");
    if (!in_array($row->type, $allowed_types)) {
      $row->type = 'file';
    }
    //U::dump($md5_sum, "MD5 value");
    //top level folders correspond to editorial sections in our drupal build.
    $row->field_section = (explode('/', str_replace('http://nnlm.gov/', '', $row->loc)));
    if (is_array($row->field_section)) {
      $row->field_section = reset($row->field_section);
    }
    //assign all_regions as default if missing or invalid
    if (!array_key_exists($row->field_section, $this->valid_editorial_sections)) {
      U::dump($row->field_section, "Section does not exist (using all_regions default)", 'warning');
      $row->field_section = 'all_regions';
    }
    $suffix = strtolower(preg_replace("/^.*\.([A-z]{3,4})$/", "$1", $row->file_path));
    //set file name to be a md5 sum of the file.
    $row->destination_file = $row->type . '/' . $row->file_md5 . '.' . $suffix;
    $file_uri = 'public://migrated/' . $row->destination_file;
    if (file_exists($file_uri)) {
      //Entry already exists.  Manually add to migration
      // Table name no longer needs {}
      //We will go through and update all file ids and scald ids in
      //post-import
      U::dump($file_uri, 'Entry ' . $row->loc . ' already exists at');
      try {
        //determine SID of prior item
        // Take a look at: file.inc::file_load_multiple
        $files = file_load_multiple(array(), array('uri' => $file_uri));
        // If empty, $file will be false, otherwise will contain the required file
        $file = reset($files);
        if (!$file) {
          //if the file can't be loaded, continue import of the new
          return TRUE;
        }
        //U::dump($file, "File", 'ok');
        $efq = new \EntityFieldQuery();
        $efq->entityCondition('entity_type', 'scald_atom');
        $efq->propertyCondition('base_id', $file->fid, '=');
        $result = $efq->execute();
        if (!isset($result['scald_atom']) || count($result['scald_atom']) === 0) {
          throw new \Exception("Couldn't find a scald atom for $file_uri");
        }
        foreach ($result['scald_atom'] as $sid => $data) {
          $this->modified_entries[$row->file_md5] = $sid;
          // should only be one, anyways.
          break;
        }
      }
      catch(\Exception$e) {
        U::dump($e->getMessage(), "Exception triggered", 'error');
        $this->skipped_entries[] = $row->loc . ': duplicate (atom not found)';
        U::dump($row->loc, "Atom not found", 'error');
        return FALSE;
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Run after new node is created, but before final save.
   *
   * @param  stdClass $file The File object created, before final save
   * @param  array $row  The row data from source
   *
   * @return NULL
   */
  public function prepare($file, $row) {
    //change the file name to be a md5 hash, for easier duplication detection.
    // $file->destination_file = $type . '/' . basename($row->loc);
    //
    U::dump(basename($row->loc) . ', section: ' . $row->field_section, "Added");
    $file->destination_file = $row->destination_file;
  }

  /**
   * Executed on single migrate row completion
   *
   * @param  stdClass $file The newly imported file
   * @param  array $row    The row data from the source
   *
   * @return NULL
   */
  public function complete($file, $row) {
    //create a new atom for the new file.
    $atom = U::create_scald_atom($file, array(
        'field_origin_url' => array('url' => $row->loc),
        'field_migration_notes' => $row->notes,
        'field_section' => $row->field_section,
      ));
    if ($atom) {
      $this->modified_entries[$row->file_md5] = $atom->sid;
    }
  }

  /**
   * add modified file names to migration map
   **/
  public function postImport() {
    //Update the migration map with the imported content
    parent::postImport();
    U::dump($this->machineName, "Starting post-import process", 'ok');
    U::memoryUsage();

    //update the migaration map with the file data.
    if (!isset($this->migration_db)) {
      $this->migration_db = \Database::getConnection('default', 'migration_map');
    }
    //$hash_map = U::create_hash_map();

    $transaction = db_transaction();
    $num_updated = 0;
    try {
      $atoms = scald_fetch_multiple(array_values($this->modified_entries));
      $expression='CASE';
      foreach($this->modified_entries as $file_md5=>$sid) {
        $expression.=" WHEN file_md5 = '$file_md5' THEN '$sid'";
      }
      $expression.=' END';
      //U::dump($expression, "Expression", 'ok');
      $mm_update_query = $this->migration_db->update('migration_map');
      $mm_update_query->condition('file_md5', array_keys($this->modified_entries), 'IN');
      $mm_update_query->expression('scald_id', $expression);
      $num_updated += $mm_update_query->execute();
      if ($num_updated > 0) {
        U::dump($num_updated, "Total migration map records updated", 'ok');
      }
    }
    catch(\Exception$e) {
      $transaction->rollback();
      U::dump($e->getMessage(), "postImport failure", 'error');
    }
    if (!empty($this->skipped_entries)) {
      //U::dump($this->skipped_entries, "The following entries were skipped during import");
    }
    U::dump($this->machineName, "Postmigration complete", 'ok');
    U::memoryUsage();
    cache_clear_all('nn-lm-migration-atom-map', 'cache');
  }

  /**
   * add modified file names to migration map
   **/
  public function preRollback() {
    parent::preRollback();
  }
  public function postRollback() {
    parent::postRollback();
    U::dump($this->machineName, "Starting post-rollback process", 'ok');
    if (!isset($this->migration_db)) {
      $this->migration_db = \Database::getConnection('default', 'migration_map');
    }
    $atoms = entity_load('scald_atom', FALSE, array());

    //remove all atoms that no longer have a file under the hood.
    $removed_files = array();
    foreach ($atoms as $sid => $atom) {
      if (!isset($atom->base_id)) {
        U::dump($atom->title, "Removing atom");
        //no underlying file record, nothing to remove but the entity
        scald_atom_delete($atom->sid);
        continue;
      }
      $file = file_load($atom->base_id);
      if (!$file) {
        U::dump($atom->title, "Removing atom");
        scald_atom_delete($atom->sid);
        continue;
      }
      //U::dump($atom, "Remaining atom");
    }
    //reload atom list.  Collect scald ids of any remaining atoms.  NULL
    //any mm entries not in this list.
    $atoms = entity_load('scald_atom', FALSE, array());
    U::dump("Cleaning migration map entries");
    if (empty($atoms)) {
      //no atoms in system - clear ALL Media Item entries
      $mm_update_query = $this->migration_db->update('migration_map');
      $mm_update_query->condition('content_type', 'Media Item', '=');
      $mm_update_query->fields(array(
          'drupal_node_id' => NULL,
          'last_migrated' => NULL,
          'scald_id' => NULL,
        ));
      $mm_update_query->execute();
      unset($mm_update_query);
    }
    else {
      $sids = array_keys($atoms);
      U::dump(count($sids), "Number of atoms remaining:");
      $mm_update_query = $this->migration_db->update('migration_map');
      $mm_update_query->condition('content_type', 'Media Item', '=');
      $mm_update_query->condition(
        db_or()->condition('scald_id', $sids, "NOT IN")
        ->isNull('scald_id')
      );
      $mm_update_query->fields(array(
          'drupal_node_id' => NULL,
          'last_migrated' => NULL,
          'scald_id' => NULL,
        ));
      $num_updated = $mm_update_query->execute();
      unset($mm_update_query);
      U::dump($num_updated, "Modified migration map entries");
      $query = $this->migration_db->select('migration_map');
      $query->condition('content_type', 'Media Item', '=');
      $query->isNotNull("drupal_node_id");
      $result = $query->countQuery()->execute()->fetchField();
      U::dump($result, "Count of currently imported entries recorded in the migration map");
    }
    //close migration db connection last.
    unset($this->migration_db);
  }
}

