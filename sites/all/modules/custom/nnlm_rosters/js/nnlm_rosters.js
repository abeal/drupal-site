/**
 * From https://www.drupal.org/node/365241
 * Modifies the
 * @param  {[type]} $ [description]
 * @return {[type]}   [description]
 */
(function($) {
        Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
        Drupal.behaviors.NNLM.rosters = {
            attach: function(context) {
                console.log("nnlm_rosters.js boostrapped");
                    $("input#edit-field-course-developer-und-0-nnlm-rosters-fullname", context).bind('autocompleteSelect', function(event, node) {
	                    console.log('autocompleteSelect');
	                    var key = $(node).data('autocompleteValue');
	                    if(key === ''){
	                    	//TODO: deal with errors
	                    	return;
	                    }
	                    var keys = key.split('|');
	                    var peopleId = keys[0];
	                    var name = keys[1];
	                    //console.log("Hidden form", $(".staff_member_peopleid", context));
	                    $(".staff_member_peopleid", context).val(peopleId);
	                    $(this).val(name);
                    });
                }
            };
        })(jQuery);