<?php
namespace Drupal\nnlm_rosters\Exception;

/** @class Drupal\nnlm_rosters\Exception\System
 * Generic exception for errors that arise when there is a problem
 * with the rosters code that is unrelated to user action.  An example
 * might be a misconfigured drupal instance that does not contain
 * required libraries.
 */
class System extends Exception {

}

