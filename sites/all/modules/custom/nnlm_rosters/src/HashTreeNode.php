<?php
namespace Drupal\nnlm_rosters;
/**
 * A simple hash tree.  Subdivides words by letter, and constructs
 * descending hash lookups to speed search
 */
class HashTreeNode {
	private $children = array();
  private $values = array();
  private $parent = NULL;
  public function __construct(&$parent=NULL){
    if(!is_null($parent)){
      $this->parent = $parent;
    }
  }
  public function getCount(){
    $result = 0;
    foreach($this->children as $k=>&$c){
      $result += $c->getCount();
    }
    return count($values) + $result;
  }
  private function getSubtreeValues($include=-1){
    $result = array();
    $result = array_merge($result, array_values($this->values));
    foreach($this->children as &$c){
      $result = array_merge($result, $c->getSubtreeValues(TRUE));
    }
    return $result;
  }
  public function find(/*string*/ $key, /* string */ $original_key=''){
    if(empty($key)){
      return array();
    }
    if($original_key === ''){
      $original_key = $key;
    }
    dfb($key, "Finding");
    $results = array();
    $k = strtolower($key[0]);
    if(strlen($key) === 1){
      if(isset($this->values[$k])){
        $results []= $this->values[$k];
      }
      if(isset($this->children[$k])){
        $results = array_merge($results, $this->children[$k]->getSubtreeValues());
      }
      return $results;
    }
    else if(isset($this->children[$k])){
      //other entries with this substring.
      $child_results = $this->children[$k]->find(substr($key, 1), $original_key);
      //dfb($child_results, "Child results");
      $results = array_merge($results, $child_results);
      if(!empty($child_results)){
        dfb($child_results, "Found");
      }
    }
    return $results;
  }
  public function add(/* string */ $key, /*string*/ $value='', /* string */ $original_key=''){
    //dpm($key, "Adding key");
    if(empty($key)){
      //dpm($key, "Key is empty", "error");
      return FALSE;
    }
    if(empty($value)){
      $value = $key;
    }
    if($original_key === ''){
      $original_key = $key;
    }
    $k = strtolower($key[0]);
    $key = substr($key, 1); //chop a letter off the front
    if(empty($key)){
      //terminal letter, store original and return
      //dpm("Assigning value $value");
      $this->values[$k] = $value;
      return TRUE;
    }
    if(!isset($this->children[$k])){
      $this->children[$k] = new HashTreeNode($this);
    }
    $this->children[$k]->add($key, $value, $original_key);
  }
  public function __toString(){
    $result = '';
    foreach($this->values as $k=>$v){
      $result .= "$k: $v\n";
    }
    foreach($this->children as $k=>&$c){
      $result .= "$k";
      $result .= '->' . (string) $c;
    }
    return $result;
  }
}
