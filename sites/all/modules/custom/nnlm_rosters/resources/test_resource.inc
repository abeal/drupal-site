<?php
/**
 * Callback to test services REST implementation
 */
function nnlm_rosters_services_test() {
  watchdog('nnlm_rosters', __FUNCTION__, array(), WATCHDOG_NOTICE, 'link');
  $result = array('foo' => 'bar');
  return $result;
}
/**
 * Callback to test services REST implementation for ids
 */
function nnlm_rosters_services_test_id($id) {
  watchdog('nnlm_rosters', __FUNCTION__, array(), WATCHDOG_NOTICE, 'link');
  watchdog('nnlm_rosters', "id: $id", array(), WATCHDOG_NOTICE, 'link');
  if(is_integer($id)){
    return array('id' => $id);
  }
  return services_error("Bad Request: invalid argument passed", 400);
}
/**
 * Access callback for test services
 * @param  string $op   The operation being performed: creat
 * @param  [type] $args [description]
 * @return [type]       [description]
 */
function nnlm_rosters_services_test_access($op, $args) {
  watchdog('nnlm_rosters', __FUNCTION__, array(), WATCHDOG_NOTICE, 'link');
  return TRUE;
}
