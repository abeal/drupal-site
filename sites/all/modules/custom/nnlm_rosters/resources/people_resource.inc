<?php
use Drupal\nnlm_rosters\Utilities as U;
/**
 * @file
 */
function nnlm_rosters_services_retrive_people_access(){
  return user_access('read services data');
}
/**
 * Services callback for 'index' query.
 *
 * @return NULL
 */
function nnlm_rosters_services_retrive_people_index() {
  watchdog('nnlm_rosters', __FUNCTION__, array(), WATCHDOG_NOTICE, 'link');
  $result = array();
  U::json_output('person', $result);//TEMP, until paging and security is set up
  return;
  $connection = \Database::getConnection('default', 'rosters');
  $query_result = $connection->select('currentStaff', 'c')
      ->fields('c')
      ->orderBy('LastName')
      ->orderBy('FirstName')
      ->range(0,10)
      ->execute()->fetchAll();
  U::json_output('person', $query_result);
}
/**
 * Services callback for individual people records.
 *
 * @param int $id
 *   The rosters PeopleID value to retrieve
 *
 * @return NULL
 */
function nnlm_rosters_services_retrive_people_single($id) {
  watchdog('nnlm_rosters', __FUNCTION__, array(), WATCHDOG_NOTICE, 'link');
  $result = array();
  if (!is_numeric($id)) {
    $result['error'] = 'Invalid input';
    drupal_json_output($result);
    return;
  }
  $connection = \Database::getConnection('default', 'rosters');
  $query_result = $connection->select('currentStaff', 'c')
      ->fields('c')
      ->condition('c.PeopleID', $id)
      ->orderBy('LastName')
      ->orderBy('FirstName')
      ->execute()->fetchAll();
  U::json_output('person', $query_result);
}
