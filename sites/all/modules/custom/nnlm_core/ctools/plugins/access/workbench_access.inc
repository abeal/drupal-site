<?php
/**
 * @file
 * workbench_access.inc
 * Plugin to provide access control based upon specific terms.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("User: workbench access section"),
  'description' => t('Control access by a specific workbench access section.'),
  'callback' => 'nnlm_core_workbench_access_ctools_access_check',
  'default' => array('tids' => array()),
  'settings form' => 'nnlm_core_workbench_access_ctools_access_settings',
  'settings form submit' => 'nnlm_core_workbench_access_ctools_access_settings_submit',
  'summary'=> 'nnlm_core_workbench_access_ctools_access_summary',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Settings form for the 'by term' access plugin
 */
function nnlm_core_workbench_access_ctools_access_settings($form, &$form_state, $conf) {
  // If no configuration was saved before, set some defaults.

  $sections = taxonomy_vocabulary_machine_name_load('sections');
  $sections = taxonomy_get_tree($sections->vid);
  $options = array();
  $options[$sections->vid] = $sections->name;
  $form['settings']['tids'] = array(
    '#title' => t('Editorial Sections'),
    '#type' => 'select',
    '#description' => t('Select a section or sections.'),
    '#default_value' => $conf['tids'],
    '#options' => nnlm_core_get_editorial_sections(array('assoc'=>TRUE)),
    '#multiple' => TRUE,
  );
  return $form;
}

function nnlm_core_workbench_access_ctools_access_settings_submit($form, &$form_state) {
  //dpm(__FUNCTION__);
}
/**
 * Check for access.
 */
function nnlm_core_workbench_access_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  return nnlm_core_check_workbench_access($conf['tids']);
}


/**
 * Provide a summary description based upon the checked roles.
 */
function nnlm_core_workbench_access_ctools_access_summary($conf, $context) {
  if (!isset($conf['tids'])) {
    $conf['tids'] = array();
  }
  $sections = nnlm_core_get_editorial_sections(array('assoc'=>TRUE));

  $names = array();
  foreach (array_filter($conf['tids']) as $tid) {
    $names[] = check_plain($sections[$tid]);
  }

  if (empty($names)) {
    return t('@identifier can have any editorial section', array('@identifier' => $context->identifier));
  }

  return format_plural(count($names), '@identifier has role "@roles"', '@identifier has one of "@sections"', array('@sections' => implode(', ', $names), '@identifier' => $context->identifier));
}


