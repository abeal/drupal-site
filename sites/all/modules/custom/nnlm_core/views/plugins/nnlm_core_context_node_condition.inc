<?php
//TODO: determine status of this file.
/**
 * Expose node views/node forms of specific node types as a context condition.
 */
class nnlm_core_context_node_condition extends context_condition {
  function condition_values() {
    $values = array();

    // Get the allowed options from our field, and return these to context
    // as the values for our condition.
    /*$field = field_info_field('field_sidebars');
    $field_values = list_allowed_values($field);

    foreach($field_values as $field_key => $field_value) {
      $values[$field_key] = check_plain($field_value);
    }*/
    return $values;
  }

  function options_form($context) {
    return array('theme' => array('#type' => 'value', '#value' => TRUE));
  }
/**
 * invokes the contextual test when presented with a node
 * @param  StdClass $node The node to be displayed
 * @param  string $op   ?
 * @return void
 */
  function execute($node, $op) {
    // Grab the value this node has stored for our field.
    if ($items = field_get_items('node', $node, 'field_sidebars', $node->language)) {

      // See if any of the field's values satisfy the condition.
      foreach ($items as $item) {
        foreach ($this->get_contexts($item['value']) as $context) {
          $this->condition_met($context, $item['value']);
        }
      }
    }
  }
}
