<?php

/**
 * @file
 * Definition of views_plugin_access_perm.
 */

/**
 * Access plugin that provides workbench-access-based access control.
 */
class nnlm_core_workbench_access_perm extends views_plugin_access {
  function access($account) {
    return nnlm_core_check_workbench_access($account);
  }

  function get_access_callback() {
    return array('nnlm_core_check_workbench_access', array($this->options['nnlm_workbench_access']));
  }
  private static function get_editorial_sections(){
    return nnlm_core_get_editorial_sections(array('assoc'=>TRUE));
  }
  /**
   * Defines the text displayed for the permission options.  Once NN/LM
   * Workbench Access is the chosen permissions scheme for a particular
   * view, the configurable options become visible after the title string
   * in the Views UI.  This provides the text for the link that allows
   * customization of those options.
   * @return string The text displayed after the pipe in the Views UI
   */
  function summary_title() {
    if(empty($this->options['nnlm_workbench_access'])){
      return t('Allowed sections');
    }
    return t('Allowed sections ('.implode(', ', $this->options['nnlm_workbench_access']).')');
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['nnlm_workbench_access'] = self::get_editorial_sections();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['nnlm_workbench_access'] = array(
      '#type' => 'select',
      '#options' => self::get_editorial_sections(),
      '#default_value' => $this->options['nnlm_workbench_access'],
      '#multiple' => TRUE,
      '#title' => t('Allowed editorial sections'),
      '#description' => t('Provided by the nnlm_core_workbench_access_perm
        plugin defined in the NN/LM Core module.  Only users assigned one of
        the selected workbench access section(s) will be able to access this
        display. Note that users with "access all views" can see any view,
        regardless of other permissions.'),
    );
  }
}
