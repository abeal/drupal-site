<?php
/**
 * @file
 * Nnlm_core_dump("nnlm_core.views.inc");.
 */
use Drupal\nnlm_core\Utilities as U;
/**
 * Implements hook_views_plugins
 * Custom plugins to do the following
 * 1. Allow workbench access views restriction.
 *
 * @return array @see https://api.drupal.org/api/views/views.api.php/group/views_plugins/7
 */
function nnlm_core_views_plugins() {
  $plugins = array(
    'access' => array(
      'nnlm_workbench_access' => array(
        'title' => t('NN/LM Workbench Access'),
        'help' => t('this is a custom access plugin'),
        'handler' => 'nnlm_core_workbench_access_perm',
        'uses options' => TRUE,
        'path' => drupal_get_path('module', 'nnlm_core') . '/views/plugins',
      ),
    ),
  );
  return $plugins;
}

/**
 * Implements hook_views_query_alter
 * Modifies the scald atom display to connect to the dnd tables to
 * determine usage count for atoms.
 *
 * @param [type] $view
 *   [description]
 * @param [type] $query
 *   [description]
 *
 * @return [type]        [description]
 */
function nnlm_core_views_query_alter(&$view, &$query) {
  // nnlm_core_dump($view->name, __FUNCTION__);
  switch ($view->name) {
    case 'scald_atoms':
      if (!module_exists('mee')) {
        break;
      }
      /*nnlm_core_dump($query, "Query");
      $mee_join = new views_join();
      $mee_join->table = 'mee_resource';
      $mee_join->field = 'atom_sid';
      $mee_join->left_table = 'scald_atoms';
      $mee_join->left_field = 'sid';
      $mee_join->type = 'left';
      $query->add_relationship('mee_resource', $mee_join, 'scald_atoms');
      $query->add_field('mee_resource', 'entity_type');
      $query->add_field('mee_resource', 'entity_id');
      nnlm_core_dump($view, "Modifying view");
      break;*/
  }
}


/**
 * Implements hook_views_query_alter().
 * Modifies the admin users view to include access to editorial section data.
 */
/*function nnlm_core_views_query_alter(&$view, &$query) {
nnlm_core_dump(__FUNCTION__);
if ($view->name == 'admin_views_user') {
// Join to workbench access table and editorial section taxonomy
$wba_join = new views_join();
$wba_join->table = 'workbench_access_user';
$wba_join->field = 'uid';
$wba_join->left_table = 'users';
$wba_join->left_field = 'uid';
$wba_join->type = 'left';
$ttd_join = new views_join();
$ttd_join->table = 'taxonomy_term_data';
$ttd_join->field = 'tid';
$ttd_join->left_table = 'workbench_access_user';
$ttd_join->left_field = 'access_id';
$ttd_join->type = 'left';
$query->add_relationship('workbench_access_user', $wba_join, 'users');
$query->add_relationship('taxonomy_term_data', $ttd_join, 'workbench_access_user');
$query->add_table('taxonomy_term_data');
nnlm_core_dump($view, "Modifying view");
}
}*/




/**
 * Implements hook_views_data_alter
 * TODO: finish flushing out adding relationship to users to allow viewing of
 * editorial sections.
 *
 * @param [type] $data
 *   [description]
 *
 * @return [type]       [description]
 */
function nnlm_core_views_data_alter(&$data) {

  $data['workbench_access']['table']['join']['workbench_access_user'] = array(
    'left_field' => 'access_id',
    'field' => 'access_id',
  );
  $data['workbench_access_user']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
  );
  // This example adds a relationship to table {foo}, so that 'foo' views can
  // add this table using a relationship. Because we don't want to write over
  // the primary key field definition for the {foo}.fid field, we use a dummy
  // field name as the key.
  $data['user']['workbench_access'] = array(
    'title' => t('Example relationship'),
    'help' => t('Example help'),
    'relationship' => array(
      'base' => 'workbench_access',
  // Table we're joining to.
      'base field' => 'access_',
  // Field on the joined table.
      'field' => 'fid',
  // Real field name on the 'foo' table.
      'handler' => 'views_handler_relationship',
      'label' => t('Default label for relationship'),
      'title' => t('Title seen when adding relationship'),
      'help' => t('More information about relationship.'),
    ),
  );
  if (module_exists('mee')) {
    //U::dump("MEE Data", $data);
    $data['mee_resource'] = array(
      'table' => array(
        'title' => t("Scald Atom Usage"),
        'group' => t('NNLM'),
        'help' => t("Allows display of Scald atom usage in a view.  Requires mee module"),
        'join' => array(
          'scald_atoms' => array(
            'left_table' => 'scald_atoms',
            'left_field' => 'sid',
            'field' => 'atom_sid',
            'type' => 'LEFT',
          ),
        ),
      ),
    /*'relationship' => array(
     'handler' => 'views_handler_relationship',
     'base' => 'scald_atoms',
     'base field' => 'sid',
     'label' => t('Foo'),
    ),*/
    // fields being added.
      'entity_type' => array(
        'title' => t("Owning entity type"),
        'help' => t("The entity type of the entity that is displaying the atom"),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
      'entity_id' => array(
        'title' => t("Owning entity id"),
        'help' => t("The entity id of the entity that is displaying the atom"),
        'filter' => array(
          'handler' => 'views_handler_filter_many_to_one',
        ),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
      ),
    );
  }
}




// function nnlm_core_views_data_alter(&$views_data){
/*$views_data['workbench_access']['table']['join']['users'] = array(
'left_table' => 'workbench_access_user',
'left_field' => 'access_id',
'field' => 'access_id',
);
$views_data['workbench_access_user']['table']['join']['users'] = array(
'left_field' => 'uid',
'field' => 'uid',
);*/









/**
 * Nnlm_core_dump($views_data, __FUNCTION__);
 * }.
 */

function nnlm_core_views_pre_render(&$view) {
  /*nnlm_core_dump(__FUNCTION__);
  if ($view->name == 'admin_views_user') {
  nnlm_core_dump($view, "Prerender");
  }*/
}
