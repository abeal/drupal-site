<?php

/**
 * Implements hook_field_group_info().
 */
function nnlm_core_field_group_info() {
  //nnlm_core_dump(__FUNCTION__);
  $export = array();
  $groups = array(
    'group_title_and_body' => array(
      'title' => t('Content'),
      'description' => t('Contains the title and body field'),
      'children' => array('title', 'body'),
    ),
    'group_location' => array(
      'title' => t('Location'),
      'description' => t('Contains fields that dictate how the content is found and accessed.'),
      'children' => array('field_tags', 'path', 'menu'),
    ),
    'group_customization' => array(
      'title' => t('Customization'),
      'description' => t('Contains fields that allow for customization of content display'),
      'children' => array('field_node_scripts', 'field_node_styles'),
    ),
    'group_administrative' => array(
      'title' => t('Administrative'),
      'description' => t('Contains administrative details about the content'),
      'children' => array('field_section', 'field_origin_url', 'field_migration_notes', 'field_migration_errors'),
    ),
  );
  $bundles = array('basic_node', 'webform', 'nodeblock');
  $weight = 1;
  //node modifications only
  foreach ($groups as $group_name => $title) {
    foreach ($bundles as $bundle) {
      $field_group = new stdClass();
      // Edit this to true to make a default field_group disabled initially
      $field_group->disabled = FALSE;
      $field_group->api_version = 1;
      $field_group->identifier = $group_name . '|node|' . $bundle . '|form';
      $field_group->group_name = $group_name;
      $field_group->entity_type = 'node';
      $field_group->bundle = $bundle;
      $field_group->mode = 'form';
      $field_group->parent_name = '';
      $field_group->data = array(
        'label' => $groups[$group_name]['title'],
        'weight' => $weight,
        'children' => $groups[$group_name]['children'],
        'format_type' => 'tab',
        'format_settings' => array(
          'formatter' => 'closed',
          'instance_settings' => array(
            'description' => $groups[$group_name]['description'],
            'classes' => 'field-group-tab',
            'required_fields' => 1,
          ),
        ),
      );
      $export[$field_group->identifier] = $field_group;
      $weight++;
    }
  }
  //nnlm_core_dump($export, "Export");
  return $export;
}
/**
 * Implements hook_field_group_pre_render().
 *
 * Function that fungates as last resort to alter the pre_render build.
 */
function nnlm_core_field_group_pre_render(& $element, $group, & $form) {
  nnlm_core_dump(__FUNCTION__);
}

/**
 * Implements hook_field_group_build_pre_render_alter().
 *
 * Function that fungates as last resort where you can alter things. It is
 * expected that when you need this function, you have most likely a very custom
 * case or it is a fix that can be put in field_group core.
 *
 * @param Array $elements by address.
 */
function nnlm_core_field_group_pre_render_alter(& $element){
  nnlm_core_dump(__FUNCTION__);
}
/**
 * Implements hook_field_group_update_field_group
 * This hook is invoked by ctools export API. Note that this is used by ctools
 * and the group could occasional be the group ID.
 * @param  string $group The group to update
 * @return ?
 */
function nnlm_core_field_group_update_field_group($group) {
  nnlm_core_dump(__FUNCTION__);
  // Delete extra data depending on the group.
}
