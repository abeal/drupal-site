<?php
namespace Drupal\nnlm_core;
/**
 * @file
 * Tests for the nnlm_core module.
 */
class NNLMWebTestCase extends \DrupalWebTestCase
{
    protected $permissions = array();
    protected $users = array();

    public function drupalCreateUser($type, $extra_permissions){
        if(isset($this->permissions[$type])){
            debug('Adding permissions for $type role to created user.');
            return parent::drupalCreateUser(array_merge($this->permissions[$type], $extra_permissions));
        }
        return parent::drupalCreateUser($extra_permissions);
    }
    /**
     * Perform any setup tasks for our test case.  Needs to be overridden by subclass.
     */
    public function setUp(array $modules, array $extra_permissions=array()) {
        //perform user setup prior to calling parent class (which would in turn perform the action of switching to a test drupal instance.)  Roles (and usernames) as of this writing are:
        //anonymous user
        //authenticated user
        //content creator
        //content moderator
        //developer
        //drupal administrator
        $roles = user_roles(FALSE);
        $permissions = user_role_permissions($roles);
        //pass an array of modules we want enabled.
        parent::setUp($modules);
        //create the roles in the newly established test instance.

        foreach($roles as $rid=>$role){
            $this->permissions[$role] = $permissions[$rid];
        }
    }

    /**
     * Get the number of watchdog entries for a given severity or worse
     *
     * See http://dcycleproject.org/blog/96/catching-watchdog-errors-your-simpletests
     *
     * @param $severity = WATCHDOG_ERROR
     *   Severity codes are listed at https://api.drupal.org/api/drupal/includes%21bootstrap.inc/group/logging_severity_levels/7
     *   Lower numbers are worse severity messages, for example an emergency is 0, and an
     *   error is 3.
     *   Specify a threshold here, for example for the default WATCHDOG_ERROR, this function
     *   will return the number of watchdog entries which are 0, 1, 2, or 3.
     *
     * @return
     *   The number of watchdog errors logged during this test.
     */
    protected static function getNumWatchdogEntries($severity = WATCHDOG_ERROR) {
        return count(self::getWatchdogEntries($severity));
    }
    protected static function clearWatchdogEntries(){
        db_delete('watchdog')->execute();
    }
    /**
     * Get the number of watchdog entries for a given severity or worse
     *
     * See http://dcycleproject.org/blog/96/catching-watchdog-errors-your-simpletests
     *
     * @param $severity = WATCHDOG_ERROR
     *   Severity codes are listed at https://api.drupal.org/api/drupal/includes%21bootstrap.inc/group/logging_severity_levels/7
     *   Lower numbers are worse severity messages, for example an emergency is 0, and an
     *   error is 3.
     *   Specify a threshold here, for example for the default WATCHDOG_ERROR, this function
     *   will return the number of watchdog entries which are 0, 1, 2, or 3.
     *
     * @return
     *   The number of watchdog errors logged during this test.
     */
    protected static function getWatchdogEntries($severity = WATCHDOG_ERROR, $clear_on_retrieve=FALSE) {
        $severity_levels = watchdog_severity_levels();
        $watchdog_messages = db_select('watchdog', 'w')->fields('w')->condition('severity', $severity, '>=')->execute()->fetchAll();
        $results = array();
        array_map(function($o) use (&$results, $severity_levels){
            $variables = unserialize($o->variables);
            $new_string = $severity_levels[$o->severity].": ".$o->message;
            foreach($variables as $k=>$v){
                $new_string = str_replace($k, $v, $new_string);
            }
            $results []= $new_string;
        }, $watchdog_messages);
        if($clear_on_retrieve){
            self::clearWatchdogEntries();
        }
        return $results;
    }
    public function tearDown() {
        // See http://dcycleproject.org/blog/96/catching-watchdog-errors-your-simpletests
        $num_errors = self::getNumWatchdogEntries();
        $expected_errors = isset($this->expected_errors) ? $this->expected_errors : 0;
        $this->assertTrue($num_errors == $expected_errors, 'Expected ' . $expected_errors . ' watchdog errors and got ' . $num_errors . '.');
        //print_r(self::getWatchdogEntries());
        parent::tearDown();
    }
}
