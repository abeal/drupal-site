/**
 * Core NN/LM codespace
 *
 * For more on working with js in D7, @see https://www.drupal.org/node/304258.
 * @type {Object}
 */
(function($) {
  Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
  Drupal.behaviors.NNLM.core = {
    /**
     * Invoked on page load
     */
    attach: function(context, settings) {
      this.dump("* NN/LM core javascript initialized, version "+Drupal.behaviors.NNLM.core.version_number);
      this.update_origin_url(context);
      this.update_scald_links(context);
      $.each(Drupal.behaviors.NNLM.core.callbacks, function(fn) {
        fn();
      });
    },
    bind_code_mirror_editor: function(context) {
      if (window.CodeMirror === undefined) {
        //either there is an error, or we're not on an admin page.  Assume the latter for now.
        return;
      }
      if (this.style_editor !== undefined || this.script_editor !== undefined) {
        return;
      }
      var codemirror_defaults = {
        dragDrop: false,
        // Set this to the theme you wish to use (codemirror themes)
        theme: 'midnight',

        // Whether or not you want to show line numbers
        lineNumbers: true,

        // Whether or not you want to use line wrapping
        lineWrapping: true,

        // Whether or not you want to highlight matching braces
        matchBrackets: true,

        // Whether or not you want tags to automatically close themselves
        autoCloseTags: true,

        // Whether or not you want Brackets to automatically close themselves
        autoCloseBrackets: true,

        // Whether or not to enable search tools, CTRL+F (Find), CTRL+SHIFT+F (Replace), CTRL+SHIFT+R (Replace All), CTRL+G (Find Next), CTRL+SHIFT+G (Find Previous)
        enableSearchTools: true,

        // Whether or not you wish to enable code folding (requires 'lineNumbers' to be set to 'true')
        enableCodeFolding: true,

        // Whether or not to enable code formatting
        enableCodeFormatting: true,

        // Whether or not to automatically format code should be done when the editor is loaded
        autoFormatOnStart: true,

        // Whether or not to automatically format code should be done every time the source view is opened
        autoFormatOnModeChange: true,

        // Whether or not to automatically format code which has just been uncommented
        autoFormatOnUncomment: true,

        // Whether or not to highlight the currently active line
        highlightActiveLine: true,

        // Define the language specific mode 'htmlmixed' for html including (css, xml, javascript), 'application/x-httpd-php' for php mode including html, or 'text/javascript' for using java script only
        mode: 'css',

        // Whether or not to show the search Code button on the toolbar
        showSearchButton: true,

        // Whether or not to show Trailing Spaces
        showTrailingSpace: true,

        // Whether or not to highlight all matches of current word/selection
        highlightMatches: true,

        // Whether or not to show the format button on the toolbar
        showFormatButton: true,

        // Whether or not to show the comment button on the toolbar
        showCommentButton: true,

        // Whether or not to show the uncomment button on the toolbar
        showUncommentButton: true,

        // Whether or not to show the showAutoCompleteButton button on the toolbar
        showAutoCompleteButton: true

      };

      var stylesheet_field = jQuery("textarea[id^=edit-field-node-styles]").get(0);
      var script_field = jQuery("textarea[id^=edit-field-node-scripts]").get(0);
      if (stylesheet_field) {
        //this.dump("Styles field found - binding codemirror editor");
        var codemirror_styles_config = $.extend({}, codemirror_defaults);
        codemirror_styles_config.mode = 'css';
        this.style_editor = CodeMirror.fromTextArea(stylesheet_field, codemirror_styles_config);
      }
      if (script_field) {
        var codemirror_scripts_config = $.extend({}, codemirror_defaults);
        codemirror_scripts_config.mode = 'javascript';
        this.script_editor = CodeMirror.fromTextArea(script_field, codemirror_scripts_config);
      }
    },
    callbacks: {},
    dump: function(msg) {
      if (window.console !== undefined) {
        console.log(msg);
      }
    },
    update_origin_url: function(context) {
      var legacy_port_number = 7456;
      var origin_url = jQuery('.field-name-field-origin-url').find('a');
      if (!origin_url || !origin_url.attr('href')) {
        return;
      }
      origin_url.attr('href', origin_url.attr('href').replace('nnlm.gov', 'nnlm.gov:' + legacy_port_number));
    },
    /**
     * updates scald media links in a page to display the contents of the
     * atom caption for link text, if both exist.  Works in 'full' context
     * only.
     * @param  {object} context ?
     * @return {NULL}
     */
    update_scald_links: function(context) {
      console.log("Updating scald media links to use caption contents");
      var scald_links = jQuery(".dnd-atom-wrapper");
      if (scald_links.length === 0) {
        return;
      }
      scald_links.each(function(param, item) {
        var legend = jQuery(item).find('.dnd-legend-wrapper').text();
        if (!legend.trim()) {
          return;
        }
        jQuery(item).find('.dnd-legend-wrapper').text('');
        var scald_id = jQuery(item).find('.dnd-drop-wrapper').find('a:not(:has(img))').text(legend);
      });
    }
  };
  Drupal.behaviors.NNLM.core.version_number = '1.1.1';
  Drupal.behaviors.NNLM.attach = function(context, settings) {
    $.each(Drupal.behaviors.NNLM, function(codebase) {
      if(Drupal.behaviors.NNLM[codebase].attach !== undefined &&
        !Drupal.behaviors.NNLM[codebase].attached){
        Drupal.behaviors.NNLM[codebase].attach(context, settings);
        Drupal.behaviors.NNLM[codebase].attached = true;
      }
    });
  };
})(jQuery);