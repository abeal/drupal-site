<?php
/**
 * @file
 * Drupal Inspect module administration layer
 */

/**
 * Implements drupal_get_form().
 *
 * Defines configuration form fields.
 *
 * @param array $form
 * @param array &$form_state
 * @return array
 *   - the return value of system_settings_form()
 */
function inspect_admin_form($form, &$form_state) {
  drupal_add_css(
      drupal_get_path('module', 'inspect') . '/inspect.admin.css',
      array('type' => 'file', 'group' => CSS_DEFAULT, 'preprocess' => FALSE, 'every_page' => FALSE)
  );
  $form['help_php'] = array(
      '#type' => 'fieldset',
      '#title' => t('Help - PHP'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'content' => array(
        '#type' => 'markup',
        '#markup' => _inspect_help_php(),
      ),
  );
  $form['help_js'] = array(
      '#type' => 'fieldset',
      '#title' => t('Help - Javascript'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'content' => array(
        '#type' => 'markup',
        '#markup' => _inspect_help_js(),
      ),
  );
  //  Establish max/default values for outmax lengths.
  $outputMaxes = InspectBs::outputMax();
  $form['output_max'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fuses - graceful degradation'),
      '#description' => t(
          'Maximum output lengths of dumps/traces/profiles secure that Inspect degrades gracefully when analyzing an overly long data set.'
              . '!breakInspector/tracer will make a second attempt when reaching maximum output, using less depth and higher truncation.'
              . '!breakVariable and trace inspections will also abort (cleanly) if more than a certain percent of PHP\'s max execution time (measured here to @max_execution_time seconds) has passed.'
          , array('!break' => '<br/>', '@max_execution_time' => $max_execution_time = ini_get('max_execution_time'))
      ),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      'inspect_outputmax_log' => array(
        '#type' => 'textfield',
        '#title' => t('Maximum output for logging to database'),
        '#description' => t(
            'Absolute max is !max. Equvalent of MySql max_allowed_packet (minus some margin): !default bytes ~ !mb Mb (1 byte ~ 1 ASCII character).',
            array('!max' => $outputMaxes['log'], '!default' => $outputMaxes['logDefault'], '!mb' => round($outputMaxes['logDefault'] / 1048576, 1))
        ),
        '#default_value' => variable_get('inspect_outputmax_log', $outputMaxes['log']),
        '#size' => 8,
        '#required' => TRUE,
      ),
      'inspect_outputmax_get' => array(
        '#type' => 'textfield',
        '#title' => t('Maximum output for getting'),
        '#description' => t(
            'Absolute max is !max ~ !mb Mb (1 byte ~ 1 ASCII character).',
            array('!max' => $outputMaxes['get'], '!mb' => round($outputMaxes['get'] / 1048576, 1))
        ),
        '#default_value' => variable_get('inspect_outputmax_get', $outputMaxes['get']),
        '#size' => 8,
        '#required' => TRUE,
      ),
      'inspect_exectime_percent' => array(
        '#type' => 'textfield',
        '#title' => t(
            'Abort variable/trace inspection if called later than this percent of max execution time (@max_execution_time seconds)',
            array('@max_execution_time' => $max_execution_time)
        ),
        '#description' => t(
            'If PHP max_execution_time is being changed during run-time - set_time_limit() - this \'fuse\' will not work fully predictably.'
                . '!breakBecause max_execution_time is only being checked at first inspection/trace in a request,'
                . '!breakand any later changes will not be reflected by Inspect\'s time limit \'fuse\' in that request.',
            array('!break' => '<br/>')
        ),
        '#default_value' => variable_get('inspect_exectime_percent', 70),
        '#size' => 2,
        '#required' => TRUE,
      ),
  );
  $form['inspect_fileline_omit'] = array(
    '#type' => 'checkbox',
    '#title' => t('PHP inspections shan\'t default to display file and line of inspection function/method call'),
    '#default_value' => variable_get('inspect_fileline_omit', FALSE),
    //'#access' => FALSE,
    '#required' => FALSE,
  );
  $form['inspect_output_nofolding'] = array(
    '#type' => 'checkbox',
    '#title' => t('Turn off variable dump folding (expandable/collapsible outputs)'),
    '#description' => t(
        'May also be turned off intermittently by calling <strong>inspect.toggleFolding()</strong> in the browser console.'
          . '!breakAnd per inspection, by using the no_folding option (the $options argument).'
        , array('!break' => '<br/>')
     ),
    '#default_value' => variable_get('inspect_output_nofolding', FALSE),
    //'#access' => FALSE,
    '#required' => FALSE,
  );
  $form['inspect_disallow_get_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prohibit the primary admin user from getting inspections of variables and traces'),
    '#description' => t(
        'Overrides the &#39;Get outputs of variable/trace inspections&#39; permission for user 1.'
          . '!breakDoes not prohibit user 1 from <em>logging</em> inspections (see also <a href="@inspect_perms">Permissions</a>).'
          . '!breakAdmin user #1 - the omnipotent user created when setting up a Drupal site - is not restricted by any permissions set'
          . '!break(revoked, disabled) in the permissions dialogue. The means for modules of checking permissioms - <a href="@api_user_access">user_access()</a>'
          . '!break- simply returns TRUE for user #1, unconditionally.'
          . '!breakChecking this option amends that - however an even better way of handling this problem is never to use the admin user 1 account.'
        , array(
          '!break' => '<br/>',
          '@inspect_perms' => '/admin/people/permissions#module-inspect',
          '@api_user_access' => 'http://api.drupal.org/api/drupal/modules--user--user.module/function/user_access/7',
        )
    ),
    '#default_value' => variable_get('inspect_disallow_get_admin', FALSE),
    //'#access' => FALSE,
    '#required' => FALSE,
  );
  $form['#submit'][] = 'inspect_admin_form_submit';
  return system_settings_form($form);
}

/**
 * @param array $form
 * @param array &$form_state
 * @return void
 */
function _inspect_admin_form_submit($form, &$form_state) {
  //  Establish values for outmax lengths.
  $outputMaxes = InspectBs::outputMax();
  $values =& $form_state['values'];
  $values['inspect_outputmax_log'] = !($val = (int)trim($values['inspect_outputmax_log'])) || $val > $outputMaxes['log'] ?
      $outputMaxes['log'] : $val;
  $values['inspect_outputmax_get'] = !($val = (int)trim($values['inspect_outputmax_get'])) || $val > $outputMaxes['get'] ?
      $outputMaxes['get'] : $val;
  $values['inspect_exectime_percent'] = !($val = (int)trim($values['inspect_exectime_percent'])) || $val < 10 ? 10 : ($val > 90 ? 90 : $val);
}

/**
 * Creates help page.
 *
 * Implements hook_help().
 * @param string $path
 * @return string
 */
function _inspect_help($path) {
  if ($path != 'admin/help#inspect') {
    return;
  }
  $module = 'inspect';
  $headline_tag = 'h3';
  $paragraph_start = 'p class="admin-help-module-' . $module . '-paragraph"';
  $module_path = drupal_get_path('module', $module);

  drupal_add_css(
      $module_path . '/inspect.admin.css',
      array('type' => 'file', 'group' => CSS_DEFAULT, 'preprocess' => FALSE, 'every_page' => FALSE)
  );

  $out = '<div><div class="admin-help-module-' . $module . '-pane-first">';

  $out .= '
<' . $headline_tag . '>' . t('Inspect variables, constants, error- and back traces, and profile execution times') . '</' . $headline_tag . '>
<h4>' . t('Backend as well as frontend') . '</h4>
<' . $paragraph_start . '>'
  . t(
'More detailed documentation available here: <a href="@doc_backend" target="_blank">PHP</a> and <a href="@doc_frontend" target="_blank">Javascript</a>.',
    array(
      '@doc_backend' => 'http://www.simple-complex.net/docs/drupal/modules/inspect/php/inspect_8module.html',
      '@doc_frontend' => 'http://www.simple-complex.net/docs/drupal/modules/inspect/js/symbols/Inspect.html',
    )
  )
  . '</p>';
  $out .= '<br/>' . _inspect_help_php() . '<br/>' . _inspect_help_js();
  $out .= '</div><div class="admin-help-module-' . $module . '-pane-second">';
  //  Parse README.txt to html.
  $out .= '<a name="readme"></a><h3>Readme</h3>';
  $readme = @file_get_contents(
      substr(dirname(__FILE__), 0, -1 * (1 + strlen($module_path)))
      . '/' . drupal_get_path('module', 'inspect')
      . '/README.txt'
    );
  $out .= str_replace('<p>', '<' . $paragraph_start . '>', _inspect_readme2html($readme) )
      . '</div></div><hr/>';
  return $out;
}

/**
 * @return string
 */
function _inspect_help_php() {
  $options = array(
      '(integer) depth' => t(
          'default !default, max !max - $options as integer is interpretated as depth',
          array('!default' => InspectBs::DEPTH_DEFAULT, '!max' => InspectBs::DEPTH_MAX)
      ),
      '(integer) truncate' => t('default !default, min !max', array('!default' => InspectBs::TRUNCATE_DEFAULT, '!max' => InspectBs::TRUNCATE_MIN)),
      '(string) message' => t('will be truncated to 255 - $options as string is interpretated as message'),
      '(string) category' => t('logging category (default \'inspect\')'),
      '(integer) severity' => t('default WATCHDOG_DEBUG'),
      '(string) name' => t('\'$var_name\' or "\\$var_name", must be escaped'),
      '(bool) hide_scalars' => t('hide values of scalar vars, and type of resource'),
      '(bool) hide_paths' => t('only relevant for log (paths always hidden for get)'),
      '(bool) one_lined' => t('create one-lined formatting instead of multi-lined (and default depth 1)'),
  );
  $s = '<h5>' . t('(array) $options for PHP inspection functions') . ':</h5>'
      . '<ul>';
  foreach ($options as $k => $v) {
    $s .= '<li><span class="code">' . $k . '</span><span class="code weak">: ' . $v . '</span></li>';
  }
  $s .= '</ul>';
  $options = array(
      '(integer) limit' => t(
          'default !default, max !max - $options as integer is interpretated as limit',
          array('!default' => InspectTrc::TRACE_LIMIT_DEFAULT, '!max' => InspectTrc::TRACE_LIMIT_MAX)
      ),
      '(integer) depth' => t(
          'default !default',
          array('!default' => InspectTrc::DEPTH_DEFAULT)
      ),
      '(integer) truncate' => t('default !default, min !max', array('!default' => InspectTrc::TRUNCATE_DEFAULT, '!max' => InspectTrc::TRUNCATE_MIN)),
      '(string) category' => t('default \'inspect trace\''),
  );
  $s .= '<h5>' . t('(array) $options for PHP tracing functions - like inspection, except') . ':</h5>'
      . '<ul>';
  foreach ($options as $k => $v) {
    $s .= '<li><span class="code">' . $k . '</span><span class="code weak">: ' . $v . '</span></li>';
  }
  $s .= '</ul>';
  $s .= '<h5>' . t('Functions') . '</h5>';
  if (($funcs = inspect_doc_functions('inspect', '', array('inspect_log_get', 'inspect_trace_log_get')))) {
    foreach ($funcs as $v) {
      $s .= '<p>' . ($v[2] ? ($v[2] . '<br/>') : '')
          . '<span class="code emph">' . $v[0] . '</span>'
          . '<span class="code">' . $v[1] . '</span></p>';
    }
  }
  return $s;
}

/**
 * @return string
 */
function _inspect_help_js() {
  $options = array(
      '(integer) depth' => t(
          'default !default, max !max - options as integer is interpretated as depth',
          array('!default' => 10, '!max' => 10)
      ),
      '(string) message' => t('and options as string is interpretated as message'),
      '(bool) proto' => t('analyze prototypal properties too)'),
  );
  $funcs = array(
      array(
          'inspect',
          '(u, options)',
          'Inspect variable and send output to console log.',
      ),
      array(
          'inspect.variableGet',
          '(u, options)',
          'Inspect variable and get output as string.',
      ),
      array(
          'inspect.trace',
          '(er, message)',
          'Trace error and send output to console log.',
      ),
      array(
          'inspect.traceGet',
          '(er, message)',
          'Trace error and get output as string.',
      ),
      array(
          'inspect.argsGet',
          '(args, names)',
          'Analyze function arguments, or an options object.',
      ),
      array(
          'inspect.typeOf',
          '(u)',
          'Get type or class.',
      ),
      array(
          'inspect.console',
          '(message)',
          'Log to browser console, if exists.',
      ),
      array(
          'inspect.toggleOutput',
          '()',
          'Toggle display of all PHP inspection outputs on current page - or click the ¡!-button.',
      ),
      array(
          'inspect.toggleFolding',
          '()',
          'Toggle PHP inspection folding - in recent logs: click the ¡!-button.',
      ),
  );
  $s = '<h5>' . t('(object) options for Javascript inspect()') . ':</h5>'
      . '<ul>';
  foreach ($options as $k => $v) {
    $s .= '<li><span class="code">' . $k . '</span><span class="code weak">: ' . $v . '</span></li>';
  }
  $s .= '</ul>';

  $s .= '<h5>' . t('Functions') . '</h5>';
  foreach ($funcs as $v) {
    $s .= '<p>' . ($v[2] ? ($v[2] . '<br/>') : '')
          . '<span class="code emph">' . $v[0] . '</span>'
          . '<span class="code">' . $v[1] . '</span></p>';
  }
  return $s;
}

/**
 * Parse README.txt to html.
 *
 *   Replacements:
 *   - formats headlines; line with no hyphens followed by all hyphens line
 *   - hyphen to bullet; newline+space+*+space becomes newline+space+bullet+space
 *   - emphasizes underscore fragments; _what ever_ becomes <em>what ever</em>
 *   - turns url into link, and http://your-drupal-site.tld/some-path becomes internal link to /some-path
 *   - turns mail address into mailto link
 *
 * Some replacements (like emphasizing) only works for ascii letters, not for letters line like ñ or ö.
 * @param string $readme_txt
 * @param string $headline_tag
 *   - default: h5
 * @return string
 */
function _inspect_readme2html($readme_txt, $headline_tag = 'h5') {
  if (!strlen($readme_txt)) {
    return 'empty readme';
  }
  $ndls = array(
    '/</',
    '/>/',
    '/\r\n|\r/', // CR -> NL
    '/[\x20\t]+\n/', // trailing spaces (in lines)
    '/\n{3,}/', // 3 or more newlines to double
    '/<([^>@]+@[^>@]+)>/', // mail addresses
    '/([\n\x20])_([a-zA-Z\d][^\.,;\:\n\x20]*[a-zA-Z\d])_([\n\x20\.,;\:])/', // _emphasize_ -> <em>emphasize</em>
  );
  $rplcs = array(
    '&lt;',
    '&gt;',
    "\n",
    "\n",
    "\n\n",
    '<a href="mailto:$1">&#60;$1&#62;</a>',
    '$1<em>$2</em>$3',
  );
  $s = preg_replace($ndls, $rplcs, $readme_txt);

  //  Insert links in CONTENTS OF THIS FILE.
  if (strpos($s, 'CONTENTS OF THIS FILE') !== FALSE) {
    $pos_start = strpos($s, ' * ');
    $pos_end = strpos($s, "\n\n", $pos_start);
    $s_toc = preg_replace('/[ ]+\n/', "\n", substr($s, $pos_start, ($pos_end - $pos_start) + 1) ); // remove trailing space(s)
    //echo $s_toc; exit;
    $s_toc = preg_replace_callback(
      '/ \* ([^\n]+)\n/',
      create_function(
          '$ms',
          '
          return \' * <a href="#\' .
              strtolower( preg_replace(\'/[^a-zA-Z_\-]/\', \'_\', $ms[1]) )
              . \'">\' . $ms[1] . \'</a>\' . "\n";
          '
      ),
      $s_toc
    );
    $s_start = substr($s, 0, $pos_start);
    $s_end = substr($s, $pos_end);
    $s = str_replace('CONTENTS OF THIS FILE', 'CONTENTS', $s_start) . $s_toc . $s_end;
  }

  //  Format headlines, and insert anchor.
  $s = preg_replace_callback(
      '/\n([^\n\-])([^\n]*[^\n\-])\n[\-]{2,}\n+/',
      create_function(
          '$ms',
          '
          return \'</p><a name="\'
              . strtolower( preg_replace(\'/[^a-zA-Z_\-]/\', \'_\', $ms[1] . $ms[2]) )
              . \'"></a><headlineTag style="margin-top:20px;">\'
              . $ms[1]
              . \'<span style="text-transform:lowercase;">\' . $ms[2] . \'</span></headlineTag><p>\';
          '
      ),
      $s
    );
  $s = str_replace('headlineTag', $headline_tag, $s);

  $ndls = array(
    '/^[^\n]*\n/', // remove first line
    //'//',
  );
  $rplcs = array(
    '',
    //'',
  );
  $s = preg_replace($ndls, $rplcs, $s);
  //  Links may be followed by a punctuation marker, hard to detect without a callback.
  //  And http://your-drupal-site.tld/path should become /path
  $s = preg_replace_callback(
      '/(https?\:\/\/)(\S+)([\x20\t\n])/',
      create_function(
          '$ms',
          '
          $protocol = $ms[1];
          $le = strlen($addr = $ms[2]);
          $dot = \'\';
          if (!preg_match(\'/[\/a-zA-Z\d]/\', $addr{$le - 1})) {
            $dot = $addr{$le - 1};
            $addr = substr($addr, 0, $le - 1);
          }
          if (strpos($addr, \'your-drupal-site.tld/\') === 0) {
            $protocol = \'/\';
            $addr = str_replace(\'your-drupal-site.tld/\', \'\', $addr);
          }
          return \'<a href="\' . $protocol . $addr . \'">\' . $addr . \'</a>\' . $dot . $ms[3];
          '
      ),
      $s
    );
  $ndls = array(
    '/([\n>]) \* /', // \n *  to \n bullet
    '/^\n?<\/p>/', // first ending <p> added by headline (in total)
    '/\n?$/', // trailing newlines (in total)
    '/\n{2,}/',
    //'//',
  );
  $rplcs = array(
    '$1 &bull; ',
    '',
    '',
    '</p><p>',
    //'',
  );
  return nl2br( preg_replace($ndls, $rplcs, $s) ) . '</p>';
}