/**
 * @name window
 * @type {obj} ms
 */
if(window.Inspect === undefined) {
  (function($) {
    var cls = "module-inspect-collapsible", shw = 1, fld = -1;
    /**
     * Variable dumper and stack tracer.
     * This library has no external dependencies; it doesnt require another library (like jQuery).
     * Does however require jQuery, if folding of dump outputs is desired (doesnt fail without jQuery, simply doesnt do folding).
     * @function
     */
    window.Inspect = (function() {
      /**
       * Actually a function, but documented as object because it's core is an object.
       * Case insensitive - inspect() works just as well as Inspect().
       * See <a href="#.variable">Inspect.variable()</a> for argument details.
       * @see Inspect.variable()
       * @name Inspect
       * @constructor
       * @class
       * @singleton
       */
      var O = function() {
        var self = this,
        _dmnRgx = new RegExp( location.href.replace(/^(https?:\/\/[^\/]+)\/.*$/, "$1").replace(/([\/\.\-])/g,"\\"+"$1") ),
        _strQt = [">>", "<<"],
        _nspct, _oKeys;
        _nspct = function(u, protos, max, depth) { // doesnt find native prototypals
          var m = max !== undefined ? max : 10, d = depth || 0, t, isArr, p, pT, v, buf, s, isProt, nInst = 0, nProt = 0, ind;
          if(m > 10 || m < 0) {
            m = 10;
          }
          if(d < 11) {
            if(u === undefined) {
              return "undefined\n";
            }
            //  check type by typeof ---------------------
            switch((t = typeof u)) {
              case "object":
                if(u === null) {
                  return "object null\n";
                }
                break;
              case "boolean":
                return t + " " + u.toString() + "\n";
              case "string":
                return t + "(" + u.length + ") " + _strQt[0] + u + _strQt[1] + "\n";
            }
            //  check by typeOf() ------------------------
            ind = new Array(d + 2).join(".  ");
            switch((t = self.typeOf(u))) {
              case "number":
                return t + " " + u + "\n";
              case "NaN":
                return t + " NaN\n";
              case "infinite":
                return t + " infinite\n";
              case "regexp":
                return t + " " + u.toString() + "\n";
              case "date":
                return t + " " + (u.toISOString ? u.toISOString() : u) + "\n";
              case "function":
                //  cant make this work via regex
                buf = t + " " + (v = u.toString()).substr(v.indexOf("("), v.indexOf(")") - v.indexOf("(") + 1) + "\n";
                //  check for static members on function
                try {
                  for(p in u) {
                    if(u.hasOwnProperty(p) && p !== "prototype") {
                      buf += ind + ".. " + p + ": " + _nspct(u[p], protos, m - 1, d + 1); // recursion
                    }
                  }
                } catch(er1) {}
                return buf;
              case "window":  case "document":  case "document.documentElement":
                return t + "\n";
              case "element":
                return (t + "(" + (u.tagName ? u.tagName.toLowerCase() : "") +  // events may have elements, with no tagName
                    ") " + (u.id ? u.id : "-") + "|" + (u.className ? u.className : "-") +
                    (!(u = u.getAttribute("name")) ? "" : ("|"+u)) + "\n"
                );
              case "textNode":  case "attributeNode":  case "otherNode":
                s = t + (t === "otherNode" ? "(type:" + u.nodeType + ") " : " ");
                try { // dumping nodeValue in IE<8 will fail
                  return s + _nspct(u.nodeValue, protos, m - 1, d + 1); // recursion
                } catch(er2) {}
                return s + "Unknown node value (IE)\n";
              case "image":
                return t + "(" + u.src + ")\n";
              case "event":
                break;
            }
            //  object -------------------------------------------------------
            isArr = (t === "array");
            buf = ["", "", ""]; // instance properties, prototypal attributes, prototypal methods
            try {
              for(p in u) {
                pT = self.typeOf(v = u[p]);
                //  event misses hasOwnProperty method in some browsers, so we have to check if hasOwnProperty exists
                if(u.hasOwnProperty && u.hasOwnProperty(p)) {
                  if(isArr) { // do always check for non-numeric instance properties if array (error)
                    if((""+p).search(/\D/) > -1) {
                      buf[0] += "ERROR, non-numeric instance property ["+p+"] in array:\n";
                    }
                    else {
                      continue; // see next: if(isArr) {
                    }
                  }
                  ++nInst;
                  isProt = false;
                }
                else {
                  ++nProt;
                  if(!protos) {
                    continue;
                  }
                  isProt = true;
                }
                if(m > 0) {
                  s = p + ": " +
                      (v && pT === t && v === u ? (!d ? "ref THIS\n" : "ref SELF\n") : // reference check
                      _nspct(v, protos, m - 1, d + 1) ); // recursion
                  if(!isProt) {
                    buf[0] += (ind + s);
                  }
                  else {
                    buf[ pT !== "function" ? 1 : 2 ] += (ind + "... " + s);
                  }
                }
              }
              if(isArr) {
                nInst = u.length;
                if(m > 0) {
                  for(p = 0; p < nInst; p++) {
                    buf[0] += ind + p + ": " +
                        ((v = u[p]) && pT === t && v === u ? (!d ? "ref THIS\n" : "ref SELF\n") : // reference check
                        _nspct(v, protos, m - 1, d + 1) ); // recursion
                  }
                }
              }
            }
            catch(er) { // IE 8, for instance Element prototype methods
              //  - wrongly typed as object
              //  - has no toString() method, but can be stringified via ""+ (but only displays [native code]
              return "Function() wrongly typed as object (IE)?\n";
            }
            return t + "(" + nInst + (isArr ? "" : ("|" + nProt)) + ")" + "\n" + (m < 1 ? "" : (buf.join("")));
          }
          return "RECURSION LIMIT\n";
        };
        _oKeys = function(o) {
          var k, a = [];
          if(Object.keys) {
            return Object.keys(o);
          }
          for(k in o) {
            if(o.hasOwnProperty(k)) {
              a.push(k);
            }
          }
          return a;
        };
        /**
         * Use for checking if that window.Inspect is actually the one we are looking for (see example).
         * For the noactions type of this script, the value of .tcepsnI is 1 (one).
         * The name of the property is just something unlikely to exist (and the class name backwards).
         * @example
//  Check if Inspect exists, and that it's the 'active' type.
if(typeof window.inspect === "function" && inspect.tcepsnI === true) {
  inspect("whatever");
}
//  Simply check if Inspect exists, 'active' or not.
if(typeof window.inspect === "function" && inspect.tcepsnI) {
  inspect("whatever");
}
         * @name Inspect.tcepsnI
         * @type {bool}
         */
        this.tcepsnI = true;
        /**
         * Logs to browser console, if exists.
         * @function
         * @name Inspect.console
         * @param {str|mixed} ms
         * @return {void}
         */
        this.console = function(ms) {
          try { // detecting console is not possible, thus try-catch
            console.log("" + ms);
          }
          catch(er) {}
        };
        /**
         * All native types are reported in lowercase (like native typeof does).
         * If given no arguments: returns "inspect".
         * Types are:
         * - native, typeof: object string number
         * - native, corrected: function array date regexp image
         * - window, document, document.documentElement (not lowercase)
         * - element, checked via .getAttributeNode
         * - text node: textNode
         * - attribute node: attributeNode
         * - event: event (native and prototyped W3C Event and native IE event)
         * - jquery
         * - emptyish and bad: undefined, null, NaN, infinite
         * - custom or prototyped native: all classes having a typeOf() method.
         * RegExp is an object of type regexp (not a function - gecko/webkit/chromium).
         * Does not check if Date object is NaN.
         * @function
         * @name Inspect.typeOf
         * @param {mixed} u
         * @return {str}
         */
        this.typeOf = function(u) {
          var t = typeof u;
          if(!arguments.length) {
            return "inspect";
          }
          switch(t) {
            case "boolean":
            case "string":
                return t;
            case "number":
              return isFinite(u) ? t : (isNaN(u) ? "NaN" : "infinite");
            case "object":
              if(u === null) {
                return "null";
              }
              if(u.typeOf && typeof u.typeOf === "function") {
                return u.typeOf();
              }
              else if(typeof u.length === "number" && !(u.propertyIsEnumerable("length")) && typeof u.splice === "function") {
                return "array";
              }
              else if(u === window) {
                return "window";
              }
              else if(u === document) {
                return "document";
              }
              else if(u === document.documentElement) {
                return "document.documentElement";
              }
              else if(u.getAttributeNode) { // element
                //  document has getElementsByTagName, but not getAttributeNode -  document.documentElement has both
                return u.tagName.toLowerCase === "img" ? "image" : "element";
              }
              else if(u.nodeType) {
                switch(u.nodeType) {
                  case 3:return "textNode";
                  case 2:return "attributeNode";
                }
                return "otherNode";
              }
              else if(typeof u.stopPropagation === "function" ||
                  (u.cancelBubble !== undefined && typeof u.cancelBubble !== "function" &&
                  typeof u.boundElements === "object")) {
                return "event";
              }
              else if(typeof u.getUTCMilliseconds === "function") {
                return "date";
              }
              else if(typeof u.exec === "function" && typeof u.test === "function") {
                return "regexp";
              }
              else if(u.hspace && typeof u.hspace !== "function") {
                return "image";
              }
              else if(u.jquery && typeof u.jquery === "string" && !u.hasOwnProperty("jquery")) {
                return "jquery";
              }
              return t;
            case "function":
              //  gecko and webkit reports RegExp as function instead of object
              return (u.constructor === RegExp || (typeof u.exec === "function" && typeof u.test === "function")) ?
                  "regexp" : t;
          }
          return t;
        };
        /**
         * Inspect variable and send output to console log.
         * Object options flags (any number of):
         *  - (int) depth (default and absolute max: 10)
         *  - (str) message (default empty)
         *  - (bool) protos (default not: do only report number of prototypal properties of objects)
         * Reference checks:
         *  - only child versus immediate parent
         *  - doesnt allow to recurse deeper than 10
         * Prototype properties are marked with prefix "... ", function's static members with prefix ".. ".
         * Detects if an Array property doesnt have numeric key.
         * Risky procedures are performed within try-catches.
         * @function
         * @name Inspect.variable
         * @param {mixed} u
         * @param {obj|int|str} [options]
         *  - obj: options
         *  - int: depth
         *  - str: message
         * @return {void}
         */
        this.variable = function(u, options) {
          //  find call (first trace line outside this file)
          var ar, le, i, v, p, fl = "";
          try {
            throw new Error();
          }
          catch(er) {
            ar = self.traceGet(er);
          }
          if(typeof ar === "string" && ar.indexOf("\n") && (le = (ar = ar.split("\n")).length)) {
            for(i = 0; i < le; i++) {
              if(i && (p = (v = ar[i]).lastIndexOf("@/")) !== -1 && v.indexOf("/inspect/js/inspect") === -1) {
                fl = " " + v.substr(p);
                break;
              }
            }
          }
          self.console("[Inspect" + fl + "]\n" + self.variableGet(u, options));
        };
        /**
         * Inspect variable and get output as string.
         * @function
         * @name Inspect.variableGet
         * @param {mixed} u
         * @param {obj|int|str} [options]
         * @return {str}
         */
        this.variableGet = function(u, options) {
          var o = options, ms = "", ps = false, m, v;
          if(o) {
            switch(typeof o) {
              case "string":
                ms = o;
                break;
              case "object":
                if(o.hasOwnProperty("depth")) {
                  m = o.depth;
                }
                if((v = o.message) && o.hasOwnProperty("message")) {
                  ms = v;
                }
                if((v = o.protos) && o.hasOwnProperty("protos")) {
                  ps = v;
                }
                break;
              case "number":
                if(isFinite(o)) {
                  m = o;
                }
                break;
            }
          }
          else if(o === 0) {
            m = 0;
          }
          return (!ms ? "" : (ms+":\n")) + _nspct(u, ps, m);
        };
        /**
         * Trace error and send output to console log.
         * @example try {
         *   throw new Error("¿Que pasa?");
         * }
         * catch(er) {
         *   Inspect.trace(er, "Class.method()");
         * }
         * @function
         * @name Inspect.trace
         * @param {Error} er
         * @param {str} [ms] - optional message to prepend
         * @return {void}
         */
        this.trace = function(er, ms) {
          self.console( self.traceGet(er, ms) );
        };
        /**
         * Trace error and get output as string.
         * @function
         * @name Inspect.traceGet
         * @param {Error} er
         * @param {str} [ms]
         *  - optional message to prepend
         * @return {str}
         */
        this.traceGet = function(er, ms) {
          var u, le, i, es = ""+er, s = (!ms ? "" : (ms + "()\n")) + es;
          if((u = er.stack)) { // gecko, chromium
            if((le = (u = (u.replace(/\r/, "").split(/\n/))).length)) {
              i = u[0] === es ? 1 : 0; // chromium first line is error toString()
              for(i; i < le; i++) {
                s += "\n  " + u[i].replace(/^[\ ]+at\ /, "@"). // chromium as gecko
                    replace(_dmnRgx, "");
              }
            }
            return s;
          }
          return er; // ie
        };
        /**
         * Analyze function arguments, or an options object.
         * @function
         * @name Inspect.argsGet
         * @param {obj|collection|arr} args
         * @param {arr} [names]
         *  - optional array of argument names
         * @return {str}
         */
        this.argsGet = function(args, names) {
          var a = args, t = self.typeOf(a), le, keys, nKs, s, i;
          if(!a) {
            return "falsy arg args";
          }
          if(t === "array") {
            if(!(le = a.length)) {
              return "0: none";
            }
            nKs = !(keys = names) ? 0 : keys.length; // use arg names as keys
          }
          else if(!(le = ((keys = _oKeys(a)).length))) { // arguments or object having no length
            return "0: none";
          }
          else if(keys[0] === "0") { // arguments, arrayish
            t = "array";
            nKs = !(keys = names) ? 0 : keys.length; // use arg names as keys
          }
          else { // object
            nKs = le;
            s = ""+le+":";
          }
          s = ""+le+":";
          for(i = 0; i < le; i++) {
            s += (i ? ", " : " ") + "#" + i + (nKs <= i ? "" : ("("+keys[i]+")")) + " " +
                _nspct(t === "array" ? a[i] : a[ keys[i] ], "", 0).
                replace(/\n/, ""); // get rid of trailing newline
          }
          return s;
        };
      },
      f;
      O = new O();
      f = function(u, options) {
        return O.variable(u, options);
      };
      f.tcepsnI = true;
      f.console = function(ms) {
        return O.console(ms);
      };
      f.typeOf = function(u) {
        return arguments.length ? O.typeOf(u) : "inspect";
      };
      f.variable = function(u, options) {
        return O.variable(u, options);
      };
      f.variableGet = function(u, options) {
        return O.variableGet(u, options);
      };
      f.trace = function(er, ms) {
        return O.trace(er, ms);
      };
      f.traceGet = function(er, ms) {
        return O.traceGet(er, ms);
      };
      f.argsGet = function(args, names) {
        return O.argsGet(args, names);
      };
      /**
       * Toggle display of all inspection outputs on current page.
       * This function is always implemented, even for no-action no-fold.
       * @function
       * @name Inspect.toggleOutput
       * @return {void}
       */
      f.toggleDisplay = function() {
        var u;
        if(shw > 0) {
          $("." + cls).hide();
        }
        else if(fld < 0) {
          $("pre."+ cls).show();
        }
        else {
          $("pre."+ cls).each(function(){
            $((u = $(this).prev("div."+ cls).get(0)) ? u : this).show();
          });
        }
        shw *= -1;
      };
      /**
       * Toggle inspection folding, if folding is enabled.
       * Folding is not supported for IE<9.
       * @function
       * @name Inspect.toggleFolding
       * @return {void}
       */
      f.toggleFolding = function() {
      };
      return f;
    })();
    if(window.inspect === undefined) {
      window.inspect = window.Inspect;
    }
    //  fold output ------------------------------
    if(!$ ||
        ($.browser.msie && parseFloat($.browser.version.replace(/^(\d+)\..+/, "$1")) < 9)) { // no good in IE<9
      return;
    }
    $(document).bind("ready", function(){
      fld = 1;
      window.Inspect.toggleFolding = function() {
        if(fld > 0) {
          $("div." + cls).hide();
          $("pre." + cls).show();
        }
        else {
          $("pre." + cls).hide();
          $("div." + cls).show();
        }
        fld *= -1;
        shw = 1;
      };
      var linesMax = 10000,
      nl = "<br/>", ds = "<div>", de = "</div>", sp = "<span>", se = "</span>",
      ps = "<div class=\"inspect-cllpsbl cllpsd\"><div class=\"inspect-prnt\">",
      b1 = "<span class=\"inspect-xpnd\">\u25b6" + se,
      b2 = "<span class=\"inspect-xpnd-all\">\u25b6\u25b6" + se,
      cs = "<div class=\"inspect-chldrn\">",
      cc = "<div class=\"inspect-chld\">",
      kp = "<span class=\"inspect-kypth\" title=\"",
      k1 = "\">" + sp, // span for measuring width
      k2 = se + "<form><div><input type=\"text\" value=\"",
      k3 = "\"/><div title=\"close\">&#215;</div></div></form>",
      sw = "<span class=\"warn\">",
      st = "<span class=\"trunc\">",
      tglCls = "module-inspect-toggle",
      rprts = location.href.indexOf("admin/reports/event/") > -1,
      fTgl = window.inspect[ "toggle" + (rprts ? "Folding" : "Display") ],
      tg = "<div class=\"" + tglCls + "\"><div title=\"toggle " + (rprts ? "folding" : "display") + "\">&#0161;!" + de + de,
      font = "font-family:Menlo,Consolas,Andale Mono,Lucida Console,Nimbus Mono L,DejaVu Sans Mono,monospace,Courier New;",
      ndntRgx = /^(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?.*$/,
      ndntRpl = "$1$2$3$4$5$6$7$8$9$10",
      trprRgx = /\(string\:(\d+)\|(\d+)\|(\d+)\|\!\) &gt;&gt;\.{3}(.*)\.{3}&lt;&lt;$/,
      trRgx = /\(string\:(\d+)\|(\d+)\|(\d+)\) &gt;&gt;(.*)\.{3}&lt;&lt;$/,
      prRgx = /\(string\:(\d+)\|(\d+)\|\-\|\!\) &gt;&gt;\.{3}(.*)&lt;&lt;$/,
      ctRgx = /\(([a-zA-Z_][a-zA-Z\d_]*)\:(\d+)\) (\[\.{3}\]|\{\.{3}\})/,
      erRgx = /\(([a-zA-Z_][a-zA-Z\d_]*)(\:)?([\d\?\|]+)*\) \*([A-Z\d_]+)\*/,
      first = true,
      prfCls = "inspect-profile", // profiler
      /** Toggle key path display.
       * @return {void}
       */
      tglKyPth = function() {
        var jqFrm = $("form", this), elm, w;
        if(jqFrm.css("display") === "none") {
          elm = ($("input", this).
              unbind("click"). // remove slctKyPth
              click(slctKyPth).
              css("width", (w = $("span", this).innerWidth()) + "px")).get(0);
          $("form>div", this).css("width", (w + 16) + "px");
          jqFrm.css("display", "inline");
          $(this).click(tglKyPth); // enable single click close
          elm.focus(); // unselect double clicked word
        }
        else {
          jqFrm.css("display", "none");
          $(this).unbind("click"); // prevent single click open
        }
      },
      slctKyPth = function(e) {
        e.stopPropagation();
        this.select();
        $(this).unbind("click", slctKyPth). // prevent continuous selection
            click(function(e){e.stopPropagation();}); // prevent single click close (on input, not form)
      },
      /**
       * Folding of backend generated dump outputs on page.
       * @ignore
       * @param {arr} lines
       * @param {int} first
       * @param {int} last
       * @param {arr} path
       * @return {arr|false}
       *  - [ (str) buffer, (int) first, (bool|undef) deep ]
       */
      fold = function(lines, first, last, path, top) {
        var start = first, j = 0, buf = "", line, obj, arr, i, stop, ndnt, a, deep, pth = path.concat(), kyPth;
        while(start <= last && (++j) < 10000) { // not linesMax, simply loop limiter
          obj = arr = stop = false;
          line = lines[start];
          //  find bucket name
          pth.push(
              (!/^\.\s\s/.test(line) ? "" : line.replace(/^(\.\s\s)+(.*?)\:\s\(.+$/, "$2")) +
              (!/\[\'$/.test(pth[pth.length - 1]) ? "" : "']")
          );
          if((obj = /\x7B$/.test(line)) || (arr = /\x5B$/.test(line))) { // ends with { or [
            if(last === start) {
              return false; // container start and end cant be on same line
            }
            deep = true;
            //  add container type
            pth[ pth.length - 1 ] += ((obj && !/\(arra[y]\:/.test(line)) ? "->" : "['");

            buf += ps +
                (ndnt = line.indexOf(".  ") !== 0 ? "" : line.replace(ndntRgx, ndntRpl)) + // remove indentation
                b1;
            ++start;
            //  find end
            for(i = start; i < last + 1; i++) {
              if((obj && lines[i] === ndnt + "}") || (arr && lines[i] === ndnt + "]")) {
                stop = i;
                if(!(a = fold(lines, start, stop - 1, pth))) {
                  return false; // error
                }
                buf += (!a[2] ? "" : b2) + "&nbsp;" +
                    (!top ?
                      (kp + (kyPth = pth.join("").replace(/\[\'$|\-\>$/, "").replace(/\[\'(\d+)\'\]/g, "[$1]")) +
                        k1 + kyPth + k2 + kyPth + k3) :
                      sp // no key path for top var
                    ) +
                    line.replace(/^(\.  )+/, "") + se +
                    de + cs +
                    a[0] +
                    cc +
                    lines[i] +
                    de + de + de;
                pth.pop();
                start = a[1] + 1; // 1 to prevent double ending
                break;
              }
            }
            if(!stop) {
              return false; // error, found no ending } or ]
            }
          }
          else { // non-container var
            ++start;
            ndnt = line.indexOf(".  ") !== 0 ? "" : line.replace(ndntRgx, ndntRpl);
            buf += cc +
              (ndnt = line.indexOf(".  ") !== 0 ? "" : line.replace(ndntRgx, ndntRpl)) + // remove indentation
              (!top ?
                (kp + (kyPth = pth.join("").replace(/\[\'(\d+)\'\]/g, "[$1]")) + k1 + kyPth + k2 + kyPth + k3) :
                (!/^Inspection aborted/.test(line) ? sp : sw) // no key path for top var
              ) +
              line.replace(/^(\.  )+/, "").
                  replace( // highlight string truncation and path removal
                      trprRgx,
                      "(string:$1|$2|" + st + "$3|!" + se + ") " + st + "&gt;&gt;..." + se + "$4" + st + "...&lt;&lt;" + st
                  ).
                  replace( // highlight string truncation
                      trRgx,
                      "(string:$1|$2|" + st + "$3" + se + ") " + st + "&gt;&gt;" + se + "$4" + st + "...&lt;&lt;" + st
                  ).
                  replace( // highlight path removal (non-truncated)
                      prRgx,
                      "(string:$1|$2|-|" + st + "!" + se + ") " + st + "&gt;&gt;..." + se + "$3" + st + "&lt;&lt;" + st
                  ).
                  replace(ctRgx, "($1:$2) " + st + "$3" + se). // highlight container truncation
                  replace(erRgx, "($1$2$3) " + sw + "*$4*" + se). // highlight error
                  replace(/_NL_/g, "_NL_<br/>") +
                  se + de;
            pth.pop();
          }
        }
        return [buf, start, deep];
      },
      prof = function(lines) {
        var s = "", le = lines.length, i, line, j = 0, ttl;
        for(i = 0; i < le; i++) {
          if(/^.+\|/.test(line = lines[i])) {
            //  truncate log, set as title
            if((ttl = !/^([\s\d]{0,4}\d)/.test(line) || /.+\|\s$/.test(line) ? "" : line.replace(/^.+\|\s([^\|]+)$/, "$1"))) {
              line = line.replace(/^(.+\|\s)[^\|]+$/, "$1") + ttl.substr(0, 32);
              ttl = " title=\"" + ttl + "\"";
            }
            s += "<div class=\"inspect-" + (((++j) % 2) ? "odd" : "even") + "\"" + ttl + ">"; // even/odd
          }
          else {
            j = 0;
            s += ds;
          }
          s += line.replace(/\s/g, "\u00A0") + de;
        }
        return s;
      },
      t;
      //  goes thru all outputs, adds div.module-inspect-collapsible, hides pre.module-inspect-collapsible
      $("pre." + cls).each(function(){
        var buf = "", jq = $(this), lines = jq.html().replace(/\r\n/, "\n").split(/\n/), le = lines.length,
          i, j = -1, a, css = "", nm = "", prf = false;
        //  filter message (everything before [Inspect)
        for(i = 0; i < le; i++) {
          if(/^\[Inspect/.test(lines[i])) {
            j = i;
            nm = (lines[i]).replace(/^\[Inspect[^\]]+\]\s?(.+)?/, "$1");
            break;
          }
        }
        if(j === -1) {
          return; // error
        }
        //  add lines before [Inspect...
        for(i = 0; i < j; i++) {
          buf += (i == 0 ? "" : nl) + lines[i];
        }
        //  remove lines before [Inspect...
        lines.splice(0, j);
        buf += ds + lines[0] + de; // [Inspect...
        //  inspect profile: dont fold
        if(jq.hasClass(prfCls)) {
          prf = true;
          lines.splice(0, 1);
          buf += prof(lines);
        }
        else {
          //  remove file_line
          if(lines.length > 1 && // content truncation (Read more) may shorten output to a single line
              lines[1].indexOf("@") === 0) {
            buf += ds + lines[1] + de;
            lines.splice(1, 1);
          }
          if((le = lines.length) <= linesMax) {
            if(!(a = fold(lines, 1, le - 1, [nm], true))) {
              return; // error
            }
            buf += a[0];// + de;
          }
        }
        if(first) {
          first = false;
          css = "<style type=\"text/css\">" +
"div." + tglCls + "{position:relative;display:block;background:#FFF;font-size:1px;}" +
"div." + tglCls + " div{position:absolute;display:inline-block;left:-2px;top:-2px;padding:0 2px;line-height:100%;cursor:pointer;" +
"background:#FFF;border-radius:2px;border:1px solid #CC6060;box-shadow:0 0 10px #CC0000;-moz-box-shadow:0 0 6px #CC0000;" + font +
"text-align:center;vertical-align:top;font-size:10px;font-weight:bold;color:#000;" +
"-moz-user-select:none;-khtml-user-select:none;-ms-user-select:none;}" +
"div." + tglCls + " div:hover{color:#FFF;background:#666;}" +
"div." + cls + "{display:block;background:#FFF;padding:7px;border:1px solid #999;font-size:11px;" + font + "}" +
"div.inspect-prnt:hover,div.inspect-chld:hover{background:#EEE;}" +
"div.inspect-chld span.warn{color:#CC0000;}div.inspect-chld span.trunc{color:#0000FF;}" +
"span.inspect-xpnd,span.inspect-xpnd-all{display:inline-block;cursor:pointer;color:#999;padding:0 2px;}" +
"span.inspect-xpnd.hover,span.inspect-xpnd-all.hover{color:#000;}div.inspect-cllpsbl.cllpsd div.inspect-chldrn{display:none}" +
"span.inspect-kypth,span.inspect-kypth span,span.inspect-kypth form,span.inspect-kypth div,span.inspect-kypth input{" +
"background:transparent;border:0;padding:0;margin:0;font-size:11px;" + font + "}" +
"span.inspect-kypth{cursor:pointer;}" +
"span.inspect-kypth:hover{background:#D8E8F7;}" +
"span.inspect-kypth span{position:absolute;visibility:hidden;}" +
"span.inspect-kypth span.trunc{position:static;visibility:visible;color:#0000FF;}" +
"span.inspect-kypth span.warn{position:static;visibility:visible;color:#CC0000;}" +
"span.inspect-kypth form{display:none;position:relative;}" +
"span.inspect-kypth form>div{display:inline-block;position:absolute;box-shadow:0 0 3px #2F78C0;background:#94B8D8;border-radius:2px;}" +
"span.inspect-kypth input{padding:0px 1px;border-radius:2px;background:#FFF;}" +
"span.inspect-kypth div div{display:inline-block;" +
"color:#FFF;font-weight:bold;font-size:12px;line-height:100%;width:10px;text-align:right;background:transparent;}" +
"span.inspect-kypth div div:hover{color:#000;}" +
"div." + prfCls + " div{white-space:nowrap;}" +
"div." + prfCls + " div.inspect-even{background:#EEE;}div." + prfCls + " div.inspect-even:hover{background:#D8E8F7;}" +
"div." + prfCls + " div.inspect-odd:hover{background:#ECF4FB;}" +
"</style>";
        }
        jq.before(
            css + tg +
            //  no div.module-inspect-collapsible if no folding
            (le <= linesMax ? ("<div class=\"" + cls + (!prf ? "" : (" " + prfCls)) + "\">" + buf + de) : "")
          );
        if(le <= linesMax) {
          jq.hide();
        }
        else {
          jq.prepend("   * Folding off, number of output lines " + le + " exceeds max " + linesMax + ", try less depth *\n");
        }
      });
      //  adds button events, and locks width
      t = setTimeout(function(){
        //  set .toggleFolding() or .toggleDisplay() on
        $("div." + tglCls + " div").click(fTgl);
        //  fixate width of expansible inspection outputs' outer container
        $("div." + cls).each(function(){
          var jq = $(this), w, p;
          if(!jq.hasClass(prfCls)) {
            jq.css({
              width: w = (jq.innerWidth() - 16) + "px",
              "max-width": w
            });
          }
          else if((p = jq.css("position")) !== "absolute" && p !== "fixed") { // force optimal width
            jq.css("position", "absolute");
            w = (jq.innerWidth() - 8) + "px";
            jq.css({
              position: p,
              width: w,
              "max-width": "auto"
            });
          }
        });
        //  for each collapsible: set expand/collapse events on all buttons
        $("div.inspect-cllpsbl").each(function(){
          var par = this,
            bt = $("span.inspect-xpnd", par).get(0), btAll = $("span.inspect-xpnd-all", par).get(0),
            btChldrn = [], chld;
          $("div.inspect-cllpsbl", par).each(function(){
            if((chld = $("span.inspect-xpnd", this).get(0))) {
              btChldrn.push(chld);
            }
          });
          $(bt).click(function(event, on, off){
            var jq = $(par), show = on ? true : (off ? false : jq.hasClass("cllpsd"));
            $(this).text(show ? "\u25bc" : "\u25b6");
            jq[show ? "removeClass" : "addClass"]("cllpsd");
            if(!show && btAll) { // if hide: collapse sibling button
              $(btAll).text("\u25b6\u25b6");
            }
          }).mouseover(function(){
            $(this).addClass("hover");
          }).mouseout(function(){
            $(this).removeClass("hover");
          });
          //  works via the single level buttons
          if(btAll) {
            $(btAll).click(function(){
              var show = $(this).text() === "\u25b6\u25b6" ? true : false;
              if(show) {
                $(this).text("\u25bc\u25bc");
              }
              $(bt).trigger("click", [show, !show]); // click sibling
              $(btChldrn).each(function(){ // click children
                var btAll; // local
                $(this).trigger("click", [show, !show]);
                //  if has expand-all sibling, make it show that all is expanded/collapsed
                if((btAll = $(this).next("span.inspect-xpnd-all").get(0))) {
                  $(btAll).text(show ? "\u25bc\u25bc" : "\u25b6\u25b6");
                }
              });
            }).mouseover(function(){
              $(this).addClass("hover");
            }).mouseout(function(){
              $(this).removeClass("hover");
            });
          }
        });
        //  add show key path behaviour
        $("span.inspect-kypth").dblclick(tglKyPth).bind("contextmenu", function(e){
          e.preventDefault();
          tglKyPth.apply(this, []);
        });
      },100);
    });
  })(jQuery);
}