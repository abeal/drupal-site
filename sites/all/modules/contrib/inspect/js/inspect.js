/**
 * @name window
 * @type {obj} ms
 */
if(window.Inspect === undefined) {
  (function($) {
    var cls = "module-inspect-collapsible", shw = 1, fld = -1;
    /**
     * Variable dumper and stack tracer.
     * This library has no external dependencies; it doesnt require another library (like jQuery).
     * Does however require jQuery, if folding of dump outputs is desired (doesnt fail without jQuery, simply doesnt do folding).
     * @function
     */
    window.Inspect = (function() {
      /**
       * Actually a function, but documented as object because it's core is an object.
       * Case insensitive - inspect() works just as well as Inspect().
       * See <a href="#.variable">Inspect.variable()</a> for argument details.
       * @see Inspect.variable()
       * @name Inspect
       * @constructor
       * @class
       * @singleton
       */
      var O = function() {
        var self = this,
        _dmnRgx = new RegExp( location.href.replace(/^(https?:\/\/[^\/]+)\/.*$/, "$1").replace(/([\/\.\-])/g,"\\"+"$1") ),
        _strQt = [">>", "<<"],
        _nspct, _oKeys;
        _nspct = function(u, protos, max, depth) { // doesnt find native prototypals
          var m = max !== undefined ? max : 10, d = depth || 0, t, isArr, p, pT, v, buf, s, isProt, nInst = 0, nProt = 0, ind;
          if(m > 10 || m < 0) {
            m = 10;
          }
          if(d < 11) {
            if(u === undefined) {
              return "undefined\n";
            }
            //  check type by typeof ---------------------
            switch((t = typeof u)) {
              case "object":
                if(u === null) {
                  return "object null\n";
                }
                break;
              case "boolean":
                return t + " " + u.toString() + "\n";
              case "string":
                return t + "(" + u.length + ") " + _strQt[0] + u + _strQt[1] + "\n";
            }
            //  check by typeOf() ------------------------
            ind = new Array(d + 2).join(".  ");
            switch((t = self.typeOf(u))) {
              case "number":
                return t + " " + u + "\n";
              case "NaN":
                return t + " NaN\n";
              case "infinite":
                return t + " infinite\n";
              case "regexp":
                return t + " " + u.toString() + "\n";
              case "date":
                return t + " " + (u.toISOString ? u.toISOString() : u) + "\n";
              case "function":
                //  cant make this work via regex
                buf = t + " " + (v = u.toString()).substr(v.indexOf("("), v.indexOf(")") - v.indexOf("(") + 1) + "\n";
                //  check for static members on function
                try {
                  for(p in u) {
                    if(u.hasOwnProperty(p) && p !== "prototype") {
                      buf += ind + ".. " + p + ": " + _nspct(u[p], protos, m - 1, d + 1); // recursion
                    }
                  }
                } catch(er1) {}
                return buf;
              case "window":  case "document":  case "document.documentElement":
                return t + "\n";
              case "element":
                return (t + "(" + (u.tagName ? u.tagName.toLowerCase() : "") +  // events may have elements, with no tagName
                    ") " + (u.id ? u.id : "-") + "|" + (u.className ? u.className : "-") +
                    (!(u = u.getAttribute("name")) ? "" : ("|"+u)) + "\n"
                );
              case "textNode":  case "attributeNode":  case "otherNode":
                s = t + (t === "otherNode" ? "(type:" + u.nodeType + ") " : " ");
                try { // dumping nodeValue in IE<8 will fail
                  return s + _nspct(u.nodeValue, protos, m - 1, d + 1); // recursion
                } catch(er2) {}
                return s + "Unknown node value (IE)\n";
              case "image":
                return t + "(" + u.src + ")\n";
              case "event":
                break;
            }
            //  object -------------------------------------------------------
            isArr = (t === "array");
            buf = ["", "", ""]; // instance properties, prototypal attributes, prototypal methods
            try {
              for(p in u) {
                pT = self.typeOf(v = u[p]);
                //  event misses hasOwnProperty method in some browsers, so we have to check if hasOwnProperty exists
                if(u.hasOwnProperty && u.hasOwnProperty(p)) {
                  if(isArr) { // do always check for non-numeric instance properties if array (error)
                    if((""+p).search(/\D/) > -1) {
                      buf[0] += "ERROR, non-numeric instance property ["+p+"] in array:\n";
                    }
                    else {
                      continue; // see next: if(isArr) {
                    }
                  }
                  ++nInst;
                  isProt = false;
                }
                else {
                  ++nProt;
                  if(!protos) {
                    continue;
                  }
                  isProt = true;
                }
                if(m > 0) {
                  s = p + ": " +
                      (v && pT === t && v === u ? (!d ? "ref THIS\n" : "ref SELF\n") : // reference check
                      _nspct(v, protos, m - 1, d + 1) ); // recursion
                  if(!isProt) {
                    buf[0] += (ind + s);
                  }
                  else {
                    buf[ pT !== "function" ? 1 : 2 ] += (ind + "... " + s);
                  }
                }
              }
              if(isArr) {
                nInst = u.length;
                if(m > 0) {
                  for(p = 0; p < nInst; p++) {
                    buf[0] += ind + p + ": " +
                        ((v = u[p]) && pT === t && v === u ? (!d ? "ref THIS\n" : "ref SELF\n") : // reference check
                        _nspct(v, protos, m - 1, d + 1) ); // recursion
                  }
                }
              }
            }
            catch(er) { // IE 8, for instance Element prototype methods
              //  - wrongly typed as object
              //  - has no toString() method, but can be stringified via ""+ (but only displays [native code]
              return "Function() wrongly typed as object (IE)?\n";
            }
            return t + "(" + nInst + (isArr ? "" : ("|" + nProt)) + ")" + "\n" + (m < 1 ? "" : (buf.join("")));
          }
          return "RECURSION LIMIT\n";
        };
        _oKeys = function(o) {
          var k, a = [];
          if(Object.keys) {
            return Object.keys(o);
          }
          for(k in o) {
            if(o.hasOwnProperty(k)) {
              a.push(k);
            }
          }
          return a;
        };
        /**
         * Use for checking if that window.Inspect is actually the one we are looking for (see example).
         * For the noactions type of this script, the value of .tcepsnI is 1 (one).
         * The name of the property is just something unlikely to exist (and the class name backwards).
         * @example
//  Check if Inspect exists, and that it's the 'active' type.
if(typeof window.inspect === "function" && inspect.tcepsnI === true) {
  inspect("whatever");
}
//  Simply check if Inspect exists, 'active' or not.
if(typeof window.inspect === "function" && inspect.tcepsnI) {
  inspect("whatever");
}
         * @name Inspect.tcepsnI
         * @type {bool}
         */
        this.tcepsnI = true;
        /**
         * Logs to browser console, if exists.
         * @function
         * @name Inspect.console
         * @param {str|mixed} ms
         * @return {void}
         */
        this.console = function(ms) {
          try { // detecting console is not possible, thus try-catch
            console.log("" + ms);
          }
          catch(er) {}
        };
        /**
         * All native types are reported in lowercase (like native typeof does).
         * If given no arguments: returns "inspect".
         * Types are:
         * - native, typeof: object string number
         * - native, corrected: function array date regexp image
         * - window, document, document.documentElement (not lowercase)
         * - element, checked via .getAttributeNode
         * - text node: textNode
         * - attribute node: attributeNode
         * - event: event (native and prototyped W3C Event and native IE event)
         * - jquery
         * - emptyish and bad: undefined, null, NaN, infinite
         * - custom or prototyped native: all classes having a typeOf() method.
         * RegExp is an object of type regexp (not a function - gecko/webkit/chromium).
         * Does not check if Date object is NaN.
         * @function
         * @name Inspect.typeOf
         * @param {mixed} u
         * @return {str}
         */
        this.typeOf = function(u) {
          var t = typeof u;
          if(!arguments.length) {
            return "inspect";
          }
          switch(t) {
            case "boolean":
            case "string":
                return t;
            case "number":
              return isFinite(u) ? t : (isNaN(u) ? "NaN" : "infinite");
            case "object":
              if(u === null) {
                return "null";
              }
              if(u.typeOf && typeof u.typeOf === "function") {
                return u.typeOf();
              }
              else if(typeof u.length === "number" && !(u.propertyIsEnumerable("length")) && typeof u.splice === "function") {
                return "array";
              }
              else if(u === window) {
                return "window";
              }
              else if(u === document) {
                return "document";
              }
              else if(u === document.documentElement) {
                return "document.documentElement";
              }
              else if(u.getAttributeNode) { // element
                //  document has getElementsByTagName, but not getAttributeNode -  document.documentElement has both
                return u.tagName.toLowerCase === "img" ? "image" : "element";
              }
              else if(u.nodeType) {
                switch(u.nodeType) {
                  case 3:return "textNode";
                  case 2:return "attributeNode";
                }
                return "otherNode";
              }
              else if(typeof u.stopPropagation === "function" ||
                  (u.cancelBubble !== undefined && typeof u.cancelBubble !== "function" &&
                  typeof u.boundElements === "object")) {
                return "event";
              }
              else if(typeof u.getUTCMilliseconds === "function") {
                return "date";
              }
              else if(typeof u.exec === "function" && typeof u.test === "function") {
                return "regexp";
              }
              else if(u.hspace && typeof u.hspace !== "function") {
                return "image";
              }
              else if(u.jquery && typeof u.jquery === "string" && !u.hasOwnProperty("jquery")) {
                return "jquery";
              }
              return t;
            case "function":
              //  gecko and webkit reports RegExp as function instead of object
              return (u.constructor === RegExp || (typeof u.exec === "function" && typeof u.test === "function")) ?
                  "regexp" : t;
          }
          return t;
        };
        /**
         * Inspect variable and send output to console log.
         * Object options flags (any number of):
         *  - (int) depth (default and absolute max: 10)
         *  - (str) message (default empty)
         *  - (bool) protos (default not: do only report number of prototypal properties of objects)
         * Reference checks:
         *  - only child versus immediate parent
         *  - doesnt allow to recurse deeper than 10
         * Prototype properties are marked with prefix "... ", function's static members with prefix ".. ".
         * Detects if an Array property doesnt have numeric key.
         * Risky procedures are performed within try-catches.
         * @function
         * @name Inspect.variable
         * @param {mixed} u
         * @param {obj|int|str} [options]
         *  - obj: options
         *  - int: depth
         *  - str: message
         * @return {void}
         */
        this.variable = function(u, options) {
          var ar, le, i, v, p, fl = "";
          //  find call (first trace line outside this file)
          try {
            throw new Error();
          }
          catch(er) {
            ar = self.traceGet(er);
          }
          if(typeof ar === "string" && ar.indexOf("\n") && (le = (ar = ar.split("\n")).length)) {
            for(i = 0; i < le; i++) {
              if(i && (p = (v = ar[i]).lastIndexOf("@/")) !== -1 && v.indexOf("/inspect/js/inspect") === -1) {
                fl = " " + v.substr(p);
                break;
              }
            }
          }
          self.console("[Inspect" + fl + "]\n" + self.variableGet(u, options));
        };
        /**
         * Inspect variable and get output as string.
         * @function
         * @name Inspect.variableGet
         * @param {mixed} u
         * @param {obj|int|str} [options]
         * @return {str}
         */
        this.variableGet = function(u, options) {
          var o = options, ms = "", ps = false, m, v;
          if(o) {
            switch(typeof o) {
              case "string":
                ms = o;
                break;
              case "object":
                if(o.hasOwnProperty("depth")) {
                  m = o.depth;
                }
                if((v = o.message) && o.hasOwnProperty("message")) {
                  ms = v;
                }
                if((v = o.protos) && o.hasOwnProperty("protos")) {
                  ps = v;
                }
                break;
              case "number":
                if(isFinite(o)) {
                  m = o;
                }
                break;
            }
          }
          else if(o === 0) {
            m = 0;
          }
          return (!ms ? "" : (ms+":\n")) + _nspct(u, ps, m);
        };
        /**
         * Trace error and send output to console log.
         * @example try {
         *   throw new Error("¿Que pasa?");
         * }
         * catch(er) {
         *   Inspect.trace(er, "Class.method()");
         * }
         * @function
         * @name Inspect.trace
         * @param {Error} er
         * @param {str} [ms] - optional message to prepend
         * @return {void}
         */
        this.trace = function(er, ms) {
          self.console( self.traceGet(er, ms) );
        };
        /**
         * Trace error and get output as string.
         * @function
         * @name Inspect.traceGet
         * @param {Error} er
         * @param {str} [ms]
         *  - optional message to prepend
         * @return {str}
         */
        this.traceGet = function(er, ms) {
          var u, le, i, es = ""+er, s = (!ms ? "" : (ms + "()\n")) + es;
          if((u = er.stack)) { // gecko, chromium
            if((le = (u = (u.replace(/\r/, "").split(/\n/))).length)) {
              i = u[0] === es ? 1 : 0; // chromium first line is error toString()
              for(i; i < le; i++) {
                s += "\n  " + u[i].replace(/^[\ ]+at\ /, "@"). // chromium as gecko
                    replace(_dmnRgx, "");
              }
            }
            return s;
          }
          return er; // ie
        };
        /**
         * Analyze function arguments, or an options object.
         * @function
         * @name Inspect.argsGet
         * @param {obj|collection|arr} args
         * @param {arr} [names]
         *  - optional array of argument names
         * @return {str}
         */
        this.argsGet = function(args, names) {
          var a = args, t = self.typeOf(a), le, keys, nKs, s, i;
          if(!a) {
            return "falsy arg args";
          }
          if(t === "array") {
            if(!(le = a.length)) {
              return "0: none";
            }
            nKs = !(keys = names) ? 0 : keys.length; // use arg names as keys
          }
          else if(!(le = ((keys = _oKeys(a)).length))) { // arguments or object having no length
            return "0: none";
          }
          else if(keys[0] === "0") { // arguments, arrayish
            t = "array";
            nKs = !(keys = names) ? 0 : keys.length; // use arg names as keys
          }
          else { // object
            nKs = le;
            s = ""+le+":";
          }
          s = ""+le+":";
          for(i = 0; i < le; i++) {
            s += (i ? ", " : " ") + "#" + i + (nKs <= i ? "" : ("("+keys[i]+")")) + " " +
                _nspct(t === "array" ? a[i] : a[ keys[i] ], "", 0).
                replace(/\n/, ""); // get rid of trailing newline
          }
          return s;
        };
      },
      f;
      O = new O();
      f = function(u, options) {
        return O.variable(u, options);
      };
      f.tcepsnI = true;
      f.console = function(ms) {
        return O.console(ms);
      };
      f.typeOf = function(u) {
        return arguments.length ? O.typeOf(u) : "inspect";
      };
      f.variable = function(u, options) {
        return O.variable(u, options);
      };
      f.variableGet = function(u, options) {
        return O.variableGet(u, options);
      };
      f.trace = function(er, ms) {
        return O.trace(er, ms);
      };
      f.traceGet = function(er, ms) {
        return O.traceGet(er, ms);
      };
      f.argsGet = function(args, names) {
        return O.argsGet(args, names);
      };
      /**
       * Toggle display of all inspection outputs on current page.
       * This function is always implemented, even for no-action no-fold.
       * @function
       * @name Inspect.toggleOutput
       * @return {void}
       */
      f.toggleDisplay = function() {
        var u;
        if(shw > 0) {
          $("." + cls).hide();
        }
        else if(fld < 0) {
          $("pre."+ cls).show();
        }
        else {
          $("pre."+ cls).each(function(){
            $((u = $(this).prev("div."+ cls).get(0)) ? u : this).show();
          });
        }
        shw *= -1;
      };
      /**
       * Toggle inspection folding, if folding is enabled.
       * Folding is not supported for IE<9.
       * @function
       * @name Inspect.toggleFolding
       * @return {void}
       */
      f.toggleFolding = function() {
      };
      return f;
    })();
    if(window.inspect === undefined) {
      window.inspect = window.Inspect;
    }
  })(jQuery);
}