<?php
/**
 * @file
 * Drupal Inspect module profiler class
 */

/**
 * Execution time profiler.
 *
 * @static
 * @singleton
 * @category Drupal
 * @package Developer
 * @subpackage Inspect */
class InspectProfile extends InspectBs {
  /**
   * Instance to enable filing stats at object destruction.
   *
   * Boolean FALSE, if user not allowed to profile.
   *
   * @type InspectProfile|FALSE|NULL
   */
  protected static $_o;

  /**
   * @return InspectProfile
   */
  protected function  __construct() {}

  /**
   * Files profile statistics.
   *
   * Files to directory private://module/inspect/profiles.
   *
   * To limit the number of files, and still avoid concurrent writing, appends to file named [date]_user_[user id];
   * plus, for anonymous user only, _[inspect session number].
   *
   * @return void
   */
  public function __destruct() {
    if (self::$_events) {
      file_put_contents(self::$_profileDir . self::$_profileName, self::_stat(TRUE), FILE_APPEND);
    }
  }

  /**
   * Copy of InspectBs::$_permissions, or FALSE.
   *
   * @var array|bool $_permissions
   */
  protected static $_permissions;

  /**
   * @var integer|string $_userId
   */
  protected static $_userId;

  /**
   * @var string $_profileDir
   */
  protected static $_profileDir;

  /**
   * Filename.
   *
   * @var string $_profileName
   */
  protected static $_profileName;

  /**
   * @var array $_events
   */
  protected static $_events = array();

  /**
   * @var array $_started
   */
  protected static $_started = array();

  /**
   * @var array $_intervals
   */
  protected static $_intervals = array();

  /**
   * @var float $_ownTime_event
   */
  protected static $_ownTime_event = 0;

  /**
   * @var float $_ownTime_total
   */
  protected static $_ownTime_total = 0;

  /**
   * @var integer $_names;
   */
  protected static $_names = array();

  /**
   * Does not call parent InspectBs::_init(), because the basic methods of this class doesnt need that.
   * However ::log() and ::get() call that parent initializer.
   *
   * @return bool
   *  - FALSE if no permission
   */
  protected static function _init_profile() {
    if (self::$_o) {
      return TRUE;
    }
    if (self::$_o === FALSE) {
      return FALSE;
    }
    if (!array_key_exists('user', $GLOBALS) || !is_object($GLOBALS['user']) || !property_exists($GLOBALS['user'], 'uid')) {
      return FALSE; // Dont set self::$_o to FALSE, later attempt may succeed in this respect.
    }
    $le = 1;
    if (!user_access('inspect profile')
        || !($le = strlen($dir = variable_get('file_private_path', FALSE)))) {
      if (!$le) {
        InspectBs::log_db(
            'Private files directory not defined, check /admin/config/media/file-system', 'inspect_profile', WATCHDOG_WARNING
        );
      }
      return (self::$_o = FALSE);
    }
    //  Establish filing directory (1).
    if ($dir{$le - 1} == '/') { // Remove trailing slash.
      $dir = substr($dir, 0, $le - 1);
      $le -= 1;
    }
    if($le < 2) { // Private files dir cannot be shorter than 2 chars (/x).
      return (self::$_o = FALSE);
    }
    if (is_dir($profileDir = $dir . '/module/inspect/profiles') || mkdir($profileDir, 0755, TRUE)) {
      //  Call parent InspectBs methods.
      self::requestTimeMilli();
      self::_initSessionNo();
      self::outputMax(TRUE);
      //  Continue own initialization.
      self::$_profileDir = $profileDir;
      self::$_userId = $uid = $GLOBALS['user']->uid;
      self::$_profileName = '/profile_'
          . date('Ymd', (int) round(self::$_requestTimeMilli / 1000))
          . '_user_' . $uid
          . ($uid ? '' : ('_' . self::$_sessionNo)) // For anonymous user: avoid filing concurrency by using the session number.
          . '.log';
      self::$_o = new InspectProfile();
      //  Measure own time, per event.
      self::event('a', '', '', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('a', '', 'a', '');
      self::event('', '', 'a', '');
      $a = self::$_intervals['a'];
      sort($a);
      array_shift($a);
      array_shift($a);
      array_pop($a);
      array_pop($a);
      self::$_ownTime_event = array_sum($a) / 24; // Should really be 16, but then own time seem to be to long
      self::$_events = self::$_started = self::$_intervals = array();
      return TRUE;
    }
    return (self::$_o = FALSE);
  }

  /**
   * Obsolete method since 7.x-2.3, logs trace as error to database log (if permitted) - and the method will be removed some day.
   *
   * @deprecated
   * @return bool
   *  - FALSE
   */
  public static function init() {
    //  @todo: remove this method from some version > 7.x-2.3, obsolete since that version. Remove when nobody uses 2.1 or 2.2.
    InspectTrc::log(
        NULL,
        array('severity' => WATCHDOG_ERROR, 'message' =>
          'InspectProfile::init() is obsolete since Inspect v. 7.x-2.3, and the method will be removed some day'
        )
    );
    return FALSE;
  }

  /**
   * Register event interval start and/or stop.
   *
   * If never followed by a stop, the event will simply registered as a point in time, having no related interval.
   *
   * Profile statistics will be written to file at shutdown, in the directory private://module/inspect/profiles.
   *
   * Example, profiling in a loop:
   * @code
   * $le = 10;
   * InspectProfile::event('loop'); // start first interval
   * for ($i = 0; $i < $le; $i++) {
   *   $pow = pow($i, $i);
   *   InspectProfile::event(
   *     $i < $le - 1 ? 'loop' : '', '',            // start new interval, except for last iteration
   *     'loop', $i . ' power ' . $i . ': ' . $pow  // do always stop current interval
   *   );
   * }
   * @endcode
   * @param string $startName
   *  - default: empty
   *  - do not use these chars in names: quote carriage-return newline tab pipe
   *  - non-empty: register event, start new interval
   *  - if interval by that name currently is already started: stops the current interval (and then starts new)
   * @param string $startLog
   *  - default: empty (non-empty gets truncated to 255)
   * @param string $stopName
   *  - default: empty
   *  - non-empty: stop interval for that event
   * @param string $stopLog
   *  - default: empty (non-empty gets truncated to 255)
   * @return bool|NULL
   *   - NULL if no permission
   *   - FALSE on error (both $startName and $stopName are empty, or either of them is TRUE)
   */
  public static function event($startName = '', $startLog = '', $stopName = '', $stopLog = '') {
    $t = microtime(TRUE) * 1000;
    //  check init
    $init = FALSE;
    if (!self::$_o && (self::$_o === FALSE || !($init = self::_init_profile()))) {
      return NULL;
    }
    if ($init) {
      $t = microtime(TRUE) * 1000; // Reset to compensate for initialization.
      if (!self::$_o) { // Waste some time.
        return NULL;
      }
    }
    $rtrn = FALSE;

    //  @todo: remove this check in some version > 7.x-2.2, 7.x-2.1 had other parameters.
    //  Remove when nobody uses 2.1.
    //  Remove also ', or either of them is TRUE' from return value documentation; and the same goes for inspect_profile().
    if ($stopName === TRUE || $startLog === TRUE) {
      InspectTrc::log(
          NULL,
          array('severity' => WATCHDOG_ERROR, 'message' =>
            'Wrong arguments for inspect_profile()/InspectProfile::event(), parameters changed since 7.x-2.2, check documentation'
          )
      );
      return FALSE;
    }

    //  If start and stop in one call, we may assume that at least one interval was handled by a single call.
    self::$_ownTime_total += ($tP = self::$_ownTime_event);

    $ss =& self::$_started;
    if ($stopName && $stopName != $startName) {
      $rtrn = TRUE;
      if (isset($ss[$stopName])) {
        //$tIn = $t - $ss[$stopName] - $tP;
        if (($tIn = $t - $ss[$stopName]) > $tP) { // More than own time, subtract that.
          $tIn -= $tP;
        }
        elseif ($tIn > $tP / 2) { // More than half of own time, subtract that.
          $tIn -= $tP / 2;
        }
        //  Register interval, for total and average calculation.
        if(isset(self::$_intervals[$stopName])) {
          self::$_intervals[$stopName][] = $tIn;
        }
        else {
          self::$_intervals[$stopName] = array(
              $tIn
          );
        }
        unset($ss[$stopName]);
      }
      else {
        self::$_names[$stopName] = strlen($stopName);
        $tIn = -1; //  ~ error, hadnt been started previously
      }
      self::$_events[] = array(
          $stopName,
          $t, // time
          $tIn, // interval
          strlen($stopLog) < 256 ? $stopLog : substr($stopLog, 255)
      );
    }
    if ($startName) {
      $rtrn = TRUE;
      //  if currently already started: stop current interval
      if (isset($ss[$startName])) {
        //$tIn = $t - $ss[$startName] - $tP;
        if (($tIn = $t - $ss[$startName]) > $tP) { // More than own time, subtract that.
          $tIn -= $tP;
        }
        elseif ($tIn > $tP / 2) { // More than half of own time, subtract that.
          $tIn -= $tP / 2;
        }
        //  Register interval, for total and average computation.
        if(isset(self::$_intervals[$startName])) {
          self::$_intervals[$startName][] = $tIn;
        }
        else {
          self::$_intervals[$startName] = array(
              $tIn
          );
        }
        self::$_events[] = array(
            $startName,
            $t, // time
            $tIn, // interval
            strlen($stopLog) < 256 ? $stopLog : substr($stopLog, 255)
        );
      }
      else {
        self::$_names[$startName] = strlen($startName);
      }
      $ss[$startName] = $t;
      self::$_events[] = array(
          $startName,
          $t, // time
          -2, // interval, -1 ~ none (is a start)
          strlen($startLog) < 256 ? $startLog : substr($startLog, 255)
      );
    }
    return $rtrn;
  }



  /**
   * Log profile statistics to database (watchdog), if permitted.
   *
   * @param string $message
   *  - default: empty
   * @return bool|NULL
   *   - NULL if never initialized or user isnt permitted to log inspections
   *   - FALSE on error
   */
  public static function log($message = '') {
    if (!self::$_o || (!$permissions = self::_init()) || !$permissions['inspect log']) {
      return NULL;
    }
    $opts =& self::$_options;
    $tagStart = $tagEnd = '';
    if (($u = $opts['enclose_tag_log'])) {
      $tagStart = '<' . $u . (empty($opts['no_folding']) ? ' class="module-inspect-collapsible inspect-profile"' : '') . '>';
      $tagEnd = '</' . $u . '>';
    }
    $nl = $opts['newline_log'];
    return self::log_db(
        $tagStart
          . ( empty($message) ? '' : ($message . ':' . $nl) )
          . '[Inspect profile - ' . self::$_sessionNo . ':' . self::$_requestNo . ']' . $nl
          . self::_stat()
          . $tagEnd,
        'inspect profile',
        WATCHDOG_INFO
    );
  }

  /**
   * Get profile statistics as string, if permitted.
   *
   * @param string $message
   *  - default: empty
   * @return string
   *   - empty if never initialized or user isnt permitted to get inspections, or on error
   */
  public static function get($message = '') {
    if (!self::$_o || (!$permissions = self::_init()) || !$permissions['inspect get']) {
      return '';
    }
    $opts =& self::$_options;
    $tagStart = $tagEnd = '';
    if (($u = $opts['enclose_tag_get'])) {
      $tagStart = '<' . $u . (empty($opts['no_folding']) ? ' class="module-inspect-collapsible inspect-profile"' : '') . '>';
      $tagEnd = '</' . $u . '>';
    }
    $nl = $opts['newline_get'];
    return $tagStart
        . ( empty($message) ? '' : ($message . ':' . $nl) )
        . '[Inspect profile]' . $nl
        . self::_stat()
        . $tagEnd;
  }

  /**
   * @param bool $file
   * @return string
   */
  protected static function _stat($file = FALSE) {
    $reqEnd = microtime(TRUE) * 1000;
    $delim = ' | ';
    $nl = $file ? "\n" : self::$_options['delimiters'][1];

    $events =& self::$_events;
    $nEvents = count($events);

    $reqTotal = $reqEnd - ($reqStart = self::$_requestTimeMilli) - self::$_ownTime_total;

    $s = (!$file ? '' :
          ($nl . self::$_sessionNo . ':' . self::$_requestNo
          . ' - ' . date('Y-m-d H:i:s', floor($reqStart / 1000)) . ' - user: ' . self::$_userId
          . ' ------------------------------------------------'
          . $nl . $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI']
          . $nl . $nl)
        )
        . 'Unit: milliseconds'
        . $nl . 'Total request time' . ($file ? '' : ' (so far)') . ': ' . number_format($reqTotal, 4, '.', '')
        . $nl . 'Subtracted estimated own time, total: ' . number_format(self::$_ownTime_total, 4, '.', '')
        . ', per event: ' . number_format(self::$_ownTime_event, 4, '.', '')
        . $nl . 'Do not use these chars in names: " \r \n \t |';

    //  Calculate totals and means.
    $intervals =& self::$_intervals;
    $totals = array();
    $stats = array();
    foreach ($intervals as $nm => $a) {
      $mTrnc = $mean = ($totals[$nm] = array_sum($a)) / ($le = count($a));
      if ($le > 1) {
        sort($a);
        $sml = $a[0];
        $big = $a[$le - 1];
      }
      else {
        $sml = $big = -1;
      }
      if ($le > 10) {
        $trnc = floor($le / 10);
        $mTrnc = array_sum(array_slice($a, $trnc, $le - ($trnc * 2))) / ($le - ($trnc * 2));
      }
      else if ($le > 4) {
        array_shift($a);
        array_pop($a);
        $mTrnc = array_sum($a) / ($le - 2);
      }
      $stats[$nm] = array(
          $le,
          $mean,
          $mTrnc,
          $big,
          $sml
      );
    }

    //  Find longest name, for making padded names.
    $padNames = self::$_names; // copy
    $lNms = 4; // ~ Name
    foreach ($padNames as $le) {
      if ($le > $lNms) {
        $lNms = $le;
      }
    }
    if (!$file && $lNms > 32) {
      $lNms = 32;
    }
    foreach ($padNames as $nm => &$v) {
      if ($v < $lNms) {
        $v = str_pad($nm, $lNms, ' ');
      }
      else if (!$file && $v > 32) {
        $v = substr($nm, 0, 32);
      }
      else {
        $v = $nm;
      }
    }
    unset($v);

    //  List summations.
    $s .= $nl . ' ' . $nl . 'Intervals cumulative' . $nl
        . join(
            $delim,
            array(
                str_pad('name', $lNms, ' '),
                'total      ',
                'total % ',
                'count',
                'mean %  ',
                'mean       ',
                'trunc. mean',
                'longest    ',
                'shortest   '
            )
        );
    arsort($totals);
    foreach ($totals as $nm => $n) {
      $s .= $nl . $padNames[$nm]
          . $delim . str_pad(number_format($n, 4, '.', ''), 11, ' ', STR_PAD_LEFT) // total
          . $delim . str_pad(number_format(($n * 100) / $reqTotal, 4, '.', ''), 8, ' ', STR_PAD_LEFT) // total percent
          . $delim . str_pad($stats[$nm][0], 5, ' ', STR_PAD_LEFT) // count
          . $delim . str_pad(number_format((($v = $stats[$nm][1]) * 100) / $reqTotal, 4, '.', ''), 8, ' ', STR_PAD_LEFT) // mean %
          . $delim . str_pad(number_format($v, 4, '.', ''), 11, ' ', STR_PAD_LEFT) // mean
          . $delim . str_pad(number_format($v = $stats[$nm][2], 4, '.', ''), 11, ' ', STR_PAD_LEFT) // truncated mean
          . $delim . ($stats[$nm][3] < 0 ? '           ' :
              str_pad(number_format($stats[$nm][3], 4, '.', ''), 11, ' ', STR_PAD_LEFT)) // longest
          . $delim . ($stats[$nm][4] < 0 ? '           ' :
              str_pad(number_format($stats[$nm][4], 4, '.', ''), 11, ' ', STR_PAD_LEFT)) // shortest
          ;
    }

    //  List all events.
    $s .= $nl . ' ' . $nl . 'All events: ' . $nEvents . $nl;
    //  Prevent too large output.
    $maxLog = self::$_outputMax_log;
    $maxGet = self::$_outputMax_get;
    if ($nEvents > ($maxEvents = floor(($file ? ($maxLog < $maxGet ? $maxGet : $maxLog) : ($maxLog < $maxGet ? $maxLog : $maxGet)) / 400))) {
      return $s . '- number of events exceeds ' . $maxEvents . ', will not list events' . ($file ? $nl : '');
    }
    $s .= join(
            $delim,
            array('event', str_pad('name', $lNms, ' '),
                  'since req. ', 'since last ', 'interval   ', 'versus mean', '%       ', 'log')
        );
    $tLast = 0;
    for ($i = 0; $i < $nEvents; $i++) {
      $a = $events[$i]; // name | time | interval | log
      $nm = $a[0];
      $s .= $nl . str_pad($i + 1, 5, ' ', STR_PAD_LEFT) // event no
          . $delim . $padNames[$nm]
          . $delim . str_pad(number_format(($t = $a[1]) - $reqStart, 4, '.', ''), 11, ' ', STR_PAD_LEFT) // since request
          . $delim . ($i && $t > $tLast ? str_pad(number_format($t - $tLast, 4, '.', ''), 11, ' ', STR_PAD_LEFT) : '           ')
          . $delim . (($tIn = $a[2]) < 0 ? ($tIn < -1 ? '           ' : '    -1     ') : // stop that hadnt been started
              str_pad(number_format($tIn, 4, '.', ''), 11, ' ', STR_PAD_LEFT)) // interval
          . $delim . ($tIn < 0 || $stats[$nm][0] == 1 ? '           ' :
              str_pad(number_format($tIn - $stats[$nm][1], 4, '.', ''), 11, ' ', STR_PAD_LEFT)) // versus mean
          . $delim . ($tIn < 0 ? '        ' : str_pad(number_format(($tIn * 100) / $reqTotal, 4, '.', ''), 8, ' ', STR_PAD_LEFT))
          . $delim . (!strlen($a[3]) ? '' :
              str_replace(array('"', "\r", "\n", "\t", '|'), array("'", '', ' ', ' ', '/'), $a[3]) // log
          );
      $tLast = $t;
    }
    $s .= $nl . '%: Percent of total request time' . ($file ? $nl : ' &#183; ') . 'Interval -1: Stop preceded by no start'
        . $nl . 'Truncated mean ignores 5 through 25 percent of the outliers at each end (5-10% for populations >= 10)'
        . ($file ? '' : (
            $nl . 'Names and logs are shortened to 32 (except in filed profile)'
            . $nl . 'File: private://module/inspect/profiles' . self::$_profileName
        ) );
    return $s . ($file ? ($nl . $nl) : '');
  }
}