<?php
/**
 * @file
 * Drupal Inspect module stack trace inspection class
 */

/**
 * Stack trace inspector
 * @static
 * @singleton
 * @category Drupal
 * @package Developer
 * @subpackage Inspect */
final class InspectTrc extends InspectBs {

  /**
   * Default sub var recursion depth.
   *
   * Overrides InspectBs::DEPTH_DEFAULT - when methods of this class uses methods of InspectBs (using static::DEPTH_DEFAULT).
   *
   * @type integer
   */
  const DEPTH_DEFAULT = 0;

  /**
   * Minimum string truncation.
   *
   * Overrides InspectBs::TRUNCATE_MIN - when methods of this class uses methods of InspectBs (using static::TRUNCATE_MIN).
   *
   * @type integer
   */
  const TRUNCATE_MIN = 1000;

  /**
   * Overrides InspectBs::TRUNCATE_DEFAULT - when methods of this class uses methods of InspectBs (using static::TRUNCATE_DEFAULT).
   *
   * @type integer
   */
  const TRUNCATE_DEFAULT = 100;

  /**
   * Default maximum stack frame depth
   *
   * @type integer
   */
  const TRACE_LIMIT_DEFAULT = 10;

  /**
   * Absolute maximum stack frame depth
   *
   * @type integer
   */
  const TRACE_LIMIT_MAX = 100;

  /**
   * Default trace options.
   *
   * Adds to/overrides options of InspectBs::$_options
   *
   *   Options:
   *   - (string) category: inspect trace
   *   - (string) trace_spacer: spacer between frames
   *   - (string) pre_indent: indentation of object/array buckets
   *   - (array) delimiters: see InspectBs
   *
   * @var array $_optionsTrace
   */
  protected static $_optionsTrace = array(
    'category' => 'inspect trace',
    'trace_spacer' => '- - - - - - - - - - - - - - - - - - - - - - - - -',
    'pre_indent' => '  ',
    'delimiters' => array(
        "\n", // Before first bucket.
        "\n", // Before later bucket.
        "\n  "  // Before last bucket.
    ),
  );

  /**
   * Whether getting is desired and allowed
   * @var bool
   */
  protected $_get = FALSE;

  /**
   * Whether logging is desired and allowed
   * @var bool
   */
  protected $_log = FALSE;


  protected $_exceptionClass; // string
  protected $_trace; // array
  protected $_limit = 0;
  protected $_options_get; // array
  protected $_options_log; // array

  protected $_traceLog = '';
  protected $_traceGet = '';

  protected $_enclose_tag_get; // string
  protected $_enclose_tag_log; // string



  /**
   * Checks it's arguments against permissions.
   *
   * Admin user 1 is never allowed to _get_ traces (see InspectBs::_checkPermissionsAndOptions()).
   * @param Exception|falsy $xc
   *   - if falsy: create new trace
   * @param mixed $options
   *   - array|object: list of options
   *   - string: interprets to message
   *   - integer: interprets to limit (maximum stack frame depth)
   *   - other type: ignored
   * @param bool $get
   * @param bool $log
   * @return InspectTrc
   */
  protected function  __construct($xc, $options, $get = FALSE, $log = FALSE) {
    //  Get limit value if not array, and secure array having trace options.
    $limitOpt = $limitDef = static::TRACE_LIMIT_DEFAULT;
    $limitMax = static::TRACE_LIMIT_MAX;
    $optsDef =& static::$_optionsTrace;
    if (($t = gettype($options)) == 'object') {
      $opts = get_object_vars($options);
      $t = 'array';
    }
    else {
      $opts = $options;
    }
    switch ($t) {
      case 'array':
        if (isset($opts['limit'])) {
          $limitOpt = (int)$opts['limit'];
          unset($opts['limit']);
        }
        $opts = array_merge($optsDef, $opts); // secure $_optionsTrace buckets
        break;
      case 'integer':
        $limitOpt = $opts;
        $opts = $optsDef;
        break;
      case 'string':
        $opts = $optsDef;
        $opts['message'] = $options;
        break;
      default:
        $opts = $optsDef;
    }
    if ($limitOpt < 1 || $limitOpt > $limitMax) {
      $limitOpt = $limitMax;
    }
    $this->_limit = $limitOpt;
    //  Check permissions, and secure population of options array(s).
    if ($get && ($optsGet = static::_checkPermissionsAndOptions('get', $opts) ) ) {
      $this->_get = TRUE;
      $this->_enclose_tag_get = $optsGet['enclose_tag_get']; // store enclose tag
      $optsGet['enclose_tag_get'] = ''; // clear to avoid enclosure of every single var inspection
      $this->_options_get =& $optsGet;
    }
    if ($log && ($optsLog = static::_checkPermissionsAndOptions('log', $opts) ) ) {
      $this->_log = TRUE;
      $this->_enclose_tag_log = $optsLog['enclose_tag_log']; // store enclose tag
      $optsLog['enclose_tag_log'] = ''; // clear to avoid enclosure of every single var inspection
      $this->_options_log =& $optsLog;
    }

    if ($this->_get || $this->_log) {
      //  Create trace, if none given by arg.
      if ($xc) {
        if (is_object($xc) && (($u = get_class($xc)) === 'Exception' || is_subclass_of($xc, 'Exception'))) {
          $this->_exceptionClass = $u;
          $trace = $xc->getTrace();
          if (count($trace) > $limitOpt) {
            array_splice($trace, $limitOpt);
          }
          $this->_trace =& $trace;
        }
      }
      else {
        try {
          $trace = debug_backtrace();
        }
        catch (Exception $xc) {
          return;
        }
        if ($trace) {
          //  Remove top levels on synthetic trace, first of all the InspectTrc::__construct frame.
          array_shift($trace);
          //  If called via function instead of class method: remove class method call.
          if (isset($trace[0]['class']) && strtolower($trace[0]['class']) === 'inspecttrc'
              && isset($trace[1]['function']) && substr($trace[1]['function'], 0, 13) === 'inspect_trace') {
            array_shift($trace);
          }
          //  Enforce the trace limit.
          if (count($trace) > $limitOpt) {
            array_splice($trace, $limitOpt + 1); // plus one because we need the bucket holding the initial event
          }
          $this->_trace =& $trace;
        }
      }
      //  Format trace to get and/or log string.
      if ($this->_trace) {
        $this->_formatAsString($xc);
      }
    }
  }


  /**
   * Inspect and log stack trace to database (watchdog), if permitted.
   *
   *   Integer value of $options (limit):
   *   - maximum stack frame depth
   *   - default: 10 (InspectTrc::TRACE_LIMIT_DEFAULT)
   *   - max: 100 (InspectTrc::TRACE_LIMIT_MAX)
   *
   *   Array/object value of $options are like inspect()'s options, except:
   *   - (integer) limit (default 10, max 100 ~ InspectTrc::TRACE_LIMIT_DEFAULT|TRACE_LIMIT_MAX)
   *   - (string) trace_spacer: spacer between frames (default hyphen dotted line, length 49)
   *   - (string) category: inspect trace
   *   - (boolean) file_line: is not used by tracer
   *
   * Default variable analysis depth (0 ~ InspectTrc::DEPTH_DEFAULT) is less than for plain variable inspection.
   *
   * String truncation minimum (1000 ~ InspectTrc::TRUNCATE_MIN) is less than for plain variable inspection.
   *
   * See inspect() for specifics of variable analysis.
   *
   * Default watchdog severity: WATCHDOG_DEBUG.
   *
   * Executes variable analysis within try-catch.
   *
   * @see InspectVar::log()
   * @param Exception|falsy $exception
   *   - default: NULL (~ create new backtrace)
   *   - if Exception, trace that
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - integer: interprets to limit (maximum stack frame depth)
   *   - string: interprets to message
   *   - other type: ignored
   * @return bool|NULL
   *   - NULL if user isnt permitted to log inspections
   *   - FALSE on error (other error than exceeding max. output length)
   */
  public static function log($exception = NULL, $options = NULL) {
    $o = new InspectTrc($exception, $options, FALSE, TRUE);
    //  If not allowed to log at all: get out.
    if (!$o->_log) {
      return NULL;
    }
    //  If tracing failed: get out.
    if (!$o->_traceLog) {
      return FALSE;
    }
    $tagStart = $tagEnd = '';
    if (($u = $o->_enclose_tag_log)) {
      $tagStart = '<' . $u . '>';
      $tagEnd = '</' . $u . '>';
    }
    return self::log_db(
        $tagStart
          . ( empty($o->_options_log['message']) ? '' : ($o->_options_log['message'] . ':' . $o->_options_log['newline_log']) )
          . '[Inspect trace - ' . self::$_sessionNo . ':' . self::$_requestNo . ':' . (++self::$_logNo)
              . ' - limit:' . $o->_limit . '|depth:' . $o->_options_log['depth'] . ']|truncate:' . $o->_options_log['truncate'] . ']'
              . $o->_options_log['newline_log']
          . $o->_traceLog
          . $tagEnd,
        $o->_options_log['category'],
        array_key_exists('severity', $o->_options_log) ? $o->_options_log['severity'] : -1
    );
  }

  /**
   * Inspect stack trace and get the output as string, if permitted.
   *
   *   Behaviour and arguments like InspectTrc::log(), except for $options:
   *   - enclose_tag_get instead of enclose_tag_log
   *   - newline_get instead of newline_log
   *
   * @see InspectTrc::log()
   * @param Exception|falsy $exception
   *   - default: NULL (~ create new backtrace)
   *   - if Exception, trace that
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - integer: interprets to limit (maximum stack frame depth)
   *   - string: interprets to message
   *   - other type: ignored
   * @return string
   *   - empty if user isnt permitted to get inspections, or tracing failed (other error than exceeding max. output length)
   */
  public static function get($exception = NULL, $options = NULL) {
    $o = new InspectTrc($exception, $options, TRUE, FALSE);
    //  If not allowed to get at all, or tracing failed: get out.
    if (!$o->_get || !$o->_traceGet) {
      return '';
    }
    $tagStart = $tagEnd = '';
    if (($u = $o->_enclose_tag_get)) {
      $tagStart = '<' . $u . '>';
      $tagEnd = '</' . $u . '>';
    }
    return $tagStart
        . ( empty($o->_options_get['message']) ? '' : ($o->_options_get['message'] . ':' . $o->_options_get['newline_get']) )
        . '[Inspect trace - limit:' . $o->_limit . '|depth:' . $o->_options_get['depth'] . ']|truncate:' . $o->_options_get['truncate'] . ']'
            . $o->_options_get['newline_get']
        . $o->_traceGet
        . $tagEnd;
  }

  /**
   * Obsolete function since 7.x-2.5, logs trace as error to database log (if permitted) - and the function will be removed some day.
   *
   * @deprecated
   * @param Exception|falsy $exception
   *   - default: NULL (~ create new backtrace)
   *   - if Exception, trace that
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - string: interprets to message
   *   - integer: interprets to limit (maximum stack frame depth)
   *   - other type: ignored
   * @return string
   *   - empty
   */
  public static function log_get($exception = NULL, $options = NULL) {
    //  @todo: remove this method from some version > 7.x-2.5, obsolete since that version. Remove when few use <2.5.
    InspectTrc::log(
        NULL,
        array('severity' => WATCHDOG_ERROR, 'message' =>
          'InspectTrc::log_get() is obsolete since Inspect v. 7.x-2.5, and the method will be removed some day'
        )
    );
    $o = new InspectTrc($exception, $options, FALSE, TRUE);
    //  If not allowed to log at all: get out.
    if (!$o->_log) {
      return '';
    }
    //  If logging trace allowed, and tracing succeeded.
    if ($o->_log && $o->_traceLog) {
      $tagStart = $tagEnd = '';
      if (($u = $o->_enclose_tag_log)) {
        $tagStart = '<' . $u . '>';
        $tagEnd = '</' . $u . '>';
      }
      self::log_db(
          $tagStart
            . ( empty($o->_options_log['message']) ? '' : ($o->_options_log['message'] . ':' . $o->_options_log['newline_log']) )
            . '[Inspect trace - ' . self::$_sessionNo . ':' . self::$_requestNo . ':' . (++self::$_logNo)
                . ' - limit:' . $o->_limit . '|depth:' . $o->_options_log['depth'] . ']' . $o->_options_log['newline_log']
            . $o->_traceLog
            . $tagEnd,
          $o->_options_log['category'],
          array_key_exists('severity', $o->_options_log) ? $o->_options_log['severity'] : -1
      );
    }
    return '';
  }

  /**
   * Analyse and format the trace frames' buckets.
   * @param Exception|falsy $xc
   *   - default: NULL
   * @return void
   */
  protected function _formatAsString($xc = NULL) {
    try {
      self::$_outputLength = 0;
      //  Resolve options for get and/or log.
      if (($get = $this->_get)) {
        self::$_outputMax = self::$_outputMax_get;
        $sGet = '';
        $options_get =& $this->_options_get;
        $delim = $options_get['delimiters'][1];
        $preIndent = $options_get['pre_indent'];
        $frameSpacer = $options_get['trace_spacer'];
      }
      if (($log = $this->_log)) {
        self::$_outputMax = self::$_outputMax_log;
        $sLog = '';
        $options_log =& $this->_options_log;
        if (!$get) {
          $delim = $options_log['delimiters'][1];
          $preIndent = $options_log['pre_indent'];
          $frameSpacer = $options_log['trace_spacer'];
        }
        $hidePaths_log = $this->_options_log['hide_paths'];
      }
      //  Find the smaller of output maxes.
      if ($log && $get) {
        self::$_outputMax = self::$_outputMax_log < self::$_outputMax_get ? self::$_outputMax_log : self::$_outputMax_get;
      }
      //  If exception: resolve its origin.
      $file = $file_noRoot = $file_basename = 'unknown';
      if ($this->_exceptionClass) {
        $sLog = $sGet = 'Exception (' . $this->_exceptionClass . ') - code: ' . intval($xc->getCode());
        $file = trim($xc->getFile());
        $line = (($u = $xc->getLine()) ? trim($u) : '?');
        $message = 'message: ' . $xc->getMessage() . $delim;
      }
      else {
        $frame = array_shift($this->_trace);
        if (isset($frame['class'])) {
          $sLog = $sGet = ($frame['class'] . '::' . (isset($frame['function']) ? ($frame['function'] . '()') : '') );
        }
        else if(isset($frame['function'])) {
          $sLog = $sGet = ($frame['function'] . '()');
        }
        if (isset($frame['file'])) {
          $file = trim($frame['file']);
        }
        $line = (isset($frame['line']) ? trim($frame['line']) : '?');
        $message = '';
      }
      if (($file_length = strlen($file))) {
        if ( $get || ($log && $hidePaths_log) ) {
          $file_basename = basename( $file );
        }
        if ( $log && !$hidePaths_log ) {
          if (strlen($file_noRoot = preg_replace(self::$_paths, '', $file)) < $file_length) {
            $file_noRoot = 'document_root/' . $file_noRoot;
          }
        }
      }
      if ($get) {
        $sGet .= $delim . 'file: ' . $file_basename . $delim
            . 'line: ' . $line . $delim
            . $message;
      }
      if ($log) {
        $sLog .= $delim . 'file: ' . ($hidePaths_log ? $file_basename : $file_noRoot) . $delim
            . 'line: ' . $line . $delim
            . $message;
      }
      //  Iterate stack frames.
      $nFrame = -1;
      foreach ($this->_trace as &$frame) {
        $s = (++$nFrame) . ' ' . $frameSpacer . $delim;
        if ($get) {
          $sGet .= $s;
        }
        if ($log) {
          $sLog .= $s;
        }
        //  File.
        $file = $file_noRoot = $file_basename = 'unknown';
        if (isset($frame['file']) && ($file_length = strlen($file = trim($frame['file'])))) {
          if ( $get || ($log && $hidePaths_log) ) {
            $file_basename = basename( $file );
          }
          if ( $log && !$hidePaths_log ) {
            if (strlen($file_noRoot = preg_replace(self::$_paths, '', $file)) < $file_length) {
              $file_noRoot = 'document_root/' . $file_noRoot;
            }
          }
        }
        if ($get) {
          $sGet .= 'file: ' . $file_basename;
        }
        if ($log) {
          $sLog .= 'file: ' . ($hidePaths_log ? $file_basename : $file_noRoot);
        }
        //  Line.
        $line = 'line: ' . (isset($frame['line']) ? trim($frame['line']) : '?') . $delim;
        if ($get) {
          $sGet .= ' - ' . $line;
        }
        if ($log) {
          $sLog .= ($hidePaths_log ? ' - ' : $delim) . $line;
        }
        //  Class, object, function, type.
        $sFunc = '';
        if (isset($frame['function'])) {
          if (strlen($u = $frame['function']) ) {
            $sFunc = $u;
          }
        }
        $sCls = '';
        if (isset($frame['class'])) {
          if (strlen($u = $frame['class'])) {
            $sCls = $u;
          }
        }
        $sType = '';
        if (isset($frame['type'])) {
          $sType = trim($frame['type']);
        }
        $sObj = '';
        if (isset($frame['object'])) {
          if (($o = $frame['object'])) {
            //  We dont know if class bucket is present when object is, but using class is cheaper.
            $sObj = $sCls ? $sCls : get_class($o);
            if ($sFunc) {
              $sFunc = 'method: (' . $sObj . ')' . ($sType ? $sType : '->') . $sFunc;
            }
            else { // Is this possible at all? Simply and object, no method call??
              $sObj = 'object (' . $sObj . ')';
            }
          }
        }
        elseif ($sFunc) {
          $sFunc = !$sCls ? ('function: ' . $sFunc) : ('static method: ' . $sCls . ($sType ? $sType : '::') . $sFunc);
          //  $frame['object'] doesnt exist.
        }
        /* else {
         *  $frame['object'] doesnt exist
         *  $frame['function'] may exist, containing non-empty string
         * }
         */
        if ($sFunc) {
          if ($get) {
            $sGet .= $sFunc . $delim;
          }
          if ($log) {
            $sLog .= $sFunc . $delim;
          }
        }
        elseif ($sObj) {
          if ($get) {
            $sGet .= $sObj . $delim;
          }
          if ($log) {
            $sLog .= $sObj . $delim;
          }
        }
        //  Args.
        if (isset($frame['args'])) {
          $le = count($frame['args']);
          $sArgsGet = '';
          if ($get) {
            $sArgsGet = '';
            for ($i = 0; $i < $le; $i++) {
              $sArgsGet .= $delim . $preIndent
                  . self::_inspect(
                      $frame['args'][$i],
                      $options_get,
                      0 // recursion counter
                    );
            }
            $sGet .= 'args (' . $le . ')'
                . (!$le ? '' : (': ' . $sArgsGet) )
                . $delim;
          }
          if ($log) {
            $sArgsLog = '';
            for ($i = 0; $i < $le; $i++) {
              $sArgsLog .= $delim . $preIndent
                  . self::_inspect(
                      $frame['args'][$i],
                      $options_log,
                      0 // recursion counter
                    );
            }
            $sLog .= 'args (' . $le . ')'
                . (!$le ? '' : (': ' . $sArgsLog) )
                . $delim;
          }
        }
      }
      if ($get) {
        $sGet .= 'END ' . $frameSpacer;
        if (($outputLength = strlen($this->_traceGet =& $sGet)) > self::$_outputMax) {
          throw new Exception(
              'Inspection aborted: output length ' . $outputLength . ' exceeds maximum ' . self::$_outputMax
                  . ', using options limit[' . $this->_limit . '], depth[' . $this->_options_get['depth']
                  . '] and truncate[' . $this->_options_get['truncate'] . ']'
                  . ', try smaller limit, less depth or more truncation.',
              100
          );
        }
      }
      if ($log) {
        $sLog .= 'END ' . $frameSpacer;
        if (($outputLength = strlen($this->_traceLog =& $sLog)) > self::$_outputMax) {
          throw new Exception(
              'Inspection aborted: output length exceeds maximum ' . self::$_outputMax . ', using options limit[' . $this->_limit
                  . '], options depth[' . $this->_options_log['depth'] . '] and truncate[' . $this->_options_log['truncate'] . ']'
                  . ', try smaller limit, less depth or more truncation.',
              100
          );
        }
      }
    }
    catch (Exception $xc1) {
      $em = $xc1->getMessage();
      if ($this->_get) {
        $this->_traceGet = $em . (!$this->_exceptionClass ? '' : substr(' Original error message: ' . $xc->getMessage(), 0, self::$_outputMax));
      }
      if ($this->_log) {
        $this->_traceLog = $em . (!$this->_exceptionClass ? '' : substr(' Original error message: ' . $xc->getMessage(), 0, self::$_outputMax));
      }
    }
  }
}