<?php
/**
 * @file
 * course_home_page.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function course_home_page_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'course_home_page_sidebar_content';
  $view->description = 'Causes the extra non-body fields that are part of the Course Home Page content type to display in the sidebar';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Course Home Page Sidebar Content';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Course Info';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Course Developer */
  $handler->display->display_options['fields']['field_course_developer']['id'] = 'field_course_developer';
  $handler->display->display_options['fields']['field_course_developer']['table'] = 'field_data_field_course_developer';
  $handler->display->display_options['fields']['field_course_developer']['field'] = 'field_course_developer';
  $handler->display->display_options['fields']['field_course_developer']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_course_developer']['click_sort_column'] = 'nnlm_rosters_fullname';
  /* Field: Content: Internal Course Materials */
  $handler->display->display_options['fields']['field_internal_course_materials']['id'] = 'field_internal_course_materials';
  $handler->display->display_options['fields']['field_internal_course_materials']['table'] = 'field_data_field_internal_course_materials';
  $handler->display->display_options['fields']['field_internal_course_materials']['field'] = 'field_internal_course_materials';
  $handler->display->display_options['fields']['field_internal_course_materials']['label'] = 'Course Materials';
  $handler->display->display_options['fields']['field_internal_course_materials']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_internal_course_materials']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_internal_course_materials']['click_sort_column'] = 'sid';
  $handler->display->display_options['fields']['field_internal_course_materials']['type'] = 'scald_icon';
  $handler->display->display_options['fields']['field_internal_course_materials']['settings'] = array(
    'link' => '0',
  );
  $handler->display->display_options['fields']['field_internal_course_materials']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_internal_course_materials']['multi_type'] = 'ul';
  /* Field: Content: External Course Materials */
  $handler->display->display_options['fields']['field_external_course_materials']['id'] = 'field_external_course_materials';
  $handler->display->display_options['fields']['field_external_course_materials']['table'] = 'field_data_field_external_course_materials';
  $handler->display->display_options['fields']['field_external_course_materials']['field'] = 'field_external_course_materials';
  $handler->display->display_options['fields']['field_external_course_materials']['label'] = '';
  $handler->display->display_options['fields']['field_external_course_materials']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_external_course_materials']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_external_course_materials']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_external_course_materials']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_external_course_materials']['multi_type'] = 'ul';
  /* Field: Content: MLA Clearinghouse Entry */
  $handler->display->display_options['fields']['field_mla_clearinghouse_entry']['id'] = 'field_mla_clearinghouse_entry';
  $handler->display->display_options['fields']['field_mla_clearinghouse_entry']['table'] = 'field_data_field_mla_clearinghouse_entry';
  $handler->display->display_options['fields']['field_mla_clearinghouse_entry']['field'] = 'field_mla_clearinghouse_entry';
  $handler->display->display_options['fields']['field_mla_clearinghouse_entry']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_mla_clearinghouse_entry']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'course_home_page' => 'course_home_page',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<h2>Course Info</h2>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  $handler->display->display_options['block_description'] = 'Course Home Page Course Info';
  $export['course_home_page_sidebar_content'] = $view;

  return $export;
}
