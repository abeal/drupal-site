<?php
/**
 * @file
 * course_home_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function course_home_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function course_home_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function course_home_page_node_info() {
  $items = array(
    'course_home_page' => array(
      'name' => t('Course Home Page'),
      'base' => 'node_content',
      'description' => t('A course home page consists of a generalized description of a course that is taught by NN/LM staff.  In particular, it does not contain things like time, date, location, or any other aspects of a course that are particular to any given instance of the course that is currently being or has been taught.  For more information on Course home pages, see: https://staff.nnlm.gov/wiki/Guidelines_for_Classes'),
      'has_title' => '1',
      'title_label' => t('Course Title'),
      'help' => t('See description'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
