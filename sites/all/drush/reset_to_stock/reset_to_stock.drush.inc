<?php

/**
 * @file
 */

/**
 *
 */
function reset_to_stock_drush_help() {
  switch ($section) {
    case 'drush:reset-to-stock':
      return dt("\n\nUsage: \n>drush reset-to-stock");
  }
}

/**
 *
 */
function reset_to_stock_drush_command() {
  $items = array(
    'reset-to-stock' => array(
      'description' => "Removes all nodes, tags, menu entries, and files,
      other than those listed as 'nnlm_drupal_stock'",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'arguments' => array(),
      'options' => array(),
      'examples' => array(
        'drush @sandy-dev reset-to-stock' => 'Resets the build at the
        drush alias @sandy-dev to be stock.',
      ),
    ),
  );
  return $items;
}

/**
 *
 */
function _extract_file_names($body) {
  $matches = array();
  preg_match_all("#[\"'][^\"']+sites/default/files([^/?\"']*/)+([^/\"'?]+)#", $body, $matches);
  if (isset($matches[2]) && !empty($matches[2])) {
    return $matches[2];
  }
  return array();
}

/**
 * Updates *all* node links in the body field of the node.
 *
 * Checks all nodes.
 */
function drush_reset_to_stock() {
  $dump =
  function ($msg, $label = "Notice") {
    if (is_array($msg)) {
      drush_log($label . ": " . print_r($msg, TRUE), 'status', FALSE);
      return;
    }
    drush_log($label . ": " . $msg, 'status', FALSE);
  };
  global $base_url, $base_path, $user;
  if (empty($base_url)) {
    drush_log("Base url for  is empty for drupal instance at path $base_path.  Cannot continue.", 'error', 'failed');
    return;
  }
  // More on user impersonation at https://www.drupal.org/node/218104
  if (!drush_confirm(t("This will reset this drupal instance to stock, deleting almost all the content within!  Are you sure you wish to continue?"))) {
    $dump("Aborting.");
    return;
  }
  $original_user = $user;
  $old_state = drupal_save_session();

  try {
    // Use custom content_moderator account to ensure scald safety hooks
    // are not bypassed by admin account.
    $dump("Resetting drupal instance to stock.");
    $dump("Changing accounts to content_moderator...");
    $user = user_load(5543);
    $query = new EntityFieldQuery();
    $content_types = array(
      'basic_node',
      'nodeblock',
      'webform',
      'vsx_slide',
    );
    $entities = $query->entityCondition('entity_type', 'node')->propertyCondition('type', $content_types, 'in')->propertyOrderBy('title', 'ASC');
    $result = $query->execute();
    if (isset($result['node']) && !empty($result['node'])) {
      foreach ($result['node'] as $nid => $o) {
        $e_node = entity_metadata_wrapper('node', $nid);
        // $dump($e_node->getPropertyInfo(), "Entity property info");
        foreach ($e_node->field_tags->getIterator() as $delta => $e_tag) {
          if (strcasecmp($e_tag->label(), 'nnlm_drupal_stock') === 0) {
            $dump($e_node->label(), "Stock node, skipping");
            continue 2;
          }
        }
        $dump($e_node->label(), "Removing node");
        $e_node->delete();
      }
    }
    else {
      $dump("No nodes removed");
    }
    // Remove all scald atoms (hooks will save ones in use).
    $atoms = entity_load('scald_atom', FALSE, array());
    // Replicating scald_fetch_multiple code to get all atom ids
    // Verify the SID is legit and fetch basic info about the Atom.
    if (!$atoms) {
      $dump("No atoms found");
      return;
    }
    $sids = array();
    $in_use = function($sid) {
      // Prevent atom deletion if atom is in use
      // BE CAREFUL!  This does not restrict the super-admin account.
      // nnlm_core_dump(__FUNCTION__);
      static $atoms_in_use = array();
      if (in_array($sid, $atoms_in_use)) {
        return TRUE;
      }

      if (!module_exists('mee')) {

        return TRUE;
      }
      $url_list = array();
      // Fetch wherever atom is embedded in a text field.
      $result = (bool) db_query(
      "SELECT COUNT(*) FROM {mee_resource}
WHERE atom_sid = :sid",
      array(':sid' => $sid)
      )->fetchField();
      if ($result) {
        $atoms_in_use[] = $sid;
        return TRUE;
      }
      return FALSE;
    };
    foreach ($atoms as $atom) {
      // note: scald usage hooks should prevent deletion of atoms
      // being used by stock nodes.
      try {
        if($in_use($atom->sid)){
          $dump($atom->title, "Skipping atom (in use)");
          continue;
        }
        $dump($atom->title, "Deleting atom");
        scald_atom_delete($atom->sid);
        $sids[] = $atom->sid;
      }
      catch (\Exception $e) {
        drush_log("Unable to delete atom:" . $e->getMessage(), 'error');
      }
    }
    $dump("Deleted " . count($sids) . " scald media atoms");
  }
  catch (\EntityMetadataWrapperException$e) {
    $dump("EntityMetadataWrapperException:" . $e->getMessage());
  }
  catch (\Exception$e) {
    $dump("Exception:" . $e->getMessage());
  }
  // Reset user to original.
  $user = $original_user;
  drupal_save_session($old_state);
  //remove all users except the original
    $query = new EntityFieldQuery();
  // Run the query as user 1.
  $query->entityCondition('entity_type', 'user');
  $result = $query->execute();
  $count = 0;
  foreach ($result['user'] as $uid=>$o) {
    if(intval($uid) < 1){
      continue;
    }
    else if(intval($uid) === 1){
      $o = user_load($uid);
      user_save($o, array(
        'mail'=>'me@example.com'
      ));
      continue;
    }
    $o = user_load($uid);
    $dump($o->name .' <'.$o->mail.'>', "Removing user");
    user_delete($uid);
    /*if (intval($o->uid) === 0) {
      continue;
    }
    drupal_set_message("removing user $o->uid:  " . $entity->label());
    $entity->delete();*/
  }
  drupal_set_message("Finished!");
}
