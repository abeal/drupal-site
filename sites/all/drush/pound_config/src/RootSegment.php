<?php
/**
 * @file Provides slightly different behavior if the segment is root.
 */

namespace Drush\pound_config;
use Drush\pound_config\LinkTree as LinkTree;
/**
 * A structural representation of a link.
 */
class RootSegment extends Segment {

  /**
   * Builds the root segment.
   */
  public function __construct() {
    parent::__construct('<root>', Segment::NON_CONTENT);
  }
  /**
   * Provides the domain basis for all urls in the link tree.
   *
   * @return string
   */
  public function url() {
    return 'http://nnlm.gov';
  }
  /**
   * Recursively prints the tree.
   *
   * @return string The tree formatted for cli output
   */
  public function rec_print($indent_level = 0) {
    $output = array();
    foreach ($this->children as $child_name => $child) {
      $output = array_merge($output, $child->rec_print(($indent_level + 1)));
    }
    return "LinkTree:\n" . implode("\n", array_values($output));
  }
  /**
   *
   */
  public function rec_print_regex($cumulative_path = '', $route_to_legacy = TRUE) {
    $output = array();
    foreach ($this->children as $child_name => $child) {
      $output = array_merge($output, $child->rec_print_regex('', $route_to_legacy));
    }
    $output_hash = array();
    foreach ($output as $link) {
      $output_hash[preg_replace("/(index)?\.html$/", "", $link)] = TRUE;
    }
    $final_list = array_keys($output_hash);
    $matching_substring = self::get_matching_substring($final_list);
    $output = '^';

    if (drush_get_option('debug', FALSE)) {
      LinkTree::dump("Final list: " . print_r($final_list, TRUE));
      LinkTree::dump("Matching substring: " . implode('', $matching_substring));
    }
    if (!empty($matching_substring)) {
      $nonsegment_start = 0;
      $nonsegment_length = FALSE;
      $array_substr =
      function ($list, $start, $length) {
        $results = array();
        foreach ($list as $item) {
          if ($length === FALSE) {
            $result = substr($item, $start);
          }
          else {
            $result = substr($item, $start, $length);
          }
          // Print "Item: $item, Result: $result\n";
          if (!empty($result) && $result !== '?' && $result != '*') {
            $results[] = $result;
          }
        }
        return array_unique($results);
      };
      for ($j = 0; $j <= max(array_keys($matching_substring)) + 1; $j++) {
        if (isset($matching_substring[$j])) {
          if ($nonsegment_length === FALSE) {
            $nonsegment_length = $j - $nonsegment_start;
            // Beginning of a contiguous segment.  create an option
            // group from what has come before.
            if ($nonsegment_length > 0) {
              $prior_values = $array_substr($final_list, $nonsegment_start, $nonsegment_length);
              if (!empty($prior_values)) {
                $output .= '(' . implode('|', $prior_values) . ')';
              }
            }
          }
          $output .= $matching_substring[$j];
        }
        else {
          $nonsegment_start = $j;
          $nonsegment_length = FALSE;
        }
      }
      //trim the matching substring from all link entries
      $prior_values = $array_substr($final_list, $nonsegment_start, $nonsegment_length);
      if (!empty($prior_values)) {
        $output .= '(' . implode('|', $prior_values) . ')';
      }
      if(drush_get_option('noregion', FALSE)){
        $output .= '{1}';
      }
      else if(in_array(implode('', $matching_substring), $final_list)){
      //if the matching substring is itself a link, the alternation must
      //be allowed to be optional.
        $output .= "?";
      }
      //LinkTree::dump("Nonstart: $nonsegment_start, Nonend: $nonsegement_end, Prior values (end):" . print_r($prior_values, TRUE));
    }
    else {
      $output .= '(' . implode('|', $final_list) . ')';
      if(drush_get_option('noregion', FALSE)){
        $output .= '{1}';
      }
    }
    if (!$route_to_legacy) {
      $output .= '(index)?';
    }
    $output .= '(\.html)?(\?.*)?';
    if (!$route_to_legacy) {
      $output .= DIRECTORY_SEPARATOR . '?';
    }
    $output .= '$';
    return $output;
  }
  /**
   * Returns whether this is the root element.  Overrides parent.
   */
  public function is_root() {
    return TRUE;
  }

}
