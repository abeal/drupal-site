<?php

/**
 * @file
 */
namespace Drush\pound_config;
use Drush\pound_config\LinkTree as LinkTree;

/**
 * A structural representation of a link.
 */
class Segment {
  const NON_CONTENT = 'n';
  const PUBLISHED = 'p';
  const UNPUBLISHED = 'u';
  // Store english descriptions of our status descriptions.
  protected static $status_descriptors = NULL;
  // TRUE.
  public $status = Segment::NON_CONTENT;
  public $path_segment = '';
  // Properties of children are path segments that point to sub-nodes.
  protected $children = array();
  // Bidirectional linking for easy url construction.
  protected $parent = NULL;
  // All children are of the same status.
  protected $cumulative_child_status = FALSE;

  /**
   * Builds a link.
   *
   * @param string $path_segment
   *   The path segment represented by this node.
   * @param const $status
   *   One of the three constant status items defined at the
   *                      top: NON_CONTENT, PUBLISHED, or UNPUBLISHED
   */
  public function __construct($path_segment, $status = Segment::NON_CONTENT) {

    // LinkTree::LinkTree::dump($path_segment, "Constructing new segment");
    self::$status_descriptors = array(
      Segment::NON_CONTENT => "(Non)",
      Segment::PUBLISHED => "(Drupal)",
      Segment::UNPUBLISHED => "(Legacy)",
    );

    if (!is_string($path_segment)) {
      throw new \Exception("Invalid path segment argument $path_segment passed to constructor of " . __CLASS__);
    }
    if (!is_string($path_segment) || !in_array($status, self::get_class_constants())) {
      throw new \Exception("Invalid status argument $status passed to constructor of " . __CLASS__);
    }
    if (empty($path_segment)) {
      throw new \Exception("Path segment is empty");
    }
    $this->path_segment = $path_segment;
    $this->status = $status;
  }

  /**
   * Convenience method to get class constants for argument checking in
   * constructor.
   *
   * @return NULL
   */
  protected static function get_class_constants() {

    $reflect = new \ReflectionClass(__CLASS__);
    return $reflect->getConstants();
  }

  /**
   *
   */
  protected function child_exists($child_name) {

    if ($this->is_leaf()) {
      return FALSE;
    }
    return in_array($child_name, array_keys($this->children));
  }

  /**
   * Hangs a given url from the segment tree.
   *
   * This recursive function initially accepts a string url argument, from
   * the nnlm.gov domain.  It isolates the path, and converts it to an array
   * containing path elements.  A tree is constructed of these elements.  For
   * example, the url 'http://nnlm.gov/evaluation/about.html' would create
   * a tree two deep:
   *   evaluation -> about.html (Leaf)
   * In the above case, evaluation is initially considered "Non-content", and
   * will remain so until another url that explicitly defines content at
   * 'http://nnlm.gov/evaluation' arrives.  Non-content is maintained only
   * for the optimization step, to allow the system to easily create the
   * entry 'evaluation/*' as part of a regex optimization.
   *
   * @param (string|array) $mixed
   *   Either a string URL from the nnlm.gov domain,
   *                                or that url's path component broken into
   *                                an array split by the url directory separators.
   * @param int $status
   *   One of the three class constants defined at the top for
   *                     content status
   *
   * @return NULL
   */
  public function hang($mixed, $status) {

    if (!in_array($status, self::get_class_constants())) {
      throw new \Exception("Wrong status argument type $status passed to constructor of " . __CLASS__);
    }
    if (is_string($mixed)) {
      if (empty($mixed) || substr($mixed, 0, 15) !== 'http://nnlm.gov') {
        throw new \Exception("Invalid parameter '$mixed' passed to " . __FUNCTION__ . " function of class " . __CLASS__);
      }
      // LinkTree::LinkTree::dump(self::$status_descriptors[$status], "Hanging new URL $mixed ");
      // strip everything but the path.
      $url = parse_url(trim($mixed, '/'), PHP_URL_PATH);
      // Obtain path components.
      $url_parts = array_filter(explode('/', $url));
      return $this->hang($url_parts, $status);
    }
    elseif (!is_array($mixed)) {
      throw new \Exception("Wrong argument type passed to " . __FUNCTION__ . " function of class " . __CLASS__);
    }
    $path_segment = (empty($mixed)) ? '' : array_shift($mixed);
    // Ignore status unless it is a leaf.
    $path_segment_is_leaf = (count($mixed) === 0) ? TRUE : FALSE;
    $path_segment_status = ($path_segment_is_leaf) ? $status : Segment::NON_CONTENT;
    $segment = NULL;
    // LinkTree::LinkTree::dump(self::$status_descriptors[$status], "Processing segment $path_segment, status");
    if (empty($path_segment)) {
      return;
    }
    if ($path_segment[0] === '<') {
      // Pseudo url - do not consider.
      return;
    }
    if ($path_segment === '*') {
      $this->parent->cumulative_child_status = $status;
      // Special case - wildcard.  Set this node to serve from Drupal regardless
      // of sub-url content.
      // LinkTree::dump( "Wildcard encountered.  Name: ".$this->path_segment.", Parent name: ".$this->parent->path_segment.", ccs: " .$this->parent->cumulative_child_status."");
      // LinkTree::LinkTree::dump("Added as wildcard", "\t-");
      return;
    }
    if (!$this->child_exists($path_segment)) {
      // LinkTree::LinkTree::dump($path_segment, "Adding new segment");
      $segment = new Segment($path_segment, $path_segment_status);
      $segment = &$this->add($segment);
      $branch = $segment->url() . '(' . self::$status_descriptors[$segment->status] . ')';
    }

    $segment = &$this->children[$path_segment];

    if (!$path_segment_is_leaf) {
      // Continue hanging the remainder
      // LinkTree::LinkTree::dump("Hanging remainder (count " . count($mixed) . ")", "\t-");
      if ($mixed[0] === '*') {
        $segment->cumulative_child_status = $status;
      }
      else {
        $segment->hang($mixed, $status);
      }
    }
    return;
  }

  /**
   * Adds a link to this link's list of children.  If the child already
   * exists, it overwrites that child's properties and returns that instead.
   * Note that one should pay attention to the result of this function;
   * it doesn't always return the same object that went in.
   * Overwriting rules are as follows:
   *   If the existing node is non-content, it is overwritten with whatever
   *   the incoming status is
   *   If the existing node is unpublished, it is overwritten only if the
   *   incoming node is published.
   *   If the existing node is published, it is never overwritten.
   *
   * @param Segment $s
   *   The link to add
   *
   * @returns Either the newly created segment, or a segment already in
   * the tree.
   */
  protected function &add(Segment & $s) {

    $result = $s;
    if ($this->child_exists($s->path_segment)) {
      // LinkTree::LinkTree::dump("Reused segment $s->path_segment", "\t-");
      $result = &$this->children[$s->path_segment];
      if ($s->status !== $result->status) {
        switch ($result->status) {
          case Segment::NON_CONTENT:
            // LinkTree::LinkTree::dump("Status updated to $s->status", "\t\t-");
            $result->status = $s->status;
            break;

          case Segment::UNPUBLISHED:
            // Implicitly published by nature of prior evaluations.
            $result->status = Segment::PUBLISHED;
            break;

          default:
            break;
        }
      }
    }
    else {
      // LinkTree::LinkTree::dump("Created segment $s->path_segment", "\t-");
      $this->children[$s->path_segment] = $s;
      $s->parent = &$this;
    }
    return $result;
  }

  /**
   * Traverses Segment children.  If all child leaves are of identical
   * status, this segment can be optimized.
   *
   * @param bool $force_rebuid
   *   Forces a rebuild of the optimization status
   *
   * @return mixed One of Segment::PUBLISHED or Segment::UNPUBLISHED if
   */
  public function optimize() {
    if ($this->is_leaf()) {
      // Trivial case.  Can't optimize a leaf.
      // $this->cumulative_child_status = $this->status;
      return $this->status;
    }
    if ($this->cumulative_child_status !== FALSE) {
      // Already optimized.
      switch ($this->status) {
        case $this->cumulative_child_status:
          return $this->status;

        break;
        default:
          return FALSE;
      }
    }
    // Attempt to discern identical child status values, and prune branches.
    $cumulative_child_status = NULL;

    $children = array_keys($this->children);
    // If (preg_match("/.*phpsysinfo.*/", $this->path_segment)) {
    //   LinkTree::LinkTree::dump($my_children, "Optimizing node $this->path_segment.  Status: " . $this->status . ", CCS: " . $this->cumulative_child_status . ", Child count" . count($this->children) . ", Children:");
    // }
    foreach ($children as $child_name) {
      $current_child = &$this->children[$child_name];
      $current_child_status = $current_child->optimize();
      if ($current_child_status === Segment::NON_CONTENT) {
        continue;
      }
      if (is_null($cumulative_child_status)) {
        $cumulative_child_status = $current_child_status;
        continue;
      }
      // Set to false the moment child status becomes heterogeneous.
      // do not break, however, so as to assure all children are optimized
      // (entire subtree may be rendered in different contexts).
      if ($cumulative_child_status !== $current_child_status) {
        $cumulative_child_status = FALSE;
      }
    }
    $this->cumulative_child_status = $cumulative_child_status;
    if ($this->cumulative_child_status === FALSE) {
      return FALSE;
    }
    elseif ($this->cumulative_child_status === NULL) {
      return $this->status;
    }
    elseif ($this->status === $this->cumulative_child_status) {
      return $this->status;
    }
    // If here, children are either all legacy or all drupal, and current node
    // is not the same status.
    elseif ($this->status === Segment::NON_CONTENT) {
      return $this->cumulative_child_status;
    }
    else {
      return FALSE;
    }
  }

  /**
   *
   */
  protected function is_leaf() {
    if ($this->status === Segment::NON_CONTENT) {
      return FALSE;
    }
    return (count($this->children) === 0);
  }

  /**
   * Returns whether or not this segment can be optimized away.
   *
   * @return boolean True if all children of this segment are homogenous,
   * false otherwise.
   */
  public function optimal() {

    if (is_null($this->homogeneous_child_status)) {
      throw new \Exception(__FUNCTION__ . " cannot be invoked before optimize is");
    }
    return ($this->homogeneous_child_status !== FALSE);
  }

  /**
   * Returns the path segment of this link.
   *
   * @return [type] [description]
   */
  public function url() {

    if (empty($this->parent)) {
      throw new \Exception("Non-root node " . $this->path_segment . " has no parent.  Malformed tree!");
    }
    return $this->parent->url() . DIRECTORY_SEPARATOR . $this->path_segment;
  }

  /**
   * ToString.
   *
   * @return string [description]
   */
  public function __toString() {

    return $this->url();
  }

  /**
   * Recursively prints the tree for command line output.
   *
   * @param int $indent_level
   *   Intended to be invoked internally by the
   *   recursive callback.  Do not set for initial call
   *
   * @return array                An array of strings to be printed to STDOUT
   */
  public function rec_print($indent_level = 0) {

    $output = array();
    $line = str_repeat('|' . str_repeat(' ', 1), $indent_level) . '-- ' . $this->path_segment;
    if (!$this->is_leaf()) {
      $line .= DIRECTORY_SEPARATOR;
    }
    if ($this->cumulative_child_status === Segment::PUBLISHED) {
      $line .= '* ' . self::$status_descriptors[$this->status];
    }
    elseif ($this->cumulative_child_status === Segment::UNPUBLISHED) {
      $line .= '! ' . self::$status_descriptors[$this->status];
      ;
    }
    if (!$this->is_leaf()) {
      $output[] = $line;
      foreach ($this->children as $child_name => $child) {
        $output = array_merge($output, $child->rec_print(($indent_level + 1)));
      }
    }
    else {
      $output[] = $line . self::$status_descriptors[$this->status];
    }
    return $output;
  }


  /**
   *
   */
  protected static function rec_helper($node, $route_to_legacy) {
    switch ($node->status) {
      case Segment::PUBLISHED:
        if (!$route_to_legacy) {
          return TRUE;
        }
        break;

      case Segment::UNPUBLISHED:
        if ($route_to_legacy) {
          return TRUE;
        }
        break;
    }
    return FALSE;
  }


  /**
   * Prints the regular expression for pound output for Drupal.
   *
   * @return NULL
   */
  public function rec_print_regex($cumulative_path = '', $route_to_legacy = TRUE) {
    $line = $cumulative_path . DIRECTORY_SEPARATOR . $this->path_segment;
    $output = array();
    $show_debug = drush_get_option('debug', FALSE);
    // && preg_match("/mar\/newsletter/", $line);
    if ($show_debug) {
      LinkTree::dump("Line: $line, Status: $this->status, CCS: $this->cumulative_child_status, Leaf: " . $this->is_leaf() . ", Child count: " . count($this->children));
    }

    if ($this->is_leaf()) {
      if (self::rec_helper($this, $route_to_legacy)) {
        if ($show_debug) {
          LinkTree::dump("Adding as leaf");
        }
        $output[] = $line;
      }
      return $output;
    }
    else {
      // If here, we are in a non-root branch of the tree, and the
      // status of all the child nodes is NOT equal to the status of
      // this node.  We can optimize in some cases, but for most
      // we'll have to recurse to the children as well.
      switch ($this->cumulative_child_status) {
        case Segment::PUBLISHED:
          switch ($this->status) {
            case Segment::PUBLISHED:
              if ($route_to_legacy) {
                return $output;
              }
              else {
                if (drush_get_option('debug', FALSE)) {
                  LinkTree::dump("Adding as pp branch wildcard");
                }
                $output[] = $line . '.*';
                return $output;
              }
              break;

            case Segment::UNPUBLISHED:
              if ($route_to_legacy) {
                if (drush_get_option('debug', FALSE)) {
                  LinkTree::dump("Adding as up branch leaf");
                }
                $output[] = $line;
                return $output;
              }
              else {
                if (drush_get_option('debug', FALSE)) {
                  LinkTree::dump("Adding as up branch wildcard");
                }
                $output[] = $line . DIRECTORY_SEPARATOR . '.*';
                return $output;
              }
              break;

            case Segment::NON_CONTENT:
              if ($route_to_legacy) {
                return $output;
              }
              else {
                if (drush_get_option('debug', FALSE)) {
                  LinkTree::dump("Adding as np branch wildcard\n");
                }
                $output[] = $line . DIRECTORY_SEPARATOR . "?.*";
                return $output;
              }

              break;
          }

          break;

        case Segment::UNPUBLISHED:
          switch ($this->status) {
            case Segment::PUBLISHED:
              if ($route_to_legacy) {
                if ($show_debug) {
                  LinkTree::dump("Adding as pu branch wildcard\n");
                }
                $output[] = $line . DIRECTORY_SEPARATOR . '.*';
                return $output;
              }
              else {
                if ($show_debug) {
                  LinkTree::dump("Adding as pu branch leaf\n");
                }
                $output[] = $line;
                return $output;
              }
              break;

            case Segment::UNPUBLISHED:
              if ($route_to_legacy) {
                if ($show_debug) {
                  LinkTree::dump("Adding as uu branch wildcard\n");
                }
                $output[] = $line . '.*';
                return $output;
              }
              else {
                return $output;
              }
              break;

            case Segment::NON_CONTENT:
              if ($route_to_legacy) {
                if ($show_debug) {
                  LinkTree::dump("Adding as np branch wildcard\n");
                }
                $output[] = $line . DIRECTORY_SEPARATOR . "?.*";
                return $output;
              }
              else {
                return $output;
              }

              break;
          }
          break;

        case FALSE:
          if ($show_debug) {
            LinkTree::dump("Adding as plain link\n");
          }
          if(self::rec_helper($this, $route_to_legacy)){
            $output []= $line;
          }
          //do not return - children need to be processed.
          break;

        // Cannot optimize.
        default:
          throw new \Exception("Invalid cumulative child status " . $this->cumulative_child_status);
        break;
      }

      if ($show_debug) {
        LinkTree::dump("Recursing on children of $this->path_segment\n");
      }
      foreach ($this->children as $child_name => $child) {
        $output = array_merge($output, $child->rec_print_regex($line, $route_to_legacy));
      }
      return $output;
    }
    /*
    elseif (($this->status === Segment::NON_CONTENT && $this->cumulative_child_status == Segment::PUBLISHED && !$route_to_legacy) || ($this->status === Segment::NON_CONTENT && $this->cumulative_child_status === Segment::UNPUBLISHED && $route_to_legacy)) {
    $output[] = $line . DIRECTORY_SEPARATOR . '.*';
    return $output;
    }
    else {
    if (($this->status === Segment::PUBLISHED && !$route_to_legacy) || ($this->status === Segment::UNPUBLISHED && $route_to_legacy)) {
    $output[] = $line;
    }
    foreach ($this->children as $child_name => $child) {
    $output = array_merge($output, $child->rec_print_regex($line, $route_to_legacy));
    }
    return $output;
    }*/
  }


  /**
   *
   */
  protected static function get_matching_substring($output) {

    $results = array();
    // Optimize regex.
    $common = '';
    // Start with arbitrarily high number.
    $shortest = 1000;
    // Find the shortest url  - commonalities will not be longer than that.
    foreach ($output as $url) {
      $shortest = min(strlen($url), $shortest);
    }
    // Find the longest matching substring from the front for all urls.
    for ($i = 0; $i < $shortest; $i++) {
      // Use the first entry.
      $common = $output[0][$i];
      for ($j = 1; $j < count($output); $j++) {
        if ($output[$j][$i] != $common) {
          $common = FALSE;
          break;
        }
      }
      if ($common !== FALSE) {
        $results[$i] = $common;
      }
    }
    return $results;
  }
  /**
   *
   */
  protected static function get_matching_substring2($output) {

    $shared_words = array();
    $results = array();
    foreach ($output as $i => $link) {
      $words = preg_split("/\//", $link);
      foreach ($words as $index => $word) {
        if (empty($word) || !preg_match("/^[-a-z0-9]+$/", $word)) {
          continue;
        }
        if (!isset($shared_words[$word])) {
          $shared_words[$word] = array(
            'index' => $index,
            'shared by' => array(),
            'strpos' => strpos($link, $word),
            'universal' => TRUE,
          );
        }
        else {
          $current_position = $shared_words[$word]['strpos'];
          $new_position = strpos($link, $word);
          if ($current_position != $new_position) {
            // Do not optimize on the same term twice in different positions.
            $shared_words[$word]['strpos'] = FALSE;
          }
        }
        $shared_words[$word]['shared by'][] = $i;
      }
    }
    // LinkTree::dump("Shared words: " .print_r($shared_words, TRUE)."\n");
    $current_word = NULL;
    foreach ($shared_words as $word => & $arr) {
      if (count($arr['shared by']) !== count($output)) {
        $arr['universal'] = FALSE;
      }

      if (count($arr['shared by']) < 3) {
        // Note: '5' is an arbitrarily chosen number.
        continue;
      }
      if ($arr['strpos'] === FALSE) {
        // Do not optimize on the same term twice in different positions.
        continue;
      }
      $results[$word] = $arr;
    }
    // LinkTree::dump("Shared words:".print_r($shared_words, TRUE)."\n");
    return $results;
  }
  /**
   *
   */
  protected static function get_matching_substring3($output) {

    $shared_words = array();
    $results = array();
    foreach ($output as $i => $link) {
      $words = preg_split("/\//", $link);
      foreach ($words as $index => $word) {
        if (empty($word) || !preg_match("/^[-a-z0-9]+$/", $word)) {
          continue;
        }
        if (!isset($shared_words[$word])) {
          $shared_words[$word] = array(
            'index' => $index,
            'shared by' => array(),
            'strpos' => strpos($link, $word),
          );
        }
        else {
          $current_position = $shared_words[$word]['strpos'];
          $new_position = strpos($link, $word);
          if ($current_position != $new_position) {
            // Do not optimize on the same term twice in different positions.
            $shared_words[$word]['strpos'] = FALSE;
          }
        }
        $shared_words[$word]['shared by'][] = $i;
      }
    }
    // LinkTree::dump("Shared words: " .print_r($shared_words, TRUE)."\n");
    foreach ($shared_words as $word => $arr) {
      if (count($arr['shared by']) < 3) {
        // Note: '5' is an arbitrarily chosen number.
        continue;
      }
      if ($arr['strpos'] === FALSE) {
        // Do not optimize on the same term twice in different positions.
        continue;
      }
      $results[$word] = $arr;
    }
    // LinkTree::dump("Shared words:".print_r($shared_words, TRUE)."\n");
    return $results;
  }

  /**
   * Returns whether or not this is a root link (base of tree structure).
   *
   * @return boolean       TRUE if this is a root node, FALSE if it is not.
   */
  public function is_root() {

    return FALSE;
  }

}
