<?php
// This configuration file is for ease of establishing instance-specific
// variables that the pound-config script use for operation.
// Comments start with '// ' as in php.ini

// This section covers hardcoded legacy exclusions.  Pound currently routes
// traffic to legacy with a positive match, with a drupal fallback.  It
// primarily relies on the contents of the migration map entries where
// migrate = 'l', i.e. the content is specifically set to be served
// from legacy.  There is content on nnlm.gov that lives outside of the
// map, however, and it must be accounted for.  This is the place to do that.
// Any path listed here will also be added to the considerations when
// determining the legacy exclusion pound regex.

// Syntax for exclusions:
// - All paths should be relative.
// - Asterisks at the end indicate a wildcard, so that the path should match
//   both the indicated path, and any sub-paths as well.
// - Each entry should be followed by ' = 0', indicating it is served from legacy
//   (a 1 would indicate Drupal, which is not very useful here.)

return array(
    "pound_config_hardcoded_legacy_exclusions_default" => array(
        "/archive/*", //  Legacy 'archive'
        "/archive_resources/*", //  Legacy 'archive'
        "/bhic/*", //  wp blog
        "/ep/*", //  wp blog
        "/evaluation/blog/*", //  wp blog
        "/gmr/blog/*", //  wp blog
        "/maps/*", //  component of the members directory
        "/mar/blog/*", //  wp blog
        "/mar/newsletter/*", //  wp blog
        "/mcr/news_blog/*", //  wp blog
        "/mcr/p2pp/*", //  blog
        "/members/*", //  members directory
        "/moodle*", //  moodle (includes development variants, like moodle22, moodle23, etc)
        "/ner/blog/*", //  wp blog
        "/ntc", //  wp blog
        "/phpsysinfo/*", //  functionality testing on legacy
        "/pnr/dragonfly/*", //  wp blog
        "/psr/newsbits/*", //  wp blog
        "/psr/newsletter/*", //  wp blog
        "/publicfolder/*", //  upload location for legacy file sharing
        "/scr/blog/*", //  wp blog
        "/scripts/*", //  legacy global scripts directory
        "/styles/*", //  legacy global styles directory
        "/sea/newsletter/*", //  wp blog
        "/sysusage/*", //  functionality testing on legacy
        "/tribalconnections/*" //  archived tribalconnections site
    )
);
