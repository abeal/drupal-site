/* Galleria NN/LM Theme 2012-08-07 | https://raw.github.com/aino/galleria/master/LICENSE | (c) Aino */
/* line 22, ../sass/galleria.nnlm.scss */
#galleria-loader {
  height: 1px !important;
}

/* line 26, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm {
  position: relative;
  overflow: hidden;
  width: 50%;
  margin: auto;
  border: 1px solid black;
  border-radius: 10px;
  padding: 10px;
  /* Safari/Chrome, other WebKit */
  -webkit-box-sizing: border-box;
  /* Firefox, other Gecko */
  -moz-box-sizing: border-box;
  /* Opera/IE 8+ */
  box-sizing: border-box;
}
/* line 40, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm img {
  -moz-user-select: none;
  -webkit-user-select: none;
  -o-user-select: none;
  margin: 0;
}
/* line 48, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-images .galleria-image img {
  max-height: 350px;
}
/* line 53, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-stage {
  background: -moz-linear-gradient(black, gray);
  /* FF 3.6+ */
  background: -ms-linear-gradient(black, gray);
  /* IE10 */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, black), color-stop(100%, gray));
  /* Safari 4+, Chrome 2+ */
  background: -webkit-linear-gradient(black, gray);
  /* Safari 5.1+, Chrome 10+ */
  background: -o-linear-gradient(black, gray);
  /* Opera 11.10 */
  -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='$gradient-start', endColorstr='$gradient-end')";
  /* IE8+ */
  background: linear-gradient(#000000, #808080);
  position: absolute;
  top: 10px;
  bottom: 60px;
  left: 10px;
  right: 10px;
  overflow: hidden;
  border-radius: 10px;
}
/* line 63, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-thumbnails-container {
  height: 50px;
  bottom: 0;
  position: absolute;
  left: 10px;
  right: 10px;
  z-index: 2;
}
/* line 71, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-thumbnails-container .galleria-thumbnails .galleria-image {
  height: 40px;
  width: 60px;
  background: black;
  margin: 0 5px 0 0;
  border: 1px solid black;
  float: left;
  cursor: pointer;
}
/* line 81, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-thumbnails-container .disabled {
  opacity: .2;
  filter: alpha(opacity=20);
  cursor: default;
}
/* line 88, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-carousel .galleria-thumbnails-list {
  margin-left: 30px;
  margin-right: 30px;
}
/* line 91, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-carousel .galleria-thumbnails-list .galleria-thumb-nav-left, .galleria-theme-nnlm .galleria-carousel .galleria-thumbnails-list .galleria-thumb-nav-right {
  display: block;
  background-image: url(nnlm-map.png);
  background-repeat: no-repeat;
}
/* line 98, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-counter {
  position: absolute;
  bottom: 10px;
  left: 10px;
  text-align: right;
  color: #fff;
  font: normal 11px/1 arial,sans-serif;
  z-index: 1;
}
/* line 107, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-loader {
  background: black;
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  right: 10px;
  z-index: 2;
  display: none;
  background: url(nnlm-loader.gif) no-repeat 2px 2px;
}
/* line 118, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-info {
  width: 100%;
  bottom: 60px;
  left: 0;
  z-index: 2;
  position: absolute;
  opacity: .8;
  filter: alpha(opacity=80);
}
/* line 126, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-info .galleria-info-title {
  font: bold 12px/1.1 arial,sans-serif;
  margin: 0;
  color: #fff;
  margin-bottom: 7px;
}
/* line 132, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-info .galleria-info-text {
  background-color: black;
  padding: 12px;
  /* IE7 */
  zoom: 1;
}
/* line 138, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-info .galleria-info-description {
  font: italic 12px/1.4 georgia,serif;
  margin: 0;
  color: #bbb;
}
/* line 143, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-info .galleria-info-link {
  display: none;
  background-position: -669px -5px;
  position: absolute;
  top: 2px;
  right: 15px;
  cursor: pointer;
  background-color: #000;
  background-image: url(nnlm-map.png);
  background-repeat: no-repeat;
  border: 1px solid rgba(255, 255, 255, 0);
  border-radius: 50%;
  width: 1.5em;
  height: 1.5em;
}
/* line 157, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-info .galleria-info-link:hover {
  background-color: #333333;
}
/* line 161, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-info .galleria-info-close {
  width: 9px;
  height: 9px;
  position: absolute;
  top: 5px;
  right: 5px;
  background-image: url(nnlm-map.png);
  background-position: -753px -11px;
  opacity: .5;
  filter: alpha(opacity=50);
  cursor: pointer;
}
/* line 175, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .notouch .galleria-info-close:hover {
  opacity: 1;
  filter: alpha(opacity=100);
}
/* line 181, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .touch .galleria-info-close:active {
  opacity: 1;
  filter: alpha(opacity=100);
}
/* line 186, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-image-nav {
  position: absolute;
  top: 50%;
  margin-top: -62px;
  width: 100%;
  height: 62px;
  left: 0;
}
/* line 194, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-image-nav-left, .galleria-theme-nnlm .galleria-image-nav-right {
  opacity: .3;
  filter: alpha(opacity=30);
  cursor: pointer;
  width: 62px;
  height: 124px;
  position: absolute;
  left: 10px;
  z-index: 2;
  background-position: 0 46px;
  background-image: url(nnlm-map.png);
  background-repeat: no-repeat;
}
/* line 206, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-image-nav-left:hover, .galleria-theme-nnlm .galleria-image-nav-left:active, .galleria-theme-nnlm .galleria-image-nav-right:hover, .galleria-theme-nnlm .galleria-image-nav-right:active {
  opacity: 1;
  filter: alpha(opacity=100);
}
/* line 212, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-image-nav-right {
  left: auto;
  right: 10px;
  background-position: -254px 46px;
  z-index: 2;
}
/* line 218, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-thumb-nav-left, .galleria-theme-nnlm .galleria-thumb-nav-right {
  cursor: pointer;
  background-position: -495px 5px;
  position: absolute;
  left: 0;
  top: 0;
  height: 40px;
  width: 23px;
  z-index: 3;
  opacity: .8;
  filter: alpha(opacity=80);
}
/* line 231, ../sass/galleria.nnlm.scss */
.galleria-theme-nnlm .galleria-thumb-nav-right {
  background-position: -578px 5px;
  border-right: none;
  right: 0;
  left: auto;
}
