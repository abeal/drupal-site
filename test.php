<?php

define('DRUPAL_ROOT', getcwd());

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

foreach (module_implements('block_info') as $module) {
  $module_blocks = module_invoke($module, 'block_info');
  if (!isset($module_blocks)) {
    print $module . ', ';
  }
}
print "DONE";