#!/bin/bash

for arg in "gmr" "mar" "mcr" "ner" "pnr" "psr" "scr" "sea"; do 
    echo -n "$arg: ";
    ./pound-config.sh $arg | wc -c
done;
echo -n "National: "
./pound-config.sh evaluation,services,about,ntc,ntcc | wc -c;
echo -n "No-arg: ";
./pound-config.sh | wc -c;