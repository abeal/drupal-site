#!/usr/bin/env bash
# promote module, theme, library, and file code.
rsync -av --delete /w3/sites/all/ /nnlm.gov/sites/all
rsync -av --delete /w3/sites/default/files/ /nnlm.gov/sites/default/files
/usr/bin/drush -r /w3/ sql-dump > /home/abeal/drush-sandbox-dump.sql

#TODO: add production alteration for pathologic module
#TODO: add commands to disable non-production modules, once I've determined
#which they are.
